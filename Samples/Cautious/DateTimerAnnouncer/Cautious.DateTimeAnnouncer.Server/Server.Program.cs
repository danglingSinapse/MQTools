﻿using Cautious.DateTimeAnnouncer.Shared;
using CPR.MQTools;
using CPR.MQTools.Functions;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;

namespace Cautious.DateTimeAnnouncer.Server
{
    public static class Program
    {
        public static void Main()
        {
            var entity = new MqEntity(new RouterSocket());

            var discovery = new DiscoveryDevice(Configs.DiscoveryPort, Configs.DiscoveryName);
            entity.AddFunctionality(discovery);

            var publisher = new CautiousDataPublisher(new DataSource(), new VersionManager());
            entity.AddFunctionality(publisher);

            var timer = new NetMQTimer(Configs.DataPublishInterval);
            var publishTopic = new MessageType(Configs.PublishTopic);

            timer.Elapsed += (sender, timerEventArgs) =>
            {
                string currentDateTime = DateTime.Now.ToString("G");
                publisher.Publish(publishTopic, new NetMQMessage(new List<NetMQFrame>
                {
                    new MqFrame(currentDateTime)
                }));
            };

            entity.Poller.Add(timer);

            var monitoringDevice = new MonitoringDevice(monitoringPort: Configs.MonitoringPort);
            entity.Run();
        }
    }
}