﻿using Cautious.DateTimeAnnouncer.Shared;
using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Threading.Tasks;

namespace Cautious.DateTimeAnnouncer.Client
{
    internal static class Program
    {
        private static void Main()
        {
            var entity = new MqEntity(new RouterSocket());

            var existence = new ExistenceBroadcaster(Configs.DiscoveryPort, Configs.DiscoveryName);
            entity.AddFunctionality(existence);

            var consumer = new CautiousDataSubscriber(new VersionManager());
            entity.AddFunctionality(consumer);

            entity.RegisterReceiveHandler(new MessageType(Configs.PublishTopic), CurrentDateTimeResponseHandler);

            entity.Bind();
            entity.Run();
        }

        private static Task CurrentDateTimeResponseHandler(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            string currentDateTime = message[0].ConvertToString();
            Console.WriteLine(currentDateTime);

            return Task.CompletedTask;
        }
    }
}