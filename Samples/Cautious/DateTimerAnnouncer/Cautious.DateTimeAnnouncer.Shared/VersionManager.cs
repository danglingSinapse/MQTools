﻿using CPR.MQTools;
using CPR.MQTools.Interfaces;
using System.Threading.Tasks;

namespace Cautious.DateTimeAnnouncer.Shared
{
    public class VersionManager : IDataVersionController
    {
        private DataInfo _dataInfo;

        public DataInfo GetLastDataInfo(string publisherId)
        {
            return _dataInfo;
        }

        public Task StoreDataInfo(string publisherId, DataInfo info)
        {
            _dataInfo = info;

            return Task.CompletedTask;
        }
    }
}