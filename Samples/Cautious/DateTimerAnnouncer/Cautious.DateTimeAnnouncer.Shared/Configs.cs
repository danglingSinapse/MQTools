﻿using System;

namespace Cautious.DateTimeAnnouncer.Shared
{
    public static class Configs
    {
        public const int DiscoveryPort = 5000;

        public const string DiscoveryName = "PubSubDiscovery";

        public const string PublishTopic = "CurrentDateTime";

        public static TimeSpan DataPublishInterval = TimeSpan.FromSeconds(1);

        public const int MonitoringPort = 5000;
    }
}