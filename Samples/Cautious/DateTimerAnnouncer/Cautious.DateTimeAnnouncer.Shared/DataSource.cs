﻿using CPR.MQTools;
using CPR.MQTools.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cautious.DateTimeAnnouncer.Shared
{
    public class DataSource : IDataSource
    {
        private static readonly Dictionary<long, MissingData> Data = new Dictionary<long, MissingData>();

        public Task Store(string publisherId, MissingData data)
        {
            Data.Add(data.DataIndex, new MissingData(data));

            return Task.CompletedTask;
        }

        public Task Store(string publisherId, long index, MissingData data)
        {
            throw new System.NotImplementedException();
        }

        public Task<MissingData> Restore(string publisherId, long index)
        {
            Data.TryGetValue(index, out MissingData data);

            return Task.FromResult(new MissingData(data));
        }

        public Task<IEnumerable<MissingData>> RestoreRange(string publisherId, long fromIndex, long toIndex)
        {
            return Task.FromResult(Data.Values
                .Where(v => v.DataIndex <= toIndex && v.DataIndex >= fromIndex));
        }
    }
}