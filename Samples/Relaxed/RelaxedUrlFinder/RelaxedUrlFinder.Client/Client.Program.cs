﻿using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using RelaxedUrlFinder.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RelaxedUrlFinder.Client
{
    internal static class Program
    {
        private static void Main()
        {
            var entity = new MqEntity(new RouterSocket());

            //var discovery = new DiscoveryDevice(Configs.DiscoveryPort, Configs.DiscoveryName);
            var redisDiscovery = new RedisDiscoveryDevice("localhost", Configs.DiscoveryName);
            entity.AddFunctionality(redisDiscovery);

            var workDistributor = new InstantWorkDistributor(TimeSpan.FromMinutes(1));
            entity.AddFunctionality(workDistributor);

            entity.RegisterReceiveHandler(new MessageType(Configs.UrlFoundResponse), UrlFoundResponseHandler);

            entity.RunAsync();

            while (true)
            {
                Console.WriteLine("=========================================");
                Console.WriteLine("Enter webpage URL to extract URLs from : ");
                string urlToScan = Console.ReadLine();

                workDistributor.SendWork(new NetMQMessage(new List<NetMQFrame>
                {
                    new MqFrame(Configs.FindUrlRequest),
                    new MqFrame(urlToScan)
                }));
            }
        }

        private static Task UrlFoundResponseHandler(IMqEntity sender, Identity identity, MessageType messagetype, NetMQMessage message)
        {
            NetMQFrame foundUrlFrames = message[1];
            string foundUrls = foundUrlFrames.ConvertToString();

            Console.WriteLine();
            Console.WriteLine("RESPONSE :");
            Console.WriteLine(foundUrls);
            Console.WriteLine("=========================================");

            return Task.CompletedTask;
        }
    }
}