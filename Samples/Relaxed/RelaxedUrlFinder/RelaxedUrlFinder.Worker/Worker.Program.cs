﻿using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using RelaxedUrlFinder.Service;
using RelaxedUrlFinder.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RelaxedUrlFinder.Worker
{
    internal static class Program
    {
        private static void Main()
        {
            var entity = new MqEntity(new RouterSocket());

            //var existence = new ExistenceBroadcaster(Configs.DiscoveryPort, Configs.DiscoveryName);
            //var customExistence = new MyCustomBroadcaster(Configs.DiscoveryPort, Configs.DiscoveryName);
            var redisExistence = new RedisExistenceBroadcaster("localhost", Configs.DiscoveryName);
            entity.AddFunctionality(redisExistence);

            var relaxedWorker = new RelaxedWorker(resultTypes: new MessageType(Configs.UrlFoundResponse));
            entity.AddFunctionality(relaxedWorker);

            entity.RegisterReceiveHandler(new MessageType(Configs.FindUrlRequest), FindUrlRequestHandler);

            entity.Bind(5000);
            entity.RunAsync();

            Console.ReadKey();
        }

        private static async Task FindUrlRequestHandler(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            NetMQFrame indexFrame = message[0];
            NetMQFrame urlFrame = message[1];
            string url = urlFrame.ConvertToString();

            var urlFinder = new UrlFinder(url);
            string data = await urlFinder.ExtractUrls();

            var response = new NetMQMessage(new List<NetMQFrame>
            {
                indexFrame,
                new MqFrame(data)
            });

            await sender.SendMessage(identity, new MessageType(Configs.UrlFoundResponse), response);
        }
    }
}