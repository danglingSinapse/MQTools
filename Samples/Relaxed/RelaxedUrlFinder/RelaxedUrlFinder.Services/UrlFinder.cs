﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace RelaxedUrlFinder.Service
{
    public class UrlFinder
    {
        private readonly string _url;

        public UrlFinder(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException(url);

            _url = url;
        }

        public async Task<string> ExtractUrls()
        {
            var web = new HtmlWeb();
            HtmlDocument htmlDocument = await web.LoadFromWebAsync(_url);

            IEnumerable<string> extractedUrls = htmlDocument.DocumentNode
                .SelectNodes("//a[@href]")
                .Select(node => node.Attributes["href"].Value)
                .Where(href => href.Contains("a"));

            return string.Join($",{Environment.NewLine}", extractedUrls);
        }
    }
}