﻿namespace RelaxedUrlFinder.Shared
{
    public static class Configs
    {
        public const int DiscoveryPort = 5000;

        public const string DiscoveryName = "Worker";

        public const string UrlFoundResponse = nameof(UrlFoundResponse);

        public const string FindUrlRequest = nameof(FindUrlRequest);
    }
}