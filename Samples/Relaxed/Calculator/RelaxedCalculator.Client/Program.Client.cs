﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using RelaxedCalculator.Shared;

namespace RelaxedCalculator.Client
{
    internal static class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("====== Client ======");

            var entity = new MqEntity(new RouterSocket());

            var discovery = new DiscoveryDevice(Configs.Port, Configs.DiscoveryName);
            entity.AddFunctionality(discovery);

            var workDistributor = new InstantWorkDistributor();
            entity.AddFunctionality(workDistributor);

            Calculator.ResponseTypes.ForEach(responseType =>
                entity.RegisterReceiveHandler(new MessageType(responseType), HandleResponse));

            entity.RunAsync();

            while (true)
            {
                Console.WriteLine("Number 1 :");
                int number1 = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                Console.WriteLine("Operation :");
                int operation = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                Console.WriteLine("Number 2 :");
                int number2 = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                var calculator = new Calculator(number1, number2);

                string jsonSerializedCalculator = JsonConvert.SerializeObject(calculator);
                byte[] encodedCalculator = Encoding.UTF8.GetBytes(jsonSerializedCalculator);
                var calculationOperation = (Operations)operation;

                await workDistributor.SendWork(new NetMQMessage(new List<NetMQFrame>
                {
                    new MqFrame(calculationOperation),
                    new MqFrame(encodedCalculator)
                }));
            }
        }

        private static Task HandleResponse(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            NetMQFrame responseFrame = message[1];
            int calculationResult = responseFrame.ConvertToInt32();

            Console.WriteLine($"Message Type: {messageType.ConvertToEnum<Operations>()}, Result: {calculationResult}");
            return Task.CompletedTask;
        }
    }
}