﻿using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using LazyCalculator.Shared;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LazyCalculator.Client
{
    internal static class Program
    {
        private static void Main()
        {
            Console.WriteLine("====== Client ======");

            var entity = new MqEntity(new RouterSocket());

            var existence = new ExistenceBroadcaster(Configs.DiscoveryPort, Configs.DiscoveryName);
            entity.AddFunctionality(existence);

            var workDistributor = new LazyWorkDistributor();
            entity.AddFunctionality(workDistributor);

            Calculator.ResponseTypes.ForEach(responseType =>
                entity.RegisterReceiveHandler(new MessageType(responseType), CalculationResponseHandler));

            entity.Bind();
            entity.RunAsync();

            while (true)
            {
                Console.WriteLine("Number 1 :");
                int number1 = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                Console.WriteLine("Operation :");
                int operation = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                Console.WriteLine("Number 2 :");
                int number2 = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

                var calculator = new Calculator(number1, number2);

                string jsonSerializedCalculator = JsonConvert.SerializeObject(calculator);
                byte[] encodedCalculator = Encoding.UTF8.GetBytes(jsonSerializedCalculator);
                var calculationOperation = (Operations)operation;

                workDistributor.EnqueueWork(new MessageType(calculationOperation),
                    new NetMQMessage(new List<NetMQFrame>
                    {
                        new MqFrame(encodedCalculator)
                    }));
            }
        }

        private static Task CalculationResponseHandler(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            NetMQFrame responseFrame = message[1];
            int calculationResult = responseFrame.ConvertToInt32();

            Console.WriteLine($"Message Type: {messageType.ConvertToEnum<Operations>()}, Response: {calculationResult}");
            return Task.CompletedTask;
        }
    }
}