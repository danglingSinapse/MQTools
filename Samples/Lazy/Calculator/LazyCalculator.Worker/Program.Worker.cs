﻿using CPR.MQTools;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using LazyCalculator.Shared;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyCalculator.Worker
{
    public static class Program
    {
        public static void Main()
        {
            Console.WriteLine("====== Worker ======");

            var entity = new MqEntity(new RouterSocket());

            var discovery = new DiscoveryDevice(Configs.DiscoveryPort, Configs.DiscoveryName);
            entity.AddFunctionality(discovery);

            MessageType[] responseTypes = Calculator.ResponseTypes
                .Select(responseType => new MessageType(responseType))
                .ToArray();

            var worker = new EagerWorker(responseTypes);
            entity.AddFunctionality(worker);

            Calculator.RequestTypes.ForEach(requestType =>
                entity.RegisterReceiveHandler(new MessageType(requestType), CalculationRequestHandler));

            entity.Run();
        }

        private static Task CalculationRequestHandler(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            NetMQFrame indexFrame = message[0];
            NetMQFrame calculatorFrame = message[1];
            byte[] calculatorBytes = calculatorFrame.ToByteArray();
            string jsonSerializedCalculator = Encoding.UTF8.GetString(calculatorBytes);

            var calculator = JsonConvert.DeserializeObject<Calculator>(jsonSerializedCalculator);
            var calculationOperation = messageType.ConvertToEnum<Operations>();

            int calculationResult;
            Operations calculationType;

            switch (calculationOperation)
            {
                case Operations.AddRequest:
                    calculationResult = calculator.Sum;
                    calculationType = Operations.AddResponse;
                    break;

                case Operations.SubtractRequest:
                    calculationResult = calculator.Subtract;
                    calculationType = Operations.SubtractResponse;
                    break;

                case Operations.MultiplyRequest:
                    calculationResult = calculator.Multiply;
                    calculationType = Operations.MultiplyResponse;
                    break;

                case Operations.DivideRequest:
                    calculationResult = calculator.Divide;
                    calculationType = Operations.DivideResponse;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            var response = new NetMQMessage(new List<NetMQFrame>
            {
                indexFrame,
                new MqFrame(calculationResult)
            });

            return sender.SendMessage(identity, new MessageType(calculationType), response);
        }
    }
}