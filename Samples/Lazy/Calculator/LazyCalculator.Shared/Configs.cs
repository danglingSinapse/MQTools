﻿namespace LazyCalculator.Shared
{
    public static class Configs
    {
        public const int DiscoveryPort = 5000;

        public const string DiscoveryName = "LazyWorker";
    }
}