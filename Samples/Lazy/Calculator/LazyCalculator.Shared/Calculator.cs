﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LazyCalculator.Shared
{
    public class Calculator
    {
        public Calculator(int number1, int number2)
        {
            Number1 = number1;
            Number2 = number2;
        }

        public int Number1 { get; }

        public int Number2 { get; }

        public int Sum => Number1 + Number2;

        public int Subtract => Number2 - Number1;

        public int Multiply => Number1 * Number2;

        public int Divide => Number1 / Number2;

        [JsonIgnore]
        public static List<Operations> RequestTypes => new List<Operations>
        {
            Operations.AddRequest,
            Operations.SubtractRequest,
            Operations.MultiplyRequest,
            Operations.DivideRequest
        };

        [JsonIgnore]
        public static List<Operations> ResponseTypes => new List<Operations>
        {
            Operations.AddResponse,
            Operations.SubtractResponse,
            Operations.MultiplyResponse,
            Operations.DivideResponse
        };
    }

    public enum Operations
    {
        AddRequest = 1,
        SubtractRequest = 2,
        MultiplyRequest = 3,
        DivideRequest = 4,
        AddResponse = 5,
        SubtractResponse = 6,
        MultiplyResponse = 7,
        DivideResponse = 8
    }
}