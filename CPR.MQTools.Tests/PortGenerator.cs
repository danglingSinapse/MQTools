﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace CPR.MQTools.Tests
{
    public class PortWrapper
    {
        private int _port;

        ~PortWrapper()
        {
            PortGenerator.ReleasePort(_port);
        }

        public static implicit operator int(PortWrapper wrapper)
        {
            return wrapper._port;
        }

        public static implicit operator PortWrapper(int port)
        {
            return new PortWrapper {_port = port};
        }

        public override string ToString()
        {
            return _port.ToString();
        }
    }

    public static class PortGenerator
    {
        private static readonly List<int> InusePorts = new List<int>();
        private static readonly object Locker = new object();

        public static PortWrapper GetNext()
        {
            lock (Locker)
            {
                int re;
                var rand = new Random();
                do
                {
                    re = rand.Next(10000, 20000);
                    if (IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpListeners().Select(x => x.Port)
                            .Contains(re) || InusePorts.Contains(re))
                        re = 0;
                } while (re == 0);

                InusePorts.Add(re);
                return re;
            }
        }

        public static void ReleasePort(int port)
        {
            lock (Locker)
            {
                InusePorts.Remove(port);
            }
        }
    }
}