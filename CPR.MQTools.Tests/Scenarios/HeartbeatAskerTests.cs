﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class HeartbeatAskerTests : TestBase
    {
        [Fact]
        private void heartbeat_asker_on_setting_middlewares()
        {
            var hearbeatAsker = new HeartbeatAsker();
            var receiveMiddleware = (IMqReceiveMiddleware) hearbeatAsker;
            var receiveMiddlewares = receiveMiddleware.GetReceiveMiddlewares();
            var receiveMessageTypes = receiveMiddlewares.Select(x => x.MessageType).ToArray();
            receiveMessageTypes.Should().Contain(new MessageType("PONG"));
        }

        [Fact]
        private void heartbeat_asker_on_monitoring_health()
        {
            var heartbeatAsker = new HeartbeatAsker(new[] {new Identity("monitoree"), },
                TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(5000));
            var mqEntity = new MqEntityStub();
            var sendEvent = new ManualResetEvent(false);
            heartbeatAsker.SetEntity(mqEntity);

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("monitoree");
                type.ConvertToString().Should().Be("PING");
                message.Should().BeNull();
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask, "Ping has not sent");
            sendEvent.Reset();
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ping has not sent again");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void heartbeat_asker_on_not_responded_ping()
        {
            var heartbeatAsker = new HeartbeatAsker(new[] {new Identity("monitoree"), },
                TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(1000));
            var mqEntity = new MqEntityStub();
            var offlineEvent = new ManualResetEvent(false);
            heartbeatAsker.SetEntity(mqEntity);

            heartbeatAsker.EntityOffline += identity =>
            {
                identity.ConvertToString().Should().Be("monitoree");
                offlineEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(offlineEvent, TimeSpan.FromMilliseconds(2000), pollerTask,
                "Entity did not went offline");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void heartbeat_asker_on_removing_from_monitors()
        {
            var heartbeatAsker = new HeartbeatAsker(new[] {new Identity("monitoree"), },
                TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(2000));
            var mqEntity = new MqEntityStub();
            var offlineEvent = new ManualResetEvent(false);
            var sendEvent = new ManualResetEvent(false);
            heartbeatAsker.SetEntity(mqEntity);

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("monitoree");
                type.ConvertToString().Should().Be("PING");
                message.Should().BeNull();
                sendEvent.Set();
            };

            heartbeatAsker.EntityOffline += identity =>
            {
                identity.ConvertToString().Should().Be("monitoree");
                offlineEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask, "Ping has not sent");
            heartbeatAsker.RemoveEntityMonitoring(new Identity("monitoree"));

            Helper.ShouldNotReceiveSignal(offlineEvent, TimeSpan.FromMilliseconds(3000), pollerTask,
                "Offline event was called");

            sendEvent.Reset();
            Helper.ShouldNotReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Sent ping to entity again");
            Helper.CheckFault(pollerTask);
        }
    }
}