﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class WorkDistributorTests : TestBase
    {
        [Fact]
        void work_distributor_on_no_work()
        {
            var mqEntity = new MqEntityStub();
            var sendEvent = new ManualResetEvent(false);
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                type.Should().Be(new NetMQFrame("NOWORK"));
                identity.Should().Be(new NetMQFrame("worker"));
                sendEvent.Set();
            };

            var workDistributor = new LazyWorkDistributor();
            var middlewares = workDistributor.GetReceiveMiddlewares();
            middlewares.FirstOrDefault(x => x.MessageType == new NetMQFrame("READY"))
                .Callback(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            if (!sendEvent.WaitOne(0))
                throw new Exception("send event has not been called");
        }

        [Fact]
        void work_distributor_on_works()
        {
            var mqEntity = new MqEntityStub();
            var workEvent = new ManualResetEvent(false);
            var noworkEvent = new ManualResetEvent(false);

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                if (!workEvent.WaitOne(0))
                {
                    type.Should().Be(new NetMQFrame("WORK"));
                    identity.Should().Be(new NetMQFrame("worker"));
                    message.Pop().ConvertToInt64().Should().BeOfType(typeof(long));
                    message.Should().ContainInOrder(new NetMQFrame("some work"));
                    workEvent.Set();
                }
                else
                {
                    type.Should().Be(new NetMQFrame("NOWORK"));
                    identity.Should().Be(new NetMQFrame("worker"));
                    noworkEvent.Set();
                }
            };

            var workDistributor = new LazyWorkDistributor();
            workDistributor.SetEntity(mqEntity);
            
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);
            
            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;

            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!workEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work has not been passed");

            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!noworkEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("nowork has not been passed");
        }

        [Fact]
        void work_distributor_on_work_result()
        {
            var mqEntity = new MqEntityStub();
            var workEvent = new ManualResetEvent(false);
            var resultEvent = new ManualResetEvent(false);
            long index = 0;

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                type.Should().Be(new NetMQFrame("WORK"));
                identity.Should().Be(new NetMQFrame("worker"));
                index = message.Pop().ConvertToInt64();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                workEvent.Set();
            };

            mqEntity.OnMessageReceive += (identity, message) =>
            {
                identity.Should().Be(new NetMQFrame("worker"));
                message.Pop().ConvertToString().Should().Be("topic");
                message.Pop().ConvertToInt64().Should().Be(index);
                message.Pop().ConvertToString().Should().Be("some result");
                resultEvent.Set();
            };

            var workDistributor = new LazyWorkDistributor();
            workDistributor.SetEntity(mqEntity);
            
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);
            
            var readyMiddleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            var resultMiddleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("WORKRESULT")).Callback;

            readyMiddleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!workEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work has not been passed");

            var result = new NetMQMessage();
            result.Append(index);
            result.Append("topic");
            result.Append("some result");
            resultMiddleware(mqEntity, new Identity("worker"), new MessageType("WORKRESULT"), result);
            if (!resultEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("result has not been passed");
        }

        [Fact]
        void work_distributor_on_work_priority()
        {
            var mqEntity = new MqEntityStub();
            var work1Event = new ManualResetEvent(false);
            var work2Event = new ManualResetEvent(false);
            var work3Event = new ManualResetEvent(false);
            var hitNumber = 0;

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                switch (hitNumber)
                {
                    case 0:
                        type.Should().Be(new NetMQFrame("WORK"));
                        identity.Should().Be(new NetMQFrame("worker"));
                        message.Should().ContainInOrder(new NetMQFrame("work1"));
                        work1Event.Set();
                        ++hitNumber;
                        break;
                    case 1:
                        type.Should().Be(new NetMQFrame("WORK"));
                        identity.Should().Be(new NetMQFrame("worker"));
                        message.Should().ContainInOrder(new NetMQFrame("work2"));
                        work2Event.Set();
                        ++hitNumber;
                        break;
                    case 2:
                        type.Should().Be(new NetMQFrame("WORK"));
                        identity.Should().Be(new NetMQFrame("worker"));
                        message.Should().ContainInOrder(new NetMQFrame("work3"));
                        work3Event.Set();
                        ++hitNumber;
                        break;
                }
            };

            var workDistributor = new LazyWorkDistributor();
            workDistributor.SetEntity(mqEntity);
            
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("work2")}), 2);
            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("work3")}), 2);
            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("work1")}), 1);
            
            Thread.Sleep(100);

            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!work1Event.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work1 has not been passed");

            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!work2Event.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work2 has not been passed");

            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!work3Event.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work3 has not been passed");
        }

        [Fact]
        void work_distributor_on_non_responding_worker()
        {
            var mqEntity = new MqEntityStub();
            var workEvent = new ManualResetEvent(false);

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                type.ConvertToString().Should().Be("WORK");
                identity.ConvertToString().Should().Be("worker");
                message.Pop().ConvertToInt64().Should().BeOfType(typeof(long));
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                workEvent.Set();
            };

            var workDistributor = new LazyWorkDistributor();
            workDistributor.SetEntity(mqEntity);
            
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);
            
            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;

            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!workEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work has not been passed");

            workDistributor.EntityOffline(new Identity("worker"));
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            if (!workEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work has not been passed");
        }

        [Fact]
        void work_distributor_on_load_balanced_nodes()
        {
            var mqEntity = new MqEntityStub();
            var workEvent = new ManualResetEvent(false);
            var resultEvent = new ManualResetEvent(false);
            long index = 0;

            mqEntity.SendMessageReturnIdentity = new Identity("worker");
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.Should().Be(new NetMQFrame("WORK"));
                index = message.Pop().ConvertToInt64();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                workEvent.Set();
            };

            mqEntity.OnMessageReceive += (identity, message) =>
            {
                identity.Should().Be(new NetMQFrame("worker"));
                message.Pop().ConvertToString().Should().Be("topic");
                message.Pop().ConvertToInt64().Should().Be(index);
                message.Pop().ConvertToString().Should().Be("some result");
                resultEvent.Set();
            };

            var workDistributor = new InstantWorkDistributor();
            workDistributor.SetEntity(mqEntity);
            
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);
            
            workDistributor.SendWork(new NetMQMessage(new[] {new NetMQFrame("some work")})).Result.Should().Be(index);

            var resultMiddleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("WORKRESULT")).Callback;

            if (!workEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("work has not been passed");

            var result = new NetMQMessage();
            result.Append(index);
            result.Append("topic");
            result.Append("some result");
            resultMiddleware(mqEntity, new Identity("worker"), new MessageType("WORKRESULT"), result);
            if (!resultEvent.WaitOne(TimeSpan.FromMilliseconds(0)))
                throw new Exception("result has not been passed");
        }

        [Fact]
        void instant_work_distributor_on_work_failure()
        {
            var mqEntity = new MqEntityStub();

            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new InstantWorkDistributor(TimeSpan.FromSeconds(1), 1);
            workDistributor.SetEntity(mqEntity);

            long workIndex = -11;
            var failureEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                failureEvent.Set();
                index.Should().Be(workIndex);
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workIndex = workDistributor.SendWork(new NetMQMessage(new[] {new NetMQFrame("some work")})).Result;
            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(2), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void instant_work_distributor_on_work_retry()
        {
            var mqEntity = new MqEntityStub();

            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new InstantWorkDistributor(TimeSpan.FromSeconds(1), 2);
            workDistributor.SetEntity(mqEntity);
            long workIndex = -1;

            var failureEvent = new ManualResetEvent(false);
            var changeIndexEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                index.Should().Be(workIndex);
                failureEvent.Set();
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            workDistributor.OnWorkIndexChange += (before, after) =>
            {
                before.Should().Be(workIndex);
                workIndex = after;
                changeIndexEvent.Set();
            };

            var sendEvent = new ManualResetEvent(false);
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.ConvertToString().Should().Be("WORK");
                message.Pop();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workIndex = workDistributor.SendWork(new NetMQMessage(new[] {new NetMQFrame("some work")})).Result;

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send event has not been raised");
            sendEvent.Reset();
            Helper.ShouldReceiveSignal(changeIndexEvent, TimeSpan.FromSeconds(2), pollerTask,
                "index changed event has not been raised");
            changeIndexEvent.Reset();
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(0), pollerTask,
                "failure event has been raised before max retry");

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send event has not been raised again");
            Helper.ShouldNotReceiveSignal(changeIndexEvent, TimeSpan.FromSeconds(2), pollerTask,
                "index changed event has been raised again");
            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(2), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void instant_work_distributor_on_offline_entity()
        {
            var mqEntity = new MqEntityStub();

            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new InstantWorkDistributor(TimeSpan.FromSeconds(100), 2);
            workDistributor.SetEntity(mqEntity);

            long workIndex = -1;
            var failureEvent = new ManualResetEvent(false);
            var changeIndexEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                failureEvent.Set();
                index.Should().Be(workIndex);
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            workDistributor.OnWorkIndexChange += (before, after) =>
            {
                before.Should().Be(workIndex);
                workIndex = after;
                changeIndexEvent.Set();
            };

            var sendEvent = new ManualResetEvent(false);
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.ConvertToString().Should().Be("WORK");
                message.Pop();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workIndex = workDistributor.SendWork(new NetMQMessage(new[] {new NetMQFrame("some work")})).Result;

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(1), pollerTask,
                "send event has not been raised");
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(1), pollerTask,
                "failure event has been raised before max retry");

            sendEvent.Reset();
            workDistributor.EntityOffline(new Identity("worker"));

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(1), pollerTask,
                "send event has not been raised");
            Helper.ShouldReceiveSignal(changeIndexEvent, TimeSpan.FromSeconds(1), pollerTask,
                "index change event has not been raised again");
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(1), pollerTask,
                "failure event has been raised before max retry");

            sendEvent.Reset();
            changeIndexEvent.Reset();
            workDistributor.EntityOffline(new Identity("worker"));

            Helper.ShouldNotReceiveSignal(sendEvent, TimeSpan.FromSeconds(1), pollerTask,
                "send event has not been raised");
            Helper.ShouldNotReceiveSignal(changeIndexEvent, TimeSpan.FromSeconds(1), pollerTask,
                "index change has not been raised again");
            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(1), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void lazy_work_distributor_on_work_failure()
        {
            var mqEntity = new MqEntityStub();

            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new LazyWorkDistributor(TimeSpan.FromSeconds(1), 1);
            workDistributor.SetEntity(mqEntity);

            var failureEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                failureEvent.Set();
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);

            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(2), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void lazy_work_distributor_on_work_retry()
        {
            var mqEntity = new MqEntityStub();

            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new LazyWorkDistributor(TimeSpan.FromSeconds(1), 2);
            workDistributor.SetEntity(mqEntity);

            var failureEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                failureEvent.Set();
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            var sendEvent = new ManualResetEvent(false);
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("worker");
                type.ConvertToString().Should().Be("WORK");
                message.Pop();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);
            
            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send event has not been raised");
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(2), pollerTask,
                "failure event has been raised before max retry");

            sendEvent.Reset();
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send event has not been raised again");
            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(2), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void lazy_work_distributor_on_offline_entity()
        {
            var mqEntity = new MqEntityStub();
            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new LazyWorkDistributor(TimeSpan.FromSeconds(100), 2);
            workDistributor.SetEntity(mqEntity);

            var failureEvent = new ManualResetEvent(false);
            workDistributor.OnWorkFailure += (index, work) =>
            {
                failureEvent.Set();
                work.Select(x => x.ConvertToString()).Should().ContainInOrder("some work");
            };

            var sendEvent = new ManualResetEvent(false);
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("worker");
                type.ConvertToString().Should().Be("WORK");
                message.Pop();
                message.Should().ContainInOrder(new NetMQFrame("some work"));
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}));
            Thread.Sleep(100);

            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event has not been raised");
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(3), pollerTask,
                "failure event has been raised before max retry");

            sendEvent.Reset();
            workDistributor.EntityOffline(new Identity("worker"));
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event has not been raised");
            Helper.ShouldNotReceiveSignal(failureEvent, TimeSpan.FromSeconds(3), pollerTask,
                "failure event has been raised before max retry");

            sendEvent.Reset();
            workDistributor.EntityOffline(new Identity("worker"));

            Helper.ShouldNotReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event has not been raised");
            Helper.ShouldReceiveSignal(failureEvent, TimeSpan.FromSeconds(3), pollerTask,
                "failure event has not been raised");
        }

        [Fact]
        void lazy_work_distributor_on_queue_after()
        {
            var mqEntity = new MqEntityStub();
            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new LazyWorkDistributor(TimeSpan.FromSeconds(100), 2);
            workDistributor.SetEntity(mqEntity);

            var sendEvent = new ManualResetEvent(false);
            bool first = true;
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                if (first)
                {
                    identity.ConvertToString().Should().Be("worker");
                    type.ConvertToString().Should().Be("NOWORK");
                    first = false;
                }
                else
                {
                    identity.ConvertToString().Should().Be("worker");
                    type.ConvertToString().Should().Be("WORK");
                    message.Pop();
                    message.Should().ContainInOrder(new NetMQFrame("some work"));
                }
                
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}), queueAfter: TimeSpan.FromSeconds(2));

            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event not has been raised");

            Thread.Sleep(2500);
            
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3000), pollerTask,
                "send event has not been raised");
        }
        
        [Fact]
        void lazy_work_distributor_on_queue_after_queueAfter()
        {
            var mqEntity = new MqEntityStub();
            mqEntity.SendMessageReturnIdentity = new Identity("worker");

            var workDistributor = new LazyWorkDistributor(TimeSpan.FromSeconds(100), 2);
            workDistributor.SetEntity(mqEntity);

            var sendEvent = new ManualResetEvent(false);
            bool first = true;
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                if (first)
                {
                    identity.ConvertToString().Should().Be("worker");
                    type.ConvertToString().Should().Be("WORK");
                    message.Pop();
                    message.Should().ContainInOrder(new NetMQFrame("some work2"));
                    first = false;
                }
                else
                {
                    identity.ConvertToString().Should().Be("worker");
                    type.ConvertToString().Should().Be("WORK");
                    message.Pop();
                    message.Should().ContainInOrder(new NetMQFrame("some work"));
                }
                
                sendEvent.Set();
            };

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work")}), queueAfter: TimeSpan.FromSeconds(2));
            workDistributor.EnqueueWork(new NetMQMessage(new[] {new NetMQFrame("some work2")}));
            
            Thread.Sleep(1000);

            var middleware = workDistributor.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("READY")).Callback;
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event not has been raised");

            Thread.Sleep(2500);
            
            middleware(mqEntity, new Identity("worker"), new MessageType("READY"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(3), pollerTask,
                "send event has not been raised");
        }
    }
}