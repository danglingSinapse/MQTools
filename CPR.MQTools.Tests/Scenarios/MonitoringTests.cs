using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ.Sockets;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace CPR.MQTools.Tests.Scenarios
{
    public class MonitoringTests : TestBase
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public MonitoringTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        void cautious_data_publisher_test()
        {
            var entity = new MqEntity(new RouterSocket());
            var dataSource = new DataSourceStub();
            var dataVersion = new DataVersionControllerStub();
            entity.AddFunctionality(new CautiousDataPublisher(dataSource, dataVersion));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("CautiousDataPublisher");
        }
        
        [Fact]
        void cautious_data_subscriber_test()
        {
            var entity = new MqEntity(new RouterSocket());
            var dataVersion = new DataVersionControllerStub();
            entity.AddFunctionality(new CautiousDataSubscriber(dataVersion));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("CautiousDataSubscriber");
        }

        [Fact]
        void discovery_device_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new UdpDiscoveryDevice(9999, "admin"));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("UdpDiscoveryDevice");
        }

        [Fact]
        void eager_worker_device_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new EagerWorker(new MessageType("111")));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("EagerWorker");
        }

        [Fact]
        void existence_broadcaster_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new UdpExistenceBroadcaster(9999, "admin", new Identity("123")));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("UdpExistenceBroadcaster");
        }
        
        [Fact]
        void heartbeat_answerer_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new HeartbeatAnswerer());
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("HeartbeatAnswerer");
        }
        
        [Fact]
        void heartbeat_asker_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new HeartbeatAsker());
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("HeartbeatAsker");
        }
        
        [Fact]
        void instant_work_distributor_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new InstantWorkDistributor());
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("InstantWorkDistributor");
        }
        
        [Fact]
        void lazy_work_distributor_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new LazyWorkDistributor());
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("LazyWorkDistributor");
        }

        [Fact]
        void relaxed_worker_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new RelaxedWorker());
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("RelaxedWorker");
        }
        
        [Fact]
        void sensitive_data_provider_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new SensitiveDataProvider("somedata", "thisdata"));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("SensitiveDataProvider");
        }
        
        [Fact]
        void sensitive_data_seeker_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new SensitiveDataSeeker("somedata"));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("SensitiveDataSeeker");
        }

        [Fact]
        void functions_with_name_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new SensitiveDataSeeker("somedata"), "mySensitiveFunction");
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(2);
            infos.Should().ContainKey("mySensitiveFunction");
        }

        [Fact]
        void duplicated_function_test()
        {
            var entity = new MqEntity(new RouterSocket());
            entity.AddFunctionality(new SensitiveDataSeeker("somedata"));
            entity.AddFunctionality(new SensitiveDataSeeker("somedata"));
            entity.RunAsync();
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var infos = entity.GetHealthInformation().Result;
            infos.Count.Should().Be(3);
            infos.Should().ContainKey("SensitiveDataSeeker_0");
            infos.Should().ContainKey("SensitiveDataSeeker_1");
        }

        [Fact]
        void http_monitoring_test()
        {
            var entity1 = new MqEntity(new RouterSocket(), entityName: "entity1");
            var dataSource = new DataSourceStub();
            var dataVersion = new DataVersionControllerStub();
            entity1.AddFunctionality(new CautiousDataPublisher(dataSource, dataVersion));
            entity1.AddFunctionality(new CautiousDataSubscriber(dataVersion));
            entity1.AddFunctionality(new UdpDiscoveryDevice(9999, "admin"));
            entity1.AddFunctionality(new EagerWorker(new MessageType("111")));
            
            var entity2 = new MqEntity(new RouterSocket(), entity1.Poller, entityName: "entity2");
            entity2.AddFunctionality(new UdpExistenceBroadcaster(9999, "admin", new Identity("123")));
            entity2.AddFunctionality(new HeartbeatAnswerer());
            entity2.AddFunctionality(new HeartbeatAsker());
            entity2.AddFunctionality(new InstantWorkDistributor());

            var entity3 = new MqEntity(new RouterSocket(), entity1.Poller, entityName: "entity3");
            entity3.AddFunctionality(new LazyWorkDistributor());
            entity3.AddFunctionality(new RelaxedWorker());
            entity3.AddFunctionality(new SensitiveDataProvider("somedata", "thisdata"));
            entity3.AddFunctionality(new SensitiveDataSeeker("somedata"));

            entity1.RunAsync();
            while (!entity1.Poller.IsRunning)
                Thread.Sleep(100);

            var httpPort = PortGenerator.GetNext();
            var monitoring = new MonitoringDevice(new[] {entity1, entity2, entity3}, httpPort);

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"http://localhost:{httpPort}");
                var responseStr = httpClient.GetStringAsync("/health").Result;
                var response = JsonConvert.DeserializeObject<dynamic>(responseStr);
                var m1 = response.entity1.CautiousDataPublisher;
                var m2 = response.entity1.CautiousDataSubscriber;
                var m3 = response.entity1.DiscoveryDevice;
                var m4 = response.entity1.EagerWorker;
                
                var m5 = response.entity2.ExistenceBroadcaster;
                var m6 = response.entity2.HeartbeatAnswerer;
                var m7 = response.entity2.HeartbeatAsker;
                var m8 = response.entity2.InstantWorkDistributor;
                
                var m9 = response.entity2.LazyWorkDistributor;
                var m10 = response.entity2.RelaxedWorker;
                var m11 = response.entity2.SensitiveDataProvider;
                var m12 = response.entity2.SensitiveDataSeeker;
            }
        }
        
//        [Fact]
//        async Task http_monitoring_auto_discover_entities_test()
//        {
//            var entity1 = new MqEntity(new RouterSocket(), entityName: "entity1");
//            var dataSource = new DataSourceStub();
//            var dataVersion = new DataVersionControllerStub();
//            entity1.AddFunctionality(new CautiousDataPublisher(dataSource, dataVersion));
//            entity1.AddFunctionality(new CautiousDataSubscriber(dataVersion));
//            entity1.AddFunctionality(new DiscoveryDevice(9999, "admin"));
//            entity1.AddFunctionality(new EagerWorker(new MessageType("111")));
//            entity1.RunAsync();
//
//            var entity2 = new MqEntity(new RouterSocket(), entity1.Poller, entityName: "entity2");
//            entity2.AddFunctionality(new ExistenceBroadcaster(9999, "admin", new Identity("123")));
//            entity2.AddFunctionality(new HeartbeatAnswerer());
//            entity2.AddFunctionality(new HeartbeatAsker());
//            entity2.AddFunctionality(new InstantWorkDistributor());
//            
//            var entity3 = new MqEntity(new RouterSocket(), entity1.Poller, entityName: "entity3");
//            entity3.AddFunctionality(new LazyWorkDistributor());
//            entity3.AddFunctionality(new RelaxedWorker());
//            entity3.AddFunctionality(new SensitiveDataProvider("somedata", "thisdata"));
//            entity3.AddFunctionality(new SensitiveDataSeeker("somedata"));
//
//            var httpPort = PortGenerator.GetNext();
//            var monitoring = new MonitoringDevice(monitoringPort: httpPort);
//
//            using (var httpClient = new HttpClient())
//            {
//                httpClient.BaseAddress = new Uri($"http://localhost:{httpPort}");
//                var responseStr = await httpClient.GetStringAsync("/health");
//
//                var response = JsonConvert.DeserializeObject<dynamic>(responseStr);
//                var m1 = response.entity1.CautiousDataPublisher;
//                var m2 = response.entity1.CautiousDataSubscriber;
//                var m3 = response.entity1.DiscoveryDevice;
//                var m4 = response.entity1.EagerWorker;
//                
//                var m5 = response.entity2.ExistenceBroadcaster;
//                var m6 = response.entity2.HeartbeatAnswerer;
//                var m7 = response.entity2.HeartbeatAsker;
//                var m8 = response.entity2.InstantWorkDistributor;
//                
//                var m9 = response.entity2.LazyWorkDistributor;
//                var m10 = response.entity2.RelaxedWorker;
//                var m11 = response.entity2.SensitiveDataProvider;
//                var m12 = response.entity2.SensitiveDataSeeker;
//            }
//        }
    }
}