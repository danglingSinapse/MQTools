﻿using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class FunctionBaseTests : TestBase
    {
        [Fact]
        private void function_base_derivations_should_setup_passed_modules_correctly()
        {
            typeof(MqFunctionBase).Should().BeAbstract();

            var mqEntityStub = new MqEntityStub();

            var discoveryDevice = new UdpDiscoveryDevice(9999, "");
            discoveryDevice.SetEntity(mqEntityStub);
            discoveryDevice.Entity.Should().Be(mqEntityStub);

            var eagerWorker = new EagerWorker();
            eagerWorker.SetEntity(mqEntityStub);
            eagerWorker.Entity.Should().Be(mqEntityStub);

            var existanceBroadcaster = new UdpExistenceBroadcaster(9999, "");
            existanceBroadcaster.SetEntity(mqEntityStub);
            existanceBroadcaster.Entity.Should().Be(mqEntityStub);

            var heartbeatChecker = new HeartbeatAsker();
            heartbeatChecker.SetEntity(mqEntityStub);
            heartbeatChecker.Entity.Should().Be(mqEntityStub);

            var heartbeatInformer = new HeartbeatAsker();
            heartbeatInformer.SetEntity(mqEntityStub);
            heartbeatInformer.Entity.Should().Be(mqEntityStub);

            var workDistributor = new HeartbeatAsker();
            workDistributor.SetEntity(mqEntityStub);
            workDistributor.Entity.Should().Be(mqEntityStub);
        }
    }
}