﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class ExistanceBroadcasterTests : TestBase
    {
        [Fact]
        private void existance_broadcaster_on_manual_identity()
        {
            var port = PortGenerator.GetNext();
            var existanceBroadcaster = new UdpExistenceBroadcaster(port, "service", new Identity("serviceABC"));
            var mqEntity = new MqEntityStub();
            existanceBroadcaster.SetEntity(mqEntity);
            var beacon = new UdpBeacon(port);
            beacon.Subscribe("service", mqEntity.PollerTaskFactory);
            
            var receiveEvent = new ManualResetEvent(false);
            beacon.OnReceive += (sender, message) =>
            {
                receiveEvent.Set();
                message.Should().Be("service@serviceABC");
            };
            existanceBroadcaster.EntityBound(port);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(1200), pollerTask,
                "Beacon not received");
            receiveEvent.Reset();
            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(1200), pollerTask,
                "Beacon not received again");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void existance_broadcaster_on_automatic_identity()
        {
            var port = PortGenerator.GetNext();
            var existanceBroadcaster = new UdpExistenceBroadcaster(port, "service");
            var mqEntity = new MqEntityStub();
            existanceBroadcaster.SetEntity(mqEntity);

            var beacon = new UdpBeacon(port);
            beacon.Subscribe("service", mqEntity.PollerTaskFactory);
            var receiveEvent = new ManualResetEvent(false);
            var ip = NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(x =>
                    x.OperationalStatus == OperationalStatus.Up &&
                    x.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                ?.GetIPProperties().UnicastAddresses
                .FirstOrDefault(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                ?.Address.ToString();

            beacon.OnReceive += (sender, message) =>
            {
                receiveEvent.Set();
                message.Should().Be($"service@{ip}:{port}");
            };

            existanceBroadcaster.EntityBound(port);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);
            
            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(1200), pollerTask,
                "Beacon not received");
            Helper.CheckFault(pollerTask);
        }
    }
}