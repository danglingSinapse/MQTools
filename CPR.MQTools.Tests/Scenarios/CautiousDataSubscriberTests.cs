﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class CautiousDataSubscriberTests : TestBase
    {
        [Fact]
        void cautious_subscriber_on_passing_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };
            
            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                receiveEvent.Set();
            };

            var storeEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                storeEvent.Set();
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
            receiveEvent.Reset();
            storeEvent.Reset();

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
            
            if (!storeEvent.WaitOne(1000))
                throw new Exception("store data method has not been called");
        }

        [Fact]
        void cautious_subscriber_on_multi_providers()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                if (id == "publisher1")
                    return new DataInfo {DataIndex = 3, LastCreationTimestamp = 10};
                if (id == "publisher2")
                    return new DataInfo {DataIndex = 5, LastCreationTimestamp = 10};

                throw new Exception($"unexpected publisher id {id} has been passed");
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receive1Event = new ManualResetEvent(false);
            var receive2Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                var publisherId = identity.ConvertToString();
                if (publisherId == "publisher1")
                {
                    message.Pop().ConvertToString().Should().Be("myData");
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                    receive1Event.Set();
                }
                else if (publisherId == "publisher2")
                {
                    message.Pop().ConvertToString().Should().Be("yourData");
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("10", "world");
                    receive2Event.Set();
                }
                else
                    throw new Exception($"passed unknown publisher {publisherId}");
            };

            var store1Event = new ManualResetEvent(false);
            var store2Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                if (id == "publisher1")
                {
                    info.DataIndex.Should().Be(4);
                    info.LastCreationTimestamp.Should().Be(10);
                    store1Event.Set();
                }
                else if (id == "publisher2")
                {
                    info.DataIndex.Should().Be(6);
                    info.LastCreationTimestamp.Should().Be(20);
                    store2Event.Set();
                }
                else
                    throw new Exception($"passed unknown publisher {id}");
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher1"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher1"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receive1Event.WaitOne(1000))
                throw new Exception("data from publisher1 did not received");

            if (!store1Event.WaitOne(1000))
                throw new Exception("store event for publisher1 did not received");

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher2"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher2"), new MqFrame((long) 6), new MqFrame((long) 20),
                    new NetMQFrame("yourData"), new NetMQFrame("10"), new NetMQFrame("world")
                }));

            store2Event.Reset();
            if (!receive2Event.WaitOne(2000))
                throw new Exception("data from publisher2 did not received");
            
            if (!store2Event.WaitOne(2000))
                throw new Exception("store event for publisher2 did not received");
        }

        [Fact]
        void cautious_subscriber_on_missing_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController, fetchDataTimeout: TimeSpan.FromMilliseconds(1500));
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            var data4Event = new ManualResetEvent(false);
            var data5Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                var index = message.Pop().ConvertToString();
                switch (index)
                {
                    case "3":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("!");
                        data3Event.Set();
                        break;
                    case "4":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("world");
                        data4Event.Set();
                        break;
                    case "5":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("hello");
                        data5Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {index}");
                }

                receiveEvent.Set();
            };

            var store5Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                switch (info.DataIndex)
                {
                    case 3:
                        info.LastCreationTimestamp.Should().Be(10);
                        break;
                    case 4:
                        info.LastCreationTimestamp.Should().Be(20);
                        break;
                    case 5:
                        store5Event.Set();
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                    default:
                        throw new Exception($"received unexpected data index {info.DataIndex}");
                }
            };

            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(3);
                message.Pop().ConvertToInt64().Should().Be(4);
                fetchEvent.Set();
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("5"), new NetMQFrame("hello")
                }));

            if (receiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");
            if (!fetchEvent.WaitOne(1000))
                throw new Exception("subscriber did not fetch for data.");
            fetchEvent.Reset();

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(100))
                throw new Exception("data received before fetching missing data");
            if (fetchEvent.WaitOne(1000))
                throw new Exception("subscriber did fetch again for data.");

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 3), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("!")
                }));

            if (!data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received");
            if (!data4Event.WaitOne(1000))
                throw new Exception("data 4 was not received");
            if (!data5Event.WaitOne(1000))
                throw new Exception("data 5 was not received");
            if (!store5Event.WaitOne(1000))
                throw new Exception("store 5 was not called");
            if (fetchEvent.WaitOne(1000))
                throw new Exception("subscriber did fetch again for data.");
        }

        [Fact]
        void cautious_subscriber_on_missing_multiple_gaped_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            var data4Event = new ManualResetEvent(false);
            var data5Event = new ManualResetEvent(false);
            var data6Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                var index = message.Pop().ConvertToString();
                switch (index)
                {
                    case "3":
                        data3Event.Set();
                        break;
                    case "4":
                        data4Event.Set();
                        break;
                    case "5":
                        data5Event.Set();
                        break;
                    case "6":
                        data6Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {index}");
                }

                receiveEvent.Set();
            };

            var store6Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                switch (info.DataIndex)
                {
                    case 3:
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                    case 4:
                        info.LastCreationTimestamp.Should().Be(40);
                        break;
                    case 5:
                        info.LastCreationTimestamp.Should().Be(50);
                        break;
                    case 6:
                        info.LastCreationTimestamp.Should().Be(60);
                        store6Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {info.DataIndex}");
                }
            };

            var firstFetch = true;
            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(firstFetch ? 3 : 5);
                message.Pop().ConvertToInt64().Should().Be(firstFetch ? 3 : 5);
                fetchEvent.Set();
                firstFetch = false;
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 40),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(100))
                throw new Exception("data received before fetching missing data");
            
            if (!fetchEvent.WaitOne(1000))
                throw new Exception("data was not fetched by subscriber.");
            fetchEvent.Reset();

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 6), new MqFrame((long) 60),
                    new NetMQFrame("myData"), new NetMQFrame("6"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");
            
            if (fetchEvent.WaitOne(1000))
                throw new Exception("data was fetched again.");
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 3), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("world")
                }));
            
            if (!data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received.");
            
            if (!data4Event.WaitOne(1000))
                throw new Exception("data 4 was not received.");
            
            if (data6Event.WaitOne(1000))
                throw new Exception("data 6 was received in wrong order.");
            
            if (!fetchEvent.WaitOne(1000))
                throw new Exception("data was not fetched for data 5.");
            
            fetchEvent.Reset();

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 50),
                    new NetMQFrame("myData"), new NetMQFrame("5"), new NetMQFrame("world")
                }));

            
            if (!data5Event.WaitOne(1000))
                throw new Exception("data 3 was not received");
            
            if (!data6Event.WaitOne(1000))
                throw new Exception("data 4 was not received");
            
            if (!store6Event.WaitOne(1000))
                throw new Exception("store 5 was not called");
            
            if (fetchEvent.WaitOne(1000))
                throw new Exception("data fetch was called again!");
        }
        
        [Fact]
        void cautious_subscriber_on_fetching_time_out()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            var data4Event = new ManualResetEvent(false);
            var data5Event = new ManualResetEvent(false);
            var data6Event = new ManualResetEvent(false);
            var data7Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                var index = message.Pop().ConvertToString();
                switch (index)
                {
                    case "3":
                        data3Event.Set();
                        break;
                    case "4":
                        data4Event.Set();
                        break;
                    case "5":
                        data5Event.Set();
                        break;
                    case "6":
                        data6Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {index}");
                }

                receiveEvent.Set();
            };

            var store6Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                switch (info.DataIndex)
                {
                    case 3:
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                    case 4:
                        info.LastCreationTimestamp.Should().Be(40);
                        break;
                    case 5:
                        info.LastCreationTimestamp.Should().Be(50);
                        break;
                    case 6:
                        info.LastCreationTimestamp.Should().Be(60);
                        store6Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {info.DataIndex}");
                }
            };

            var firstFetch = true;
            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(3);
                message.Pop().ConvertToInt64().Should().Be(firstFetch ? 4 : 3);
                fetchEvent.Set();
                firstFetch = false;
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 50),
                    new NetMQFrame("myData"), new NetMQFrame("5"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(100))
                throw new Exception("data received before fetching missing data");
            
            if (!fetchEvent.WaitOne(1000))
                throw new Exception("data was not fetched by subscriber.");
            fetchEvent.Reset();

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 40),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");
            
            if (fetchEvent.WaitOne(1000))
                throw new Exception("data was fetched again.");
            Thread.Sleep(1200);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 6), new MqFrame((long) 60),
                    new NetMQFrame("myData"), new NetMQFrame("6"), new NetMQFrame("world")
                }));
            
            if (receiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");
            
            if (!fetchEvent.WaitOne(1000))
                throw new Exception("data was not fetched again.");
            fetchEvent.Reset();
            
            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 3), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("world")
                }));
            
            if (!data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received.");
            
            if (!data4Event.WaitOne(1000))
                throw new Exception("data 4 was not received.");

            if (!data5Event.WaitOne(1000))
                throw new Exception("data 5 was not received.");

            if (!data6Event.WaitOne(1000))
                throw new Exception("data 6 was received in wrong order.");

            if (!store6Event.WaitOne(1000))
                throw new Exception("store event was not called.");

            if (fetchEvent.WaitOne(1000))
                throw new Exception("data was fetched again.");
        }
        
        
        [Fact]
        void cautious_subscriber_on_unsuccessful_fetching_of_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;
            
            var missedDataEvent = new ManualResetEvent(false);
            subscriber.OnMissedData += (publisher, index) =>
            {
                publisher.ConvertToString().Should().Be("publisher");
                index.Should().Be(4);
                missedDataEvent.Set();
            };
            
            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            var data4Event = new ManualResetEvent(false);
            var data5Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                var index = message.Pop().ConvertToString();
                switch (index)
                {
                    case "3":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("!");
                        data3Event.Set();
                        break;
                    case "4":
                        data4Event.Set();
                        break;
                    case "5":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("hello");
                        data5Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {index}");
                }

                receiveEvent.Set();
            };

            var store5Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                switch (info.DataIndex)
                {
                    case 3:
                        info.LastCreationTimestamp.Should().Be(10);
                        break;
                    case 4:
                        info.LastCreationTimestamp.Should().Be(20);
                        break;
                    case 5:
                        store5Event.Set();
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                    default:
                        throw new Exception($"received unexpected data index {info.DataIndex}");
                }
            };

            var firstFetch = true;
            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(3);
                message.Pop().ConvertToInt64().Should().Be(firstFetch ? 4 : 3);
                fetchEvent.Set();
                firstFetch = false;
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("5"), new NetMQFrame("hello")
                }));

            if (receiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 20),
                    new NetMQFrame("MISS")
                }));

            if (receiveEvent.WaitOne(100))
                throw new Exception("data received before fetching missing data");

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 3), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("!")
                }));

            if (!data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received");
            if (data4Event.WaitOne(1000))
                throw new Exception("data 4 was received");
            if (!missedDataEvent.WaitOne(1000))
                throw new Exception("missing of data 4 was not notified");
            if (!data5Event.WaitOne(1000))
                throw new Exception("data 5 was not received");
            if (!store5Event.WaitOne(1000))
                throw new Exception("store 5 was not called");
        }
        
        [Fact]
        void cautious_subscriber_on_missing_with_multiple_providers()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                if (id == "publisher1")
                    return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
                if (id == "publisher2")
                    return new DataInfo {DataIndex = 5, LastCreationTimestamp = 0};

                throw new Exception($"unexpected publisher id {id} has been passed");
            };

            versionController.OnStoreDataInfo += (id, info) =>
            {
                if (id == "publisher1")
                {
                    switch (info.DataIndex)
                    {
                        case 3:
                            info.LastCreationTimestamp.Should().Be(10);
                            break;
                        case 4:
                            info.LastCreationTimestamp.Should().Be(20);
                            break;
                        case 5:
                            info.LastCreationTimestamp.Should().Be(30);
                            break;
                        default:
                            throw new Exception($"unexpected data index {info.DataIndex} has been passed");
                    }
                }
                else if (id == "publisher2")
                {
                    if (info.DataIndex != 6)
                        throw new Exception($"unexpected data index {info.DataIndex} has been passed");
                    info.LastCreationTimestamp.Should().Be(15);
                }
                else
                {
                    throw new Exception($"unexpected publisher id {id} has been passed");
                }
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var publisher1ReceiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var publisher1Data3Event = new ManualResetEvent(false);
            var publisher1Data4Event = new ManualResetEvent(false);
            var publisher1Data5Event = new ManualResetEvent(false);
            var publisher2DataEvent = new ManualResetEvent(false);

            entity.OnMessageReceive += (identity, message) =>
            {
                var publisher = identity.ConvertToString();
                if (publisher == "publisher1")
                {
                    message.Pop().ConvertToString().Should().Be("myData");
                    var index = message.Pop().ConvertToString();
                    switch (index)
                    {
                        case "3":
                            message.Select(x => x.ConvertToString()).Should().ContainInOrder("!");
                            publisher1Data3Event.Set();
                            break;
                        case "4":
                            message.Select(x => x.ConvertToString()).Should().ContainInOrder("world");
                            publisher1Data4Event.Set();
                            break;
                        case "5":
                            message.Select(x => x.ConvertToString()).Should().ContainInOrder("hello");
                            publisher1Data5Event.Set();
                            break;
                        default:
                            throw new Exception($"received unexpected data index {index}");
                    }

                    publisher1ReceiveEvent.Set();
                }
                else if (publisher == "publisher2")
                {
                    message.Pop().ConvertToString().Should().Be("myData");
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("6", "hello2");
                    publisher2DataEvent.Set();
                }
            };

            var firstFetch = true;
            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher1");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(3);
                message.Pop().ConvertToInt64().Should().Be(firstFetch ? 4 : 3);
                fetchEvent.Set();
                firstFetch = false;
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher1"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher1"), new MqFrame((long) 5), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("5"), new NetMQFrame("hello")
                }));
            
            Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher2"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher2"), new MqFrame((long) 6), new MqFrame((long) 15),
                    new NetMQFrame("myData"), new NetMQFrame("6"), new NetMQFrame("hello2")
                }));
            
            if (publisher1ReceiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");

            if (!publisher2DataEvent.WaitOne(1000))
                throw new Exception("publisher2 did not receive any data");

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher1"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher1"), new MqFrame((long) 4), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("world")
                }));

            if (publisher1ReceiveEvent.WaitOne(1000))
                throw new Exception("data received before fetching missing data");

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher1"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher1"), new MqFrame((long) 3), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("!")
                }));

            if (!publisher1Data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received");
            if (!publisher1Data4Event.WaitOne(1000))
                throw new Exception("data 4 was not received");
            if (!publisher1Data5Event.WaitOne(1000))
                throw new Exception("data 5 was not received");
        }
        
        [Fact]
        void cautious_subscriber_on_reset_data_index()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var publishIndex = 4;
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                receiveEvent.Set();
            };

            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                switch (publishIndex)
                {
                    case 4:
                        info.DataIndex.Should().Be(4);
                        info.LastCreationTimestamp.Should().Be(10);
                        break;
                    case 5:
                        info.DataIndex.Should().Be(5);
                        info.LastCreationTimestamp.Should().Be(20);
                        break;
                    case 6:
                        info.DataIndex.Should().Be(0);
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                }
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
            receiveEvent.Reset();

            Thread.Sleep(100);
            publishIndex = 5;
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");

            Thread.Sleep(100);
            publishIndex = 6;
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 0), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
        }
        
        [Fact]
        void cautious_subscriber_on_missed_and_reset_data_and_index()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 2, LastCreationTimestamp = 0};
            };

            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var fetchEvent = new ManualResetEvent(false);
            var data0Event = new ManualResetEvent(false);
            var data1Event = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                var index = message.Pop().ConvertToString();
                switch (index)
                {
                    case "0":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("!");
                        data3Event.Set();
                        break;
                    case "1":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("world");
                        data0Event.Set();
                        break;
                    case "3":
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("hello");
                        data1Event.Set();
                        break;
                    default:
                        throw new Exception($"received unexpected data index {index}");
                }

                receiveEvent.Set();
            };

            var store1Event = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                switch (info.DataIndex)
                {
                    case 3:
                        info.LastCreationTimestamp.Should().Be(10);
                        break;
                    case 0:
                        info.LastCreationTimestamp.Should().Be(20);
                        break;
                    case 1:
                        store1Event.Set();
                        info.LastCreationTimestamp.Should().Be(30);
                        break;
                    default:
                        throw new Exception($"received unexpected data index {info.DataIndex}");
                }
            };

            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("FETCH");
                message.Pop().ConvertToInt64().Should().Be(0);
                message.Pop().ConvertToInt64().Should().Be(0);
                fetchEvent.Set();
            };
            
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 3), new MqFrame((long) 30),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data 3 not received");
            receiveEvent.Reset();

            Thread.Sleep(100);
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 1), new MqFrame((long) 50),
                    new NetMQFrame("myData"), new NetMQFrame("1"), new NetMQFrame("world")
                }));

            if (receiveEvent.WaitOne(100))
                throw new Exception("data received before fetching missing data");

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 0), new MqFrame((long) 40),
                    new NetMQFrame("myData"), new NetMQFrame("0"), new NetMQFrame("!")
                }));

            if (!data3Event.WaitOne(1000))
                throw new Exception("data 3 was not received");
            if (!data0Event.WaitOne(1000))
                throw new Exception("data 0 was not received");
            if (!data1Event.WaitOne(1000))
                throw new Exception("data 1 was not received");
            if (!store1Event.WaitOne(1000))
                throw new Exception("store 1 was not called");
        }
        
        
        [Fact]
        void cautious_subscriber_on_application_exception()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };
            
            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            var turn = 1;
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                
                if (turn == 1 || turn == 2)
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                else
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("4", "goodbye");
                receiveEvent.Set();
                
                if (turn == 1)
                {
                    ++turn;
                    throw new Exception("some error!");
                }
                ++turn;
            };

            var storeEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                storeEvent.Set();
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("goodbye")
                }));

            if (!receiveEvent.WaitOne(2000))
                throw new Exception("data did not received");
            receiveEvent.Reset();
            storeEvent.Reset();

            if (!receiveEvent.WaitOne(2000))
                throw new Exception("data did not received again");
            receiveEvent.Reset();
            storeEvent.Reset();

            if (!receiveEvent.WaitOne(2000))
                throw new Exception("second data did not received");
        }
        
        [Fact]
        void cautious_subscriber_on_process_failure()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };
            
            var subscriber = new CautiousDataSubscriber(versionController, TimeSpan.FromSeconds(1), 3);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var failureEvent = new ManualResetEvent(false);
            subscriber.OnProcessFailure += async (publisher, index, timestamp, message) =>
            {
                publisher.Should().Be("publisher");
                index.Should().Be(4);
                timestamp.Should().Be(10);
                message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                failureEvent.Set();
            };
            
            var receiveEvent = new ManualResetEvent(false);
            var turn = 1;
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                
                if (turn == 1 || turn == 2 || turn == 3)
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                else
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("4", "goodbye");
                receiveEvent.Set();
                
                if (turn == 1 || turn == 2 || turn == 3)
                {
                    ++turn;
                    throw new Exception("some error!");
                }
                ++turn;
            };

            var storeEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                storeEvent.Set();
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));
            
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("4"), new NetMQFrame("goodbye")
                }));

            if (!receiveEvent.WaitOne(1200))
                throw new Exception("data did not received");
            receiveEvent.Reset();
            storeEvent.Reset();

            if (!receiveEvent.WaitOne(1200))
                throw new Exception("data did not received again");
            receiveEvent.Reset();
            storeEvent.Reset();

            if (!receiveEvent.WaitOne(1200))
                throw new Exception("data did not received again");
            receiveEvent.Reset();
            storeEvent.Reset();
            
            if (failureEvent.WaitOne(1200))
                throw new Exception("failure callback has not been called");

            if (!receiveEvent.WaitOne(2000))
                throw new Exception("second data did not received");
        }
        
        [Fact]
        async Task cautious_subscriber_on_shutting_down_waiting_for_store()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };
            
            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                receiveEvent.Set();
                Thread.Sleep(1000);
            };

            var storeEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                storeEvent.Set();
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
            receiveEvent.Reset();
            Thread.Sleep(100);
            storeEvent.Reset();

            await subscriber.StopGracefully();

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (receiveEvent.WaitOne(2000))
                throw new Exception("data received after shutdown");
            
            if (!storeEvent.WaitOne(2000))
                throw new Exception("store not received after shutdown");
        }

        [Fact]
        async Task cautious_subscriber_on_shutting_down()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };
            
            var subscriber = new CautiousDataSubscriber(versionController);
            var dataCallback = subscriber.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("DATA")).Callback;

            var entity = new MqEntityStub();
            subscriber.SetEntity(entity);

            var receiveEvent = new ManualResetEvent(false);
            entity.OnMessageReceive += (identity, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                message.Pop().ConvertToString().Should().Be("myData");
                message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                receiveEvent.Set();
            };

            var storeEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                id.Should().Be("publisher");
                storeEvent.Set();
            };

            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);

            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 4), new MqFrame((long) 10),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (!receiveEvent.WaitOne(1000))
                throw new Exception("data did not received");
            receiveEvent.Reset();
            Thread.Sleep(100);
            storeEvent.Reset();

            await subscriber.StopGracefully();

            Thread.Sleep(100);
            dataCallback(entity, new Identity("publisher"), new MessageType("DATA"),
                new NetMQMessage(new[]
                {
                    new NetMQFrame("publisher"), new MqFrame((long) 5), new MqFrame((long) 20),
                    new NetMQFrame("myData"), new NetMQFrame("3"), new NetMQFrame("hello")
                }));

            if (receiveEvent.WaitOne(2000))
                throw new Exception("data received after shutdown");
            
            if (storeEvent.WaitOne(2000))
                throw new Exception("store received after shutdown");
        }
    }
}