﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class EagerWorkerTests : TestBase
    {
        [Fact]
        private void eager_worker_on_setting_middlewares()
        {
            var eagerWorker = new EagerWorker(new MessageType("result1"), new MessageType("result2"), new MessageType("result3"));

            var receiveMiddleware = (IMqReceiveMiddleware) eagerWorker;
            var receiveMiddlewares = receiveMiddleware.GetReceiveMiddlewares();
            var receiveMessageTypes = receiveMiddlewares.Select(x => x.MessageType).ToArray();
            receiveMessageTypes.Should().Contain(new MessageType("WORK"));
            receiveMessageTypes.Should().Contain(new MessageType("NOWORK"));

            var sendMiddleware = (IMqSendMiddleware) eagerWorker;
            var sendMiddlewares = sendMiddleware.GetSendMiddlewares();
            var sendMessageTypes = sendMiddlewares.Select(x => x.MessageType).ToArray();
            sendMessageTypes.Should().Contain(new NetMQFrame("result1"));
            sendMessageTypes.Should().Contain(new NetMQFrame("result2"));
            sendMessageTypes.Should().Contain(new NetMQFrame("result3"));
        }

        [Fact]
        private void eager_worker_on_sending_ready_timeout()
        {
            var eagerWorker = new EagerWorker(TimeSpan.FromMilliseconds(500));
            var sendEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.ConvertToString().Should().Be("READY");
                sendEvent.Set();
            };
            eagerWorker.SetEntity(mqEntity);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent");
            sendEvent.Reset();
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent again");
            sendEvent.Reset();
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent again");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void eager_worker_on_receiving_nowork()
        {
            var eagerWorker = new EagerWorker(TimeSpan.FromMilliseconds(500));
            var receivers = eagerWorker.GetReceiveMiddlewares();
            var sendEvent = new ManualResetEvent(false);
            var receiveEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnSendMessage += (type, message) =>
            {
                sendEvent.Set();
                type.ConvertToString().Should().Be("READY");
                receivers.FirstOrDefault(x => x.MessageType == new NetMQFrame("NOWORK")).Callback(mqEntity,
                    new Identity("sender"), new MessageType("NOWORK"), null);
            };
            mqEntity.OnMessageReceive += (identity, message) => { receiveEvent.Set(); };
            eagerWorker.SetEntity(mqEntity);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent");
            sendEvent.Reset();
            Helper.ShouldNotReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(600), pollerTask, "Work received");
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent again");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void eager_worker_on_receiving_works()
        {
            var eagerWorker = new EagerWorker(TimeSpan.FromMilliseconds(500), 1);
            var receivers = eagerWorker.GetReceiveMiddlewares();
            var sendEvent = new ManualResetEvent(false);
            var receiveEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.ConvertToString().Should().Be("READY");
                sendEvent.Set();
            };
            mqEntity.OnMessageReceiveWithMessageType += (identity, messageType, message) =>
            {
                receiveEvent.Set();
                identity.ConvertToString().Should().Be("sender");
                messageType.ConvertToString().Should().Be("some_work");
                message.Should().ContainInOrder(new NetMQFrame("some_data"));
            };
            eagerWorker.SetEntity(mqEntity);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent");
            sendEvent.Reset();
            receivers.FirstOrDefault(x => x.MessageType == new NetMQFrame("WORK")).Callback(mqEntity,
                new Identity("sender"), new MessageType("WORK"), 
                new NetMQMessage(new[]
                {
                    new NetMQFrame(BitConverter.GetBytes((long) 1)), new NetMQFrame("some_work"),
                    new NetMQFrame("some_data")
                }));

            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Work was not received");
            Helper.ShouldNotReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was sent again");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void eager_worker_on_wrapping_work_result()
        {
            var eagerWorker = new EagerWorker(TimeSpan.FromMilliseconds(500), 1, new MessageType("some_work"));
            var receivers = eagerWorker.GetReceiveMiddlewares();
            var senders = eagerWorker.GetSendMiddlewares();
            var sendEvent = new ManualResetEvent(false);
            var receiveEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnSendMessage += (type, message) =>
            {
                sendEvent.Set();
                type.ConvertToString().Should().Be("READY");
            };
            mqEntity.OnMessageReceiveWithMessageType += (identity, messageType, message) =>
            {
                receiveEvent.Set();
                identity.ConvertToString().Should().Be("sender");
                messageType.ConvertToString().Should().Be("some_work");
                message.Should().ContainInOrder(new NetMQFrame("some_data"));
            };

            eagerWorker.SetEntity(mqEntity);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent");

            receivers.FirstOrDefault(x => x.MessageType == new NetMQFrame("WORK")).Callback(mqEntity,
                new Identity("sender"), new MessageType("WORK"), 
                new NetMQMessage(new[]
                {
                    new NetMQFrame(BitConverter.GetBytes((long) 1)), new NetMQFrame("some_work"),
                    new NetMQFrame("some_data")
                }));
            sendEvent.Reset();

            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Work was not received");

            var msg = new NetMQMessage(new[]
            {
                new NetMQFrame(BitConverter.GetBytes((long) 1)), new NetMQFrame("some_work"),
                new NetMQFrame("some_data")
            });
            var id = new Identity("sender");
            var msgType = new MessageType("some_work");
            senders.FirstOrDefault(x => x.MessageType == new NetMQFrame("some_work")).Callback(mqEntity,
                ref id, ref msgType, ref msg);
            msgType.Should().Be(new NetMQFrame("WORKRESULT"));
            msg.Should().ContainInOrder(new NetMQFrame(BitConverter.GetBytes((long) 1)), new NetMQFrame("some_work"),
                new NetMQFrame("some_data"));

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent again");
            Helper.CheckFault(pollerTask);
        }
        
        [Fact]
        private void eager_worker_on_concurrent_works()
        {
            var eagerWorker = new EagerWorker(TimeSpan.FromMilliseconds(500), 2);
            var receivers = eagerWorker.GetReceiveMiddlewares();
            var sendEvent = new ManualResetEvent(false);
            var receiveEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnSendMessage += (type, message) =>
            {
                type.ConvertToString().Should().Be("READY");
                sendEvent.Set();
            };
            mqEntity.OnMessageReceiveWithMessageType += (identity, messageType, message) =>
            {
                receiveEvent.Set();
                identity.ConvertToString().Should().Be("sender");
                messageType.ConvertToString().Should().Be("some_work");
                message.Should().ContainInOrder(new NetMQFrame("some_data"));
            };
            eagerWorker.SetEntity(mqEntity);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent");
            sendEvent.Reset();
            receivers.FirstOrDefault(x => x.MessageType == new NetMQFrame("WORK")).Callback(mqEntity,
                new Identity("sender"), new MessageType("WORK"), 
                new NetMQMessage(new[]
                {
                    new NetMQFrame(BitConverter.GetBytes((long) 1)), new NetMQFrame("some_work"),
                    new NetMQFrame("some_data")
                }));

            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Work was not received");
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(600), pollerTask,
                "Ready signal was not sent again");
            Helper.CheckFault(pollerTask);
        }
    }
}