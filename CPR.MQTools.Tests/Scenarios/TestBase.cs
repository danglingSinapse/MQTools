﻿using System.Threading;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace CPR.MQTools.Tests.Scenarios
{
    public class TestBase
    {
        protected TestBase()
        {
            ThreadPool.SetMinThreads(400, 200);
            ThreadPool.SetMaxThreads(400, 400);
            
            var config = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget("logconsole")
            {
                UseDefaultRowHighlightingRules = true,
                Layout =
                    @"${date:format=yyyy-MM-dd HH\:mm\:ss} | ${logger} | ${level} | ${message} ${exception:format=toString,data:separator=\r\n}"
            };
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Trace",
                ConsoleOutputColor.DarkGray, ConsoleOutputColor.NoChange));
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Debug",
                ConsoleOutputColor.Gray, ConsoleOutputColor.NoChange));
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Info",
                ConsoleOutputColor.White, ConsoleOutputColor.NoChange));
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Warn",
                ConsoleOutputColor.Yellow, ConsoleOutputColor.NoChange));
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Error",
                ConsoleOutputColor.Red, ConsoleOutputColor.NoChange));
            consoleTarget.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Fatal",
                ConsoleOutputColor.White, ConsoleOutputColor.Red));

            config.AddTarget(consoleTarget);
            config.AddRuleForAllLevels(consoleTarget);
            LogManager.Configuration = config;
        }
    }
}