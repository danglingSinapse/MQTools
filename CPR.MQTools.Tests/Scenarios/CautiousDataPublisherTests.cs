﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class CautiousDataPublisherTests : TestBase
    {
        [Fact]
        void cautious_publisher_on_publishing_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 3, LastCreationTimestamp = 0};
            };

            var firstTime = true;
            var storeInfoEvent = new ManualResetEvent(false);
            versionController.OnStoreDataInfo += (id, info) =>
            {
                storeInfoEvent.Set();
                id.Should().Be("publisher");
                info.DataIndex.Should().Be(firstTime ? 4 : 5);
            };

            var dataSource = new DataSourceStub();
            var entity = new MqEntityStub();
            var publisher = new CautiousDataPublisher(dataSource, versionController, "publisher");
            var pollerTask = Task.Run(() => entity.Poller.Run());
            while (!entity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var storeCallEvent = new ManualResetEvent(false);
            var sendEvent = new ManualResetEvent(false);
            publisher.SetEntity(entity);
            publisher.EntityOnline(new Identity("consumer"));

            dataSource.OnStore += (publisherId, index, data) =>
            {
                publisherId.Should().Be("publisher");
                if (firstTime)
                {
                    data.DataIndex.Should().Be(4);
                    data.MessageType.ConvertToString().Should().Be("myData");
                    data.Message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                }
                else
                {
                    data.DataIndex.Should().Be(5);
                    data.MessageType.ConvertToString().Should().Be("myData");
                    data.Message.Select(x => x.ConvertToString()).Should().ContainInOrder("6", "world");
                }

                storeCallEvent.Set();
            };

            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("consumer");
                type.ConvertToString().Should().Be("DATA");
                message.Pop().ConvertToString().Should().Be("publisher");

                if (firstTime)
                {
                    message.Pop().ConvertToInt64().Should().Be(4);
                    message.Pop().ConvertToInt64();
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "hello");
                }
                else
                {
                    message.Pop().ConvertToInt64().Should().Be(5);
                    message.Pop().ConvertToInt64();
                    message.Select(x => x.ConvertToString()).Should().ContainInOrder("6", "world");
                }
                
                message.Pop().ConvertToString().Should().Be("myData");

                sendEvent.Set();
            };

            publisher.Publish(new MessageType("myData"),
                new NetMQMessage(new[] {new NetMQFrame("3"), new NetMQFrame("hello")}));

            Helper.ShouldReceiveSignal(storeCallEvent, TimeSpan.FromSeconds(2), pollerTask,
                "store method was not called");
            Helper.ShouldReceiveSignal(storeInfoEvent, TimeSpan.FromSeconds(2), pollerTask,
                "store info was not called");
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send method was not called");

            storeCallEvent.Reset();
            sendEvent.Reset();
            firstTime = false;

            publisher.Publish(new MessageType("myData"),
                new NetMQMessage(new[] {new NetMQFrame("6"), new NetMQFrame("world")}));

            Helper.ShouldReceiveSignal(storeCallEvent, TimeSpan.FromSeconds(2), pollerTask,
                "store method was not called again");
            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromSeconds(2), pollerTask,
                "send method was not called again");
        }

        [Fact]
        void cautious_publisher_on_providing_missing_data()
        {
            var versionController = new DataVersionControllerStub();
            versionController.OnGetLastDataInfo += id =>
            {
                id.Should().Be("publisher");
                return new DataInfo {DataIndex = 5, LastCreationTimestamp = 0};
            };

            var restoreCallEvent = new ManualResetEvent(false);
            var restoreRangeCallEvent = new ManualResetEvent(false);
            var data1Event = new ManualResetEvent(false);
            var data2Event = new ManualResetEvent(false);
            var data3Event = new ManualResetEvent(false);
            var data4Event = new ManualResetEvent(false);
            var data5Event = new ManualResetEvent(false);

            var dataSource = new DataSourceStub();
            dataSource.OnRestore += (id, index) =>
            {
                id.Should().Be("publisher");
                restoreCallEvent.Set();
                if (index == 1)
                {
                    return new MissingData
                    {
                        DataIndex = index,
                        Message = new NetMQMessage(new[] {new NetMQFrame("3"), new NetMQFrame("world")}),
                        MessageType = new MessageType("myData"),
                        CreationTimestamp = 0
                    };
                }
                
                if (index == 2)
                {
                    return null;
                }
                
                throw new Exception("unexpected data index passed to restore");
            };

            dataSource.OnRestoreRangeRange += (id, fromIndex, toIndex) =>
            {
                fromIndex.Should().Be(3);
                toIndex.Should().Be(5);
                restoreRangeCallEvent.Set();
                return new[]
                {
                    new MissingData
                    {
                        DataIndex = 3,
                        Message = new NetMQMessage(new[] {new NetMQFrame("5"), new NetMQFrame("world")}),
                        MessageType = new MessageType("myData"),
                        CreationTimestamp = 10
                    },
                    new MissingData
                    {
                        DataIndex = 5,
                        Message = new NetMQMessage(new[] {new NetMQFrame("7"), new NetMQFrame("world")}),
                        MessageType = new MessageType("myData"),
                        CreationTimestamp = 20
                    }
                };
            };

            var entity = new MqEntityStub();
            entity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("publisher");
                type.ConvertToString().Should().Be("DATA");
                message.Pop().ConvertToString().Should().Be("publisher");
                var index = message.Pop().ConvertToInt64();
                var creation = message.Pop().ConvertToInt64();
                var messageType = message.Pop();
                switch (index)
                {
                    case 1:
                        creation.Should().Be(0);
                        messageType.ConvertToString().Should().Be("myData");
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("3", "world");
                        data1Event.Set();
                        break;
                    
                    case 2:
                        messageType.ConvertToString().Should().Be("MISS");
                        message.Should().BeEmpty();
                        data2Event.Set();
                        break;
    
                    case 3:
                        creation.Should().Be(10);
                        messageType.ConvertToString().Should().Be("myData");
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("5", "world");
                        data3Event.Set();
                        break;

                    case 4:
                        messageType.ConvertToString().Should().Be("MISS");
                        message.Should().BeEmpty();
                        data4Event.Set();
                        break;

                    case 5:
                        creation.Should().Be(20);
                        messageType.ConvertToString().Should().Be("myData");
                        message.Select(x => x.ConvertToString()).Should().ContainInOrder("7", "world");
                        data5Event.Set();
                        break;
                    default:
                        throw new Exception($"unexpected data index {index} was sent");
                }
            };

            var publisher = new CautiousDataPublisher(dataSource, versionController, "publisher");
            publisher.SetEntity(entity);
            publisher.EntityOnline(new Identity("consumer"));

            var fetchCallback = publisher.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new MessageType("FETCH"));

            fetchCallback.Callback(entity, new Identity("publisher"), new MessageType("FETCH"),
                new NetMQMessage(new[] {new MqFrame((long)1)}));
            
            if (!restoreCallEvent.WaitOne(0))
                throw new Exception("restore method was not called");
            restoreCallEvent.Reset();
            
            fetchCallback.Callback(entity, new Identity("publisher"), new MessageType("FETCH"),
                new NetMQMessage(new[] {new MqFrame((long)2)}));
            
            if (!restoreCallEvent.WaitOne(0))
                throw new Exception("restore method was not called");

            fetchCallback.Callback(entity, new Identity("publisher"), new MessageType("FETCH"),
                new NetMQMessage(new[] {new MqFrame((long)3), new MqFrame((long)5)}));


            if (!restoreRangeCallEvent.WaitOne(1000))
                throw new Exception("restoreRange method was not called");

            if (!data1Event.WaitOne(1000))
                throw new Exception("data1 was not called");

            if (!data3Event.WaitOne(1000))
                throw new Exception("data3 was not called");

            if (!data4Event.WaitOne(1000))
                throw new Exception("data4 was not called");

            if (!data5Event.WaitOne(1000))
                throw new Exception("data5 was not called");
        }
    }
}