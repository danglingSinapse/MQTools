﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using NetMQ.Sockets;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class MqEntityTests : TestBase
    {
        [Fact]
        private void mqEntity_on_registering_connection_callbacks()
        {
            var mqEntity = new MqEntity(new RouterSocket());
            var connectionInformer = new MqConnectionInformerStub();
            var connectionInformee = new MqConnectionInformeeStub();
            var onlineEvent = new ManualResetEvent(false);
            var offlineEvent = new ManualResetEvent(false);
            var boundEvent = new ManualResetEvent(false);

            connectionInformee.EntityOnline += identity =>
            {
                identity.Should().Be(new NetMQFrame("service"));
                onlineEvent.Set();
            };
            connectionInformee.EntityOffline += identity =>
            {
                identity.Should().Be(new NetMQFrame("service"));
                offlineEvent.Set();
            };
            connectionInformee.EntityBound += port =>
            {
                port.Should().Be(9999);
                boundEvent.Set();
            };

            mqEntity.AddFunctionality(connectionInformer);
            mqEntity.AddFunctionality(connectionInformee);

            connectionInformer.InvokeEntityOnline(new Identity("service"));
            if (!onlineEvent.WaitOne(0))
                throw new Exception("online event was not called");
            connectionInformer.InvokeEntityOffline(new Identity("service"));
            if (!offlineEvent.WaitOne(0))
                throw new Exception("offline event was not called");
            connectionInformer.InvokeEntityBound(9999);
            if (!boundEvent.WaitOne(0))
                throw new Exception("bound event was not called");
        }

        [Fact]
        private void mqEntity_on_registering_middlewares()
        {
            var mqEntity = new MqEntity(new RouterSocket());
            var sendEvent = new ManualResetEvent(false);
            var receiveEvent = new ManualResetEvent(false);
            var socketReceiveEvent = new ManualResetEvent(false);
            var port = PortGenerator.GetNext();

            var sendMiddleWare = new SendMiddlewareStub();
            sendMiddleWare.SendHandlers.Add(new SendHandleInfo
            {
                MessageType = new NetMQFrame("test"),
                Callback = (IMqEntity sender, ref Identity identity, ref MessageType type, ref NetMQMessage message) =>
                {
                    identity.Should().Be(new NetMQFrame("sender"));
                    type.Should().Be(new NetMQFrame("test"));
                    message.Should().ContainInOrder(new NetMQFrame("data"));
                    message.Push("metadata");
                    sendEvent.Set();
                }
            });

            var receiveMiddleWare = new ReceiveMiddlewareStub();
            receiveMiddleWare.ReceiveHandlers.Add(new ReceiveHandleInfo
            {
                MessageType = new MessageType("response"),
                Callback = (sender, identity, type, message) =>
                {
                    type.Should().Be(new NetMQFrame("response"));
                    message.Should().ContainInOrder(new NetMQFrame("rdata"));
                    receiveEvent.Set();
                    return Task.CompletedTask;
                }
            });

            mqEntity.AddFunctionality(sendMiddleWare);
            mqEntity.AddFunctionality(receiveMiddleWare);
            mqEntity.Bind(port);

            var socket = new RouterSocket();
            socket.Options.Identity = new NetMQFrame("sender").ToByteArray();
            socket.ReceiveReady += (sender, args) =>
            {
                var response = new NetMQMessage();
                var msg = socket.ReceiveMultipartMessage();
                response.Append(msg.Pop());
                response.Append(msg.Pop());
                msg.Should().ContainInOrder(new NetMQFrame("test"), new NetMQFrame("metadata"),
                    new NetMQFrame("data"));
                socketReceiveEvent.Set();

                response.Append("response");
                response.Append("rdata");
                socket.SendMultipartMessage(response);
            };
            mqEntity.Poller.Add(socket);
            socket.Connect($"tcp://localhost:{port}");

            var pollerTask = Task.Run(() => mqEntity.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            mqEntity.SendMessage(new Identity("sender"), new MessageType("test"),
                new NetMQMessage(new[] {new NetMQFrame("data")}));

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(5000), pollerTask,
                "send handler was not called");
            Helper.ShouldReceiveSignal(socketReceiveEvent, TimeSpan.FromMilliseconds(5000), pollerTask,
                "socket did not receive any message");
            Helper.ShouldReceiveSignal(receiveEvent, TimeSpan.FromMilliseconds(5000), pollerTask,
                "receive handler was not called");
        }

        [Fact]
        private void mqEntity_on_connect_and_disconnect_orders()
        {
            var mqEntity = new MqEntity(new RouterSocket());
            var port = PortGenerator.GetNext();
            var socketReceiveEvent = new ManualResetEvent(false);

            var socket = new RouterSocket();
            socket.Options.Identity = new NetMQFrame("receiver").ToByteArray();
            socket.ReceiveReady += (sender, args) =>
            {
                var msg = socket.ReceiveMultipartMessage();
                msg.Pop();
                msg.Pop();
                msg.Should().ContainInOrder(new NetMQFrame("hello"), new NetMQFrame("data"));
                socketReceiveEvent.Set();
            };
            mqEntity.Poller.Add(socket);
            socket.Bind($"tcp://localhost:{port}");
            mqEntity.Connect($"tcp://localhost:{port}");

            var pollerTask = Task.Run(() => mqEntity.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            mqEntity.SendMessage(new Identity("receiver"), new MessageType("hello"),
                new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldReceiveSignal(socketReceiveEvent, TimeSpan.FromMilliseconds(2000), pollerTask,
                "socket did not receive any message");
            socketReceiveEvent.Reset();
            mqEntity.Disconnect($"tcp://localhost:{port}");
            mqEntity.SendMessage(new Identity("receiver"), new MessageType("hello"),
                new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldNotReceiveSignal(socketReceiveEvent, TimeSpan.FromMilliseconds(2000), pollerTask,
                "socket did receive a message");
        }

        [Fact]
        private void mqEntity_on_load_balancer()
        {
            var mqEntity = new MqEntity(new RouterSocket());
            var loadBalancer = new LoadBalancerStub(new[] {new Identity("socket1"), new Identity("socket2"), });
            var port = PortGenerator.GetNext();
            var socket1ReceiveEvent = new ManualResetEvent(false);
            var socket2ReceiveEvent = new ManualResetEvent(false);

            mqEntity.AddFunctionality(loadBalancer);
            mqEntity.Bind(port);
            var socket1 = new RouterSocket();
            socket1.Options.Identity = new NetMQFrame("socket1").ToByteArray();
            socket1.ReceiveReady += (sender, args) =>
            {
                var msg = socket1.ReceiveMultipartMessage();
                msg.Pop();
                msg.Pop();
                msg.Should().ContainInOrder(new NetMQFrame("hello"), new NetMQFrame("data"));
                socket1ReceiveEvent.Set();
            };
            mqEntity.Poller.Add(socket1);
            socket1.Connect($"tcp://localhost:{port}");

            var socket2 = new RouterSocket();
            socket2.Options.Identity = new NetMQFrame("socket2").ToByteArray();
            socket2.ReceiveReady += (sender, args) =>
            {
                var msg = socket2.ReceiveMultipartMessage();
                msg.Pop();
                msg.Pop();
                msg.Should().ContainInOrder(new NetMQFrame("hello"), new NetMQFrame("data"));
                socket2ReceiveEvent.Set();
            };
            mqEntity.Poller.Add(socket2);
            socket2.Connect($"tcp://localhost:{port}");

            var pollerTask = Task.Run(() => mqEntity.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            mqEntity.SendMessage(new MessageType("hello"), new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldReceiveSignal(socket1ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 1 did not receive any message");
            Helper.ShouldNotReceiveSignal(socket2ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 2 did receive a message");
            socket1ReceiveEvent.Reset();

            mqEntity.SendMessage(new MessageType("hello"), new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldReceiveSignal(socket2ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 2 did not receive any message");
            Helper.ShouldNotReceiveSignal(socket1ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 1 did receive a message");
            socket2ReceiveEvent.Reset();

            mqEntity.SendMessage(new MessageType("hello"), new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldReceiveSignal(socket1ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 1 did not receive any message");
            Helper.ShouldNotReceiveSignal(socket2ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 2 did receive a message");
            socket1ReceiveEvent.Reset();

            mqEntity.SendMessage(new MessageType("hello"), new NetMQMessage(new[] {new NetMQFrame("data")}));
            Helper.ShouldReceiveSignal(socket2ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 2 did not receive any message");
            Helper.ShouldNotReceiveSignal(socket1ReceiveEvent, TimeSpan.FromMilliseconds(500), pollerTask,
                "socket 1 did receive a message");
            socket2ReceiveEvent.Reset();
        }

        [Fact]
        private void mqEntity_on_async_handlers_exception()
        {
            var mqEntity = new MqEntity(new RouterSocket());

            async Task InnerHandler()
            {
                await Task.Delay(TimeSpan.FromMilliseconds(200));
            }
            
            async Task Handler(IMqEntity sender, Identity identity, MessageType messagetype, NetMQMessage message)
            {
                await InnerHandler();
                throw new Exception("test exception");
            }
            
            var exceptionReceived = new ManualResetEvent(false);
            mqEntity.RegisterReceiveHandler(new MessageType("Test"), Handler);
            mqEntity.OnReceiveException += (exception, identity, type, message) =>
            {
                exception.ToString().Should().ContainAll("test exception");
                exceptionReceived.Set();
                return Task.CompletedTask;
            };

            var bindPort = 0;
            mqEntity.Socket.Options.Identity = Encoding.ASCII.GetBytes("entity");
            mqEntity.OnBind += port => { bindPort = port; };
            mqEntity.Bind();
            var pollerTask = Task.Run(() => mqEntity.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            using (var client = new RouterSocket($">tcp://*:{bindPort}"))
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));

                var requestMessage = new NetMQMessage();
                requestMessage.Push(new MqFrame((long) 123));
                requestMessage.Push(new MqFrame("Test"));
                requestMessage.PushEmptyFrame();
                requestMessage.Push(mqEntity.Socket.Options.Identity);

                client.SendMultipartMessage(requestMessage);
                Helper.ShouldReceiveSignal(exceptionReceived, TimeSpan.FromSeconds(1), pollerTask,
                    "did not receive any exception");
            }

            exceptionReceived.Reset();
            
            using (var client = new RouterSocket($">tcp://*:{bindPort}"))
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));

                var requestMessage = new NetMQMessage();
                requestMessage.Push(new MqFrame((long) 123));
                requestMessage.Push(new MqFrame("Test"));
                requestMessage.PushEmptyFrame();
                requestMessage.Push(mqEntity.Socket.Options.Identity);

                client.SendMultipartMessage(requestMessage);
                Helper.ShouldReceiveSignal(exceptionReceived, TimeSpan.FromSeconds(1), pollerTask,
                    "did not receive any exception");            
            }
        }

        [Fact]
        private void mqEntity_on_sync_handlers_exception()
        {
            var mqEntity = new MqEntity(new RouterSocket());
            
            Task Handler(IMqEntity sender, Identity identity, MessageType messagetype, NetMQMessage message)
            {
                Thread.Sleep(200);
                throw new Exception("test exception");
            }
            
            var exceptionReceived = new ManualResetEvent(false);
            mqEntity.RegisterReceiveHandler(new MessageType("Test"), Handler);
            mqEntity.OnReceiveException += (exception, identity, type, message) =>
            {
                exception.ToString().Should().ContainAll("test exception");
                exceptionReceived.Set();
                return Task.CompletedTask;
            };
            
            var bindPort = 0;
            mqEntity.Socket.Options.Identity = Encoding.ASCII.GetBytes("entity");
            mqEntity.OnBind += port => { bindPort = port; };
            mqEntity.Bind();
            var pollerTask = Task.Run(() => mqEntity.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            using (var client = new RouterSocket($">tcp://*:{bindPort}"))
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));

                var requestMessage = new NetMQMessage();
                requestMessage.Push(new MqFrame((long) 123));
                requestMessage.Push(new MqFrame("Test"));
                requestMessage.PushEmptyFrame();
                requestMessage.Push(mqEntity.Socket.Options.Identity);

                client.SendMultipartMessage(requestMessage);
                Helper.ShouldReceiveSignal(exceptionReceived, TimeSpan.FromSeconds(1), pollerTask,
                    "did not receive any exception");
            }

            exceptionReceived.Reset();
            
            using (var client = new RouterSocket($">tcp://*:{bindPort}"))
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));

                var requestMessage = new NetMQMessage();
                requestMessage.Push(new MqFrame((long) 123));
                requestMessage.Push(new MqFrame("Test"));
                requestMessage.PushEmptyFrame();
                requestMessage.Push(mqEntity.Socket.Options.Identity);

                client.SendMultipartMessage(requestMessage);
                Helper.ShouldReceiveSignal(exceptionReceived, TimeSpan.FromSeconds(1), pollerTask,
                    "did not receive any exception");            
            }
        }
    }
}