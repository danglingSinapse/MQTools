using System;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using StackExchange.Redis;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class RedisDiscoveryTests
    {
        [Fact]
        private void redis_discovery_device_on_new_discoveries()
        {
            var connectEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnConnect += address =>
            {
                address.Should().Be("tcp://localhost:555");
                connectEvent.Set();
            };
            var discoveryDevice = new RedisDiscoveryDevice("localhost", "server");
            discoveryDevice.SetEntity(mqEntity);

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            
            var redis = ConnectionMultiplexer.Connect("localhost");
            var subscriber = redis.GetSubscriber();
            subscriber.Publish("server", "tcp://localhost:555");

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(connectEvent, TimeSpan.FromSeconds(5), pollerTask,
                "Connect method was not called.");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void redis_discovery_device_on_undesired_discoveries()
        {
            var mqEntity = new MqEntityStub();
            mqEntity.OnConnect += address => throw new Exception("Connect method was called.");
            var discoveryDevice = new RedisDiscoveryDevice("localhost", "server");
            discoveryDevice.SetEntity(mqEntity);

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            
            var redis = ConnectionMultiplexer.Connect("localhost");
            var subscriber = redis.GetSubscriber();
            subscriber.Publish("noserver", "tcp://localhost:555");

            Thread.Sleep(TimeSpan.FromMilliseconds(100));

            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void redis_discovery_device_on_multiple_discoveries()
        {
            var port1 = PortGenerator.GetNext();
            var port2 = PortGenerator.GetNext();
            var firstconnectEvent = new ManualResetEvent(false);
            var secondconnectEvent = new ManualResetEvent(false);
            var mqEntity = new MqEntityStub();
            mqEntity.OnConnect += address =>
            {
                if (address == $"tcp://localhost:{port1}")
                    firstconnectEvent.Set();
                else if (address == $"tcp://localhost:{port2}")
                    secondconnectEvent.Set();
                else
                    throw new Exception($"Got unexpected address {address} in connect method.");
            };
            var discoveryDevice = new RedisDiscoveryDevice("localhost", "server");
            discoveryDevice.SetEntity(mqEntity);

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            var redis = ConnectionMultiplexer.Connect("localhost");
            var subscriber = redis.GetSubscriber();
            subscriber.Publish("server", $"tcp://localhost:{port1}");
            subscriber.Publish("server", $"tcp://localhost:{port2}");
            
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(firstconnectEvent, TimeSpan.FromSeconds(5), pollerTask,
                $"Connect method was not called for localhost:{port1}");
            Helper.ShouldReceiveSignal(secondconnectEvent, TimeSpan.FromSeconds(5), pollerTask,
                $"Connect method was not called for localhost:{port2}");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void discovery_device_on_service_offline()
        {
            var port = PortGenerator.GetNext();
            var connectEvent = new ManualResetEvent(false);
            var disconnectEvent = new ManualResetEvent(false);

            var mqEntity = new MqEntityStub();
            mqEntity.OnConnect += address =>
            {
                address.Should().Be($"tcp://localhost:{port}");
                connectEvent.Set();
            };

            mqEntity.OnDisconnect += address =>
            {
                address.Should().Be($"tcp://localhost:{port}");
                disconnectEvent.Set();
            };
            
            var discoveryDevice = new RedisDiscoveryDevice("localhost", "server", TimeSpan.FromSeconds(1));
            discoveryDevice.SetEntity(mqEntity);

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var redis = ConnectionMultiplexer.Connect("localhost");
            var subscriber = redis.GetSubscriber();
            subscriber.Publish("server", $"tcp://localhost:{port}");

            Helper.ShouldReceiveSignal(connectEvent, TimeSpan.FromSeconds(2), pollerTask,
                "Connect method was not called");
            Helper.ShouldReceiveSignal(disconnectEvent, TimeSpan.FromSeconds(2), pollerTask,
                "Disconnect method was not called");
            Helper.CheckFault(pollerTask);
        }

        [Fact]
        private void discovery_device_on_load_balancing()
        {
            var port1 = PortGenerator.GetNext();
            var port2 = PortGenerator.GetNext();
            var port3 = PortGenerator.GetNext();
            var mqEntity = new MqEntityStub();
            var connected = 0;
            var connectEvent = new ManualResetEvent(false);
            mqEntity.OnConnect += address =>
            {
                ++connected;
                if (connected == 3)
                    connectEvent.Set();
            };
            var discoveryDevice = new RedisDiscoveryDevice("localhost", "server");
            discoveryDevice.SetEntity(mqEntity);

            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);
            
            var redis = ConnectionMultiplexer.Connect("localhost");
            var subscriber = redis.GetSubscriber();
            subscriber.Publish("server", $"tcp://localhost:{port1}");
            Thread.Sleep(100);
            subscriber.Publish("server", $"tcp://localhost:{port2}");
            Thread.Sleep(100);
            subscriber.Publish("server", $"tcp://localhost:{port3}");

            Helper.ShouldReceiveSignal(connectEvent, TimeSpan.FromSeconds(10), pollerTask,
                "Connect method was not called three times");
            Helper.CheckFault(pollerTask);

            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port1}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port2}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port3}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port1}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port2}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port3}");
            discoveryDevice.GetNextNode().ConvertToString().Should().Be($"tcp://localhost:{port1}");
        }
    }
}