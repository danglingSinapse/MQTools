﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using CPR.MQTools.Interfaces;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class BinarySerializationTests : TestBase
    {
        [Fact]
        void missing_data_on_serialization()
        {
            var data = new MissingData
            {
                CreationTimestamp = 100,
                DataIndex = 10,
                Message = new NetMQMessage(new[] {new MqFrame("123"), new MqFrame("321")}),
                MessageType = new MessageType("Test")
            };

            var bytes = data.ToByteArray();
            var deserialized = MissingData.FromByteArray(bytes);

            deserialized.CreationTimestamp.Should().Be(100);
            deserialized.DataIndex.Should().Be(10);
            deserialized.Message.Select(x => x.ConvertToString()).Should().ContainInOrder("123", "321");
            deserialized.MessageType.ConvertToString().Should().Be("Test");
        }

        [Fact]
        void data_info_on_serialization()
        {
            var data = new DataInfo
            {
                DataIndex = 120,
                LastCreationTimestamp = 1050
            };

            var bytes = data.ToByteArray();
            var deserialized = DataInfo.FromByteArray(bytes);

            deserialized.DataIndex.Should().Be(120);
            deserialized.LastCreationTimestamp.Should().Be(1050);
        }

        [Serializable]
        public class TestDataInfo : SerializableObject<TestDataInfo>, ISerializable
        {
            [DataMember(Name = "i")]
            public long DataIndex { get; set; }
            [DataMember(Name = "t")]
            public long LastCreationTimestamp { get; set; }

            public TestDataInfo()
            {
            }

            private TestDataInfo(SerializationInfo info, StreamingContext context)
            {
                DataIndex = (long)info.GetValue("i", typeof(long));
                LastCreationTimestamp = (long)info.GetValue("c", typeof(long));
            }

            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("i", DataIndex);
                info.AddValue("c", LastCreationTimestamp);
            }
        }

        [Fact]
        void data_info_on_namespace_change_serialization()
        {
            var data = new TestDataInfo
            {
                DataIndex = 120,
                LastCreationTimestamp = 1050
            };

            var bytes = data.ToByteArray();
            var deserialized = DataInfo.FromByteArray(bytes);

            deserialized.DataIndex.Should().Be(120);
            deserialized.LastCreationTimestamp.Should().Be(1050);
        }
    }
}