﻿using System;
using System.Linq;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class HeartbeatAnswererTests : TestBase
    {
        [Fact]
        private void heartbeatAnswerer_on_setting_middlewares()
        {
            var heartbeatAnswerer = new HeartbeatAnswerer();
            var receiveMiddleware = (IMqReceiveMiddleware) heartbeatAnswerer;
            var receiveMiddlewares = receiveMiddleware.GetReceiveMiddlewares();
            var receiveMessageTypes = receiveMiddlewares.Select(x => x.MessageType).ToArray();
            receiveMessageTypes.Should().Contain(new MessageType("PING"));
        }

        [Fact]
        private void heartbeatAnswerer_on_answering_heartbeat()
        {
            var mqEntity = new MqEntityStub();
            var sendEvent = false;
            var heartbeatAnswerer = new HeartbeatAnswerer();
            heartbeatAnswerer.SetEntity(mqEntity);
            var receiveMiddleware = heartbeatAnswerer.GetReceiveMiddlewares()
                .FirstOrDefault(x => x.MessageType == new NetMQFrame("PING"));
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("sender");
                type.ConvertToString().Should().Be("PONG");
                sendEvent = true;
            };
            receiveMiddleware.Callback(mqEntity, new Identity("sender"), new MessageType("PING"), null);

            if (!sendEvent)
                throw new Exception("Did not receive pong message");
        }
    }
}