﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Tests.Stubs;
using FluentAssertions;
using NetMQ;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class SensitiveDataTests : TestBase
    {
        [Fact]
        void sensitive_data_on_asking_for_data()
        {
            var mqEntity = new MqEntityStub();
            var seeker = new SensitiveDataSeeker("thatdata");
            seeker.SetEntity(mqEntity);
            var firstSendEvent = new ManualResetEvent(false);
            var secondSendEvent = new ManualResetEvent(false);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());
            
            seeker.EntityOnline(new Identity("first"));
            seeker.EntityOnline(new Identity("second"));
            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                type.ConvertToString().Should().Be("NDATA(thatdata)");
                if (identity.ConvertToString() == "first")
                    firstSendEvent.Set();
                else if (identity.ConvertToString() == "second")
                    secondSendEvent.Set();
                else
                    throw new Exception("sent NDATA to unexpected identity");
            };

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(firstSendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "first provider did not get asked for sensitive data");
            Helper.ShouldReceiveSignal(secondSendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "second provider did not get asked for sensitive data");

            firstSendEvent.Reset();
            secondSendEvent.Reset();

            Helper.ShouldReceiveSignal(firstSendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "first provider did not get asked for sensitive data again");
            Helper.ShouldReceiveSignal(secondSendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "second provider did not get asked for sensitive data again");
        }

        [Fact]
        void sensitive_data_on_receiving_data()
        {
            var mqEntity = new MqEntityStub();
            var seeker = new SensitiveDataSeeker("thatdata");
            seeker.EntityOnline(new Identity("provider"));
            seeker.SetEntity(mqEntity);
            var sendEvent = new ManualResetEvent(false);
            var raiseEvent = new ManualResetEvent(false);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            seeker.OnSensitiveDataReceive += data =>
            {
                data.Should().Be("mydata");
                raiseEvent.Set();
            };

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                type.ConvertToString().Should().Be("NDATA(thatdata)");
                identity.ConvertToString().Should().Be("provider");
                sendEvent.Set();
                seeker.GetReceiveMiddlewares().FirstOrDefault(x => x.MessageType == new MessageType("SDATA(thatdata)"))
                    .Callback(mqEntity, new Identity("provider"), new MessageType("SDATA(thatdata)"),
                        new NetMQMessage(new[] {new NetMQFrame("mydata")}));
            };

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "did not asked for sensitive data");

            sendEvent.Reset();

            Helper.ShouldReceiveSignal(raiseEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "receive data event did not raised");

            Helper.ShouldNotReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "did not asked for sensitive data again");
        }

        [Fact]
        void sensitive_data_on_answering_data()
        {
            var mqEntity = new MqEntityStub();
            var provider = new SensitiveDataProvider("thatdata", "mydata");
            provider.SetEntity(mqEntity);
            var sendEvent = new ManualResetEvent(false);
            var pollerTask = Task.Run(() => mqEntity.Poller.Run());

            while (!mqEntity.Poller.IsRunning)
                Thread.Sleep(100);

            mqEntity.OnSendMessageWithIdentity += (identity, type, message) =>
            {
                identity.ConvertToString().Should().Be("seeker");
                type.ConvertToString().Should().Be("SDATA(thatdata)");
                message.First.ConvertToString().Should().Be("mydata");
                sendEvent.Set();
            };

            provider.GetReceiveMiddlewares().FirstOrDefault(x => x.MessageType == new MessageType("NDATA(thatdata)"))
                .Callback(mqEntity, new Identity("seeker"), new MessageType("NDATA(thatdata)"), null);

            Helper.ShouldReceiveSignal(sendEvent, TimeSpan.FromMilliseconds(1500), pollerTask,
                "did not asked for sensitive data");
        }
    }
}