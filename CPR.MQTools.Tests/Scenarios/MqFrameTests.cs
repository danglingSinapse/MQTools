﻿using FluentAssertions;
using Xunit;

namespace CPR.MQTools.Tests.Scenarios
{
    public class MqFrameTests : TestBase
    {
        private enum MyEnum
        {
            Value1,
            Value2,
            Value3
        }
        
        [Fact]
        private void mqFram_on_conversions()
        {
            new MqFrame(100).ConvertToInt32().Should().Be(100);
            new MqFrame((long)200).ConvertToInt64().Should().Be(200);
            new MqFrame((byte)'c').ToByteArray()[0].Should().Be((byte)'c');
            new MqFrame("hello").ConvertToString().Should().Be("hello");
            new MqFrame(MyEnum.Value1).ConvertToEnum<MyEnum>().Should().Be(MyEnum.Value1);
            new MqFrame(MyEnum.Value2).ConvertToEnum<MyEnum>().Should().Be(MyEnum.Value2);
            new MqFrame(MyEnum.Value3).ConvertToEnum<MyEnum>().Should().Be(MyEnum.Value3);
        }
    }
}