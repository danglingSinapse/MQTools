﻿using System.Collections.Generic;
using System.Linq;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class LoadBalancerStub : MqFunctionBase, IMqLoadBalancer
    {
        private readonly IEnumerable<Identity> _endpoints;
        private int _currentIndex;

        public LoadBalancerStub(IEnumerable<Identity> endpoints)
        {
            _endpoints = endpoints;
            _currentIndex = 0;
        }

        public Identity GetNextNode()
        {
            return _endpoints.ElementAt(_currentIndex++ % _endpoints.Count());
        }
    }
}