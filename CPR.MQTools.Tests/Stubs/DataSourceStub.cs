﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class DataSourceStub : IDataSource
    {
        public delegate void StoreCallBack(string publisherId, long index, MissingData data);

        public event StoreCallBack OnStore;

        public Task Store(string publisherId, long index, MissingData data)
        {
            OnStore?.Invoke(publisherId, index, data);
            return Task.CompletedTask;
        }

        public RestoreCallBack OnRestore { get; set; }
        
        public delegate MissingData RestoreCallBack(string publisherId, long index);

        public Task<MissingData> Restore(string publisherId, long index)
        {
            return Task.FromResult(OnRestore?.Invoke(publisherId, index));
        }

        public delegate IEnumerable<MissingData> RestoreRangeCallBack(string publisherId, long fromIndex, long toIndex);

        public RestoreRangeCallBack OnRestoreRangeRange { get; set; }
        
        public Task<IEnumerable<MissingData>> RestoreRange(string publisherId, long fromIndex, long toIndex)
        {
            return Task.FromResult(OnRestoreRangeRange?.Invoke(publisherId, fromIndex, toIndex));
        }
    }
}