﻿using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using NLog;

namespace CPR.MQTools.Tests.Stubs
{
    public class MqEntityStub : IMqEntity
    {
        public MqEntityStub()
        {
            Poller = new NetMQPoller();
            PollerTaskFactory = new TaskFactory(Poller);
            Socket = new DealerSocket();
            Logger = LogManager.GetLogger("MqTools");
        }

        public NetMQPoller Poller { get; }
        public TaskFactory PollerTaskFactory { get; }
        public NetMQSocket Socket { get; }
        public ILogger Logger { get; }

        public delegate void SendMessageWithIdentityCallback(Identity identity, MessageType messageType,
            NetMQMessage message = null);

        public event SendMessageWithIdentityCallback OnSendMessageWithIdentity;

        public Task<bool> SendMessage(Identity identity, MessageType messageType, NetMQMessage message = null)
        {
            OnSendMessageWithIdentity?.Invoke(identity, messageType, message);
            return Task.FromResult(true);
        }

        public delegate void SendMessageCallback(MessageType messageType, NetMQMessage message = null);

        public event SendMessageCallback OnSendMessage;
        public Identity SendMessageReturnIdentity { get; set; }

        public Task<Identity> SendMessage(MessageType messageType, NetMQMessage message = null)
        {
            OnSendMessage?.Invoke(messageType, message);
            return Task.FromResult(SendMessageReturnIdentity);
        }

        public void RegisterReceiveHandler(MessageType messageType, ReceiveMiddleware callback, bool isPlugin = false)
        {
        }

        public void RegisterSendHandler(MessageType messageType, SendMiddleware callback)
        {
        }

        public delegate void CoonectionCallback(string address);

        public event CoonectionCallback OnConnect;

        public void Connect(string address)
        {
            OnConnect?.Invoke(address);
        }

        public delegate void DiscoonectionCallback(string address);

        public event DiscoonectionCallback OnDisconnect;

        public void Disconnect(string address)
        {
            OnDisconnect?.Invoke(address);
        }

        public delegate void MessageReceiveCallback(Identity identity, NetMQMessage message);

        public event MessageReceiveCallback OnMessageReceive;

        public Task MessageReceived(Identity identity, NetMQMessage message)
        {
            OnMessageReceive?.Invoke(identity, message);
            return Task.CompletedTask;
        }

        public delegate void MessageReceiveWithMessageTypeCallback(Identity identity, MessageType messageType,
            NetMQMessage message);

        public event MessageReceiveWithMessageTypeCallback OnMessageReceiveWithMessageType;

        public Task MessageReceived(Identity identity, MessageType messageType, NetMQMessage message)
        {
            OnMessageReceiveWithMessageType?.Invoke(identity, messageType, message);
            return Task.CompletedTask;
        }

        public event AddEntityMonitoringCallback OnAddEntityMonitoring;

        public void AddEntityMonitoring(Identity identity)
        {
            OnAddEntityMonitoring?.Invoke(identity);
        }

        public event RemoveEntityMonitoringCallback OnRemoveEntityMonitoring;

        public void RemoveEntityMonitoring(Identity identity)
        {
            OnRemoveEntityMonitoring?.Invoke(identity);
        }

        public event IdentitySetCallback OnIdentitySet;

        public void IdentitySet(Identity selfIdentity)
        {
            OnIdentitySet?.Invoke(selfIdentity);
        }

        public void AddFunctionality(MqFunctionBase function, string name = "")
        {
        }

        public void Bind(int bindPort)
        {
        }
        
        public void Run()
        {
        }

        public Task RunAsync()
        {
            return Task.CompletedTask;
        }

        public Task StopGracefully()
        {
            return Task.CompletedTask;
        }

        public void ForceStop()
        {
        }
    }
}