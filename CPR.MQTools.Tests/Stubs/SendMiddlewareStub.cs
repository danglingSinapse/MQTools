﻿using System.Collections.Generic;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class SendMiddlewareStub : MqFunctionBase, IMqSendMiddleware
    {
        public readonly List<SendHandleInfo> SendHandlers = new List<SendHandleInfo>();

        public IEnumerable<SendHandleInfo> GetSendMiddlewares()
        {
            return SendHandlers;
        }
    }
}