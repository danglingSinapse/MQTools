﻿using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class MqConnectionInformerStub : MqFunctionBase, IMqConnectionInformer
    {
        public event ConnectionCallback EntityOnline;
        public event DisconnectionCallback EntityOffline;
        public event BindingCallback EntityBound;
        public event IdentitySetCallback IdentitySet;

        public void InvokeEntityOnline(Identity identity)
        {
            EntityOnline?.Invoke(identity);
        }

        public void InvokeEntityOffline(Identity identity)
        {
            EntityOffline?.Invoke(identity);
        }

        public void InvokeEntityBound(int port)
        {
            EntityBound?.Invoke(port);
        }

        public void InvokeIdentitySet(Identity identity)
        {
            IdentitySet?.Invoke(identity);
        }

        public event AddEntityMonitoringCallback OnAddEntityMonitoring;
        public AddEntityMonitoringCallback AddEntityMonitoring => identity => OnAddEntityMonitoring?.Invoke(identity);

        public event AddEntityMonitoringCallback OnRemoveEntityMonitoring;

        public RemoveEntityMonitoringCallback RemoveEntityMonitoring =>
            identity => OnRemoveEntityMonitoring?.Invoke(identity);
    }
}