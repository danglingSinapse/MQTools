﻿using System.Collections.Generic;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class ReceiveMiddlewareStub : MqFunctionBase, IMqReceiveMiddleware
    {
        public readonly List<ReceiveHandleInfo> ReceiveHandlers = new List<ReceiveHandleInfo>();

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return ReceiveHandlers;
        }
    }
}