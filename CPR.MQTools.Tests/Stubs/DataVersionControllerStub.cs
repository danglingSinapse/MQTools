﻿using System.Threading.Tasks;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class DataVersionControllerStub : IDataVersionController
    {
        public delegate DataInfo GetLastDataVersionCallback(string publisherId);

        public GetLastDataVersionCallback OnGetLastDataInfo { get; set; }
        
        public DataInfo GetLastDataInfo(string publisherId)
        {
            var re = OnGetLastDataInfo?.Invoke(publisherId);
            return re ?? new DataInfo{DataIndex = -1, LastCreationTimestamp = 0};
        }

        public delegate void StoreDataInfoCallback(string publisherId, DataInfo info);
        public StoreDataInfoCallback OnStoreDataInfo { get; set; }

        public Task StoreDataInfo(string publisherId, DataInfo info)
        {
            OnStoreDataInfo?.Invoke(publisherId, info);
            return Task.CompletedTask;
        }

    }
}