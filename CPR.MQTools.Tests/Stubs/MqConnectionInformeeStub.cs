﻿using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;

namespace CPR.MQTools.Tests.Stubs
{
    public class MqConnectionInformeeStub : MqFunctionBase, IMqConnectionInformee
    {
        public ConnectionCallback EntityOnline { get; set; }
        public DisconnectionCallback EntityOffline { get; set; }
        public BindingCallback EntityBound { get; set; }
        public IdentitySetCallback IdentitySet { get; set; }
    }
}