﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CPR.MQTools.Tests
{
    public static class Helper
    {
        public static void ShouldReceiveSignal(ManualResetEvent manualEvent, TimeSpan timeout, Task task,
            string exceptionMessage)
        {
            CheckSignal(manualEvent, timeout, task, exceptionMessage, true);
        }

        public static void ShouldNotReceiveSignal(ManualResetEvent manualEvent, TimeSpan timeout, Task task,
            string exceptionMessage)
        {
            CheckSignal(manualEvent, timeout, task, exceptionMessage, false);
        }

        private static void CheckSignal(ManualResetEvent manualEvent, TimeSpan timeout, Task task,
            string exceptionMessage, bool shouldReceive)
        {
            if (shouldReceive ^ !manualEvent.WaitOne(timeout)) return;
            CheckFault(task);
            throw new Exception(exceptionMessage);
        }

        public static void CheckFault(Task pollerTask)
        {
            if (pollerTask.IsFaulted)
                throw pollerTask.Exception.Flatten();
        }
    }
}