﻿using System;
using System.Text;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class is created to separate specific kinds of netmq frames which will be used to define types of messages. 
    /// </summary>
    public class MessageType : MqFrame
    {
        public MessageType(byte[] buffer) : base(buffer)
        {
        }

        public MessageType(string message) : base(message)
        {
        }

        public MessageType(string message, Encoding encoding) : base(message, encoding)
        {
        }

        public MessageType(int value) : base(value)
        {
        }

        public MessageType(long value) : base(value)
        {
        }

        public MessageType(byte value) : base(value)
        {
        }

        public MessageType(NetMQFrame frame) : base(frame)
        {
        }

        public MessageType(Enum enumValue) : base(enumValue)
        {
        }
    }
}