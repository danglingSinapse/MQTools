using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will listen on a udp port and discovers endpoints which serves a required type of service.
    /// If the udp beacons from an endpoint was not received for a configured timespan, this functionality will deduce
    /// this endpoint to have became offline. This will also play as a load balancer functionality which will pass
    /// messages in a round robin fashion.
    /// </summary>
    public sealed class UdpDiscoveryDevice : DiscoveryDevice
    {
        /// <summary>
        /// The beacon object which will receive udp packets.
        /// </summary>
        private readonly UdpBeacon _beacon;

        /// <summary>
        /// The port which this functionality should listen for udp beacons.
        /// </summary>
        private readonly int _broadcastPort;

        private readonly string _desiredService;

        /// <param name="broadcastPort">The port which this functionality should listen for udp beacons.</param>
        /// <param name="desiredService">The desired service that should be discovered.</param>
        /// <param name="deadNodeTimeout">This is the time which each discovered endpoint has to send beacons before it
        /// is deduced to be offline.</param>
        public UdpDiscoveryDevice(int broadcastPort, string desiredService, TimeSpan? deadNodeTimeout = null)
            : base(desiredService, deadNodeTimeout)
        {
            _broadcastPort = broadcastPort;
            _desiredService = desiredService;
            _beacon = new UdpBeacon(broadcastPort);
            _beacon.OnReceive += BeaconReceive;
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            _beacon.Subscribe(_desiredService, entity.PollerTaskFactory);
        }

        /// <summary>
        /// This method will be called when a new beacon has been received. This will unwrap the message and if the
        /// endpoint is new, it will add it to the discovered endpoints. If it is not new, its latest received beacon
        /// will be updated.
        /// </summary>
        private void BeaconReceive(object sender, string message)
        {
            var parts = message.Split('@');
            if (parts.Length < 2)
            {
                Entity.Logger.Warn($"Received a corrupted beacon: {message}");
                return;
            }

            if (DesiredService != parts[0])
                throw new Exception($"Received beacon of service {parts[0]} which is not desired");

            var node = new Identity(parts[1]);
            if (!Nodes.ContainsKey(node))
            {
                Nodes.Add(node, DateTime.Now);
                Entity.Logger.Info($"Discovered(udp): {message}");
                Entity.Connect(node.ConvertToString());
            }
            else
            {
                Nodes[node] = DateTime.Now;
            }
        }

        public override Task StopGracefully()
        {
            _beacon.Dispose();
            return base.StopGracefully();
        }

        public override object GetHealthInformation()
        {
            var baseInfo = base.GetHealthInformation();
            var myType = baseInfo.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());

            return new
            {
                desired_service = props.FirstOrDefault(x => x.Name == "desired_service")?.GetValue(baseInfo, null),
                nodes = props.FirstOrDefault(x => x.Name == "nodes")?.GetValue(baseInfo, null),
                listening_upd_port = _broadcastPort
            };
        }
    }
}