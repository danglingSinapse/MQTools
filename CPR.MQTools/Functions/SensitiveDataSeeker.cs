﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    public delegate void SensitiveDataHandler(string sensitiveData);

    /// <summary>
    /// This functionality will simply ask for a sensitive data from any source it can until this data is provided by
    /// one of them. This can be used to fetch a configuration from the remote entities.
    /// </summary>
    public class SensitiveDataSeeker : MqFunctionBase, IMqReceiveMiddleware, IMqConnectionInformee
    {
        /// <summary>
        /// This timer will be used to send SDATA packets on each of its elapsed ticks.
        /// </summary>
        private readonly NetMQTimer _timer;
        
        /// <summary>
        /// The actual seeking data that will be filled when it is received.
        /// </summary>
        private string _sensitiveData;
        
        /// <summary>
        /// The name of the seeking data.
        /// </summary>
        private readonly string _dataName;
        
        /// <summary>
        /// This is the list of all connected peers. It will be used to ask for the sensitive data from all of them.
        /// </summary>
        private readonly List<Identity> _identities;
        
        /// <summary>
        /// The last time which this sensitive data has been received.
        /// </summary>
        private DateTime _lastReceiveTime;

        /// <summary>
        /// This event will be called when the seeking sensitive data has been received. Applications can use this
        /// event to be informed about having this sensitive data.
        /// </summary>
        public event SensitiveDataHandler OnSensitiveDataReceive;

        public ConnectionCallback EntityOnline => identity => _identities.Add(identity);
        public DisconnectionCallback EntityOffline => identity => _identities.Remove(identity);
        public BindingCallback EntityBound => port => { };
        public IdentitySetCallback IdentitySet => identity => { };

        /// <param name="dataName">he name of the seeking data that should be seeked.</param>
        public SensitiveDataSeeker(string dataName)
        {
            _dataName = dataName;
            _identities = new List<Identity>();
            _timer = new NetMQTimer(TimeSpan.FromSeconds(1));
            _timer.Elapsed += (sender, args) =>
            {
                _identities.ForEach(x => Entity.SendMessage(x, new MessageType($"NDATA({_dataName})")));
            };
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.Poller.Add(_timer);
        }

        /// <summary>
        /// This is the entry point of the received data. It will unwrap the message, save the data, inform the
        /// application by the OnSensitiveDataReceive event and stops the timer so that asking for data can be stopped.
        /// </summary>
        private Task SensitiveDataReceived(object sender, Identity identity, MessageType messageType,
            NetMQMessage message)
        {
            if (_sensitiveData != null) return Task.CompletedTask;
            _sensitiveData = message.First.ConvertToString();
            OnSensitiveDataReceive?.Invoke(_sensitiveData);
            _timer.Enable = false;
            _lastReceiveTime = DateTime.Now;
            return Task.CompletedTask;
        }

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo
                {
                    Callback = SensitiveDataReceived,
                    MessageType = new MessageType($"SDATA({_dataName})")
                }
            };
        }

        /// <summary>
        /// This method can be used to retrieve the seeked data.
        /// </summary>
        /// <returns>The data that this functionality was configured to find. Returns null if it has not been found
        /// yet.</returns>
        public string GetSensitiveData()
        {
            return _sensitiveData;
        }

        public override object GetHealthInformation()
        {
            return new
            {
                seeking_data = _dataName,
                is_received = !string.IsNullOrEmpty(_sensitiveData),
                last_receive_time = _lastReceiveTime
            };
        }
    }
}