﻿using System;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This class is the base of all the functionalities which perform broadcasting of a service so that it can be
    /// engaged in the service discovery procedure.  
    /// </summary>
    public abstract class ExistenceBroadcaster : MqFunctionBase, IMqConnectionInformee
    {
        /// <summary>
        /// The identity of the current socket.
        /// </summary>
        protected Identity SelfIdentity;
        
        /// <summary>
        /// The name of the service which should be broadcast.
        /// </summary>
        protected readonly string Role;
        
        /// <summary>
        /// The name of the network interface which the beacons should be broadcast on.
        /// </summary>
        private readonly string _networkInterfaceName;
        
        public ConnectionCallback EntityOnline { get; }
        public DisconnectionCallback EntityOffline { get; }
        public BindingCallback EntityBound { get; }
        public IdentitySetCallback IdentitySet { get; }

        /// <param name="role">The name of the service which should be broadcast.</param>
        /// <param name="selfIdentity">The identity of the current socket.</param>
        /// <param name="networkInterfaceName">The name of the network interface which the beacons should be broadcast
        /// on.</param>
        public ExistenceBroadcaster(string role, Identity selfIdentity = null, string networkInterfaceName = null)
        {
            EntityOnline = identity => { };
            EntityOffline = identity => { };
            EntityBound = Bound;
            IdentitySet = identity => { };

            SelfIdentity = selfIdentity;
            Role = role;
            _networkInterfaceName = networkInterfaceName;
        }

        /// <summary>
        /// This method will be called when the current socket has been bound to a port for serving a service. This will
        /// set the identity of this socket to be the address of this service, then it will notify the mqEntity object
        /// about changing the identity.
        /// </summary>
        /// <param name="port">The port which this socket has been bound to.</param>
        protected virtual void Bound(int port)
        {
            if (SelfIdentity == null)
            {
                var ip = NetworkHelper.GetCurrentIp(_networkInterfaceName);
                SelfIdentity = new Identity($"tcp://{ip}:{port}");
            }

            Entity.Socket.Options.Identity = SelfIdentity.ToByteArray();
            Entity.IdentitySet(SelfIdentity);
        }

        public override object GetHealthInformation()
        {
            return new
            {
                self_identity = SelfIdentity?.ConvertToBase64(),
                role = Role,
                interface_name = _networkInterfaceName
            };
        }

        public override Task StopGracefully()
        {
            base.StopGracefully();
            return Task.CompletedTask;
        }
    }
}