using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;
using StackExchange.Redis;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will subscribe on a redis broadcast topic (the desired service) and discovers endpoints
    /// which serves a required type of service. If the broadcast messages an endpoint was not received for a
    /// configured timespan, this functionality will deduce this endpoint to have became offline. This will also play
    /// as a load balancer functionality which will pass messages in a round robin fashion.
    /// </summary>
    public sealed class RedisDiscoveryDevice : DiscoveryDevice
    {
        private readonly string _redisConnectionString;
        private readonly ISubscriber _subscriber;
        
        public RedisDiscoveryDevice(string connectionString, string desiredService, TimeSpan? deadNodeTimeout = null)
            : base(desiredService, deadNodeTimeout)
        {
            _redisConnectionString = connectionString;
            var redis = ConnectionMultiplexer.Connect(connectionString);
            _subscriber = redis.GetSubscriber();
            _subscriber.Subscribe(desiredService, ReceivedRedisPublication);
        }

        /// <summary>
        /// This method will be called when a new publish has been received on redis. If the endpoint is new, it will
        /// add it to the discovered endpoints. If it is not new, its latest received approved existence will be
        /// updated.
        /// </summary>
        private void ReceivedRedisPublication(RedisChannel channel, RedisValue message)
        {
            var nodeIdentity = new Identity((string)message);
            if (!Nodes.ContainsKey(nodeIdentity))
            {
                Nodes.Add(nodeIdentity, DateTime.Now);
                Entity.Logger.Info($"Discovered(redis): {DesiredService}@{message}");
                Entity.Connect(message);
            }
            else
            {
                Nodes[nodeIdentity] = DateTime.Now;
            }
        }

        public override object GetHealthInformation()
        {
            var baseInfo = base.GetHealthInformation();
            var myType = baseInfo.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());

            return new
            {
                desired_service = props.FirstOrDefault(x => x.Name == "desired_service")?.GetValue(baseInfo, null),
                nodes = props.FirstOrDefault(x => x.Name == "nodes")?.GetValue(baseInfo, null),
                redis_connection = _redisConnectionString
            };
        }
        
        public override Task StopGracefully()
        {
            _subscriber.UnsubscribeAll();
            return base.StopGracefully();
        }
    }
}