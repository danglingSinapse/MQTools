using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;
using StackExchange.Redis;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will broadcast the existence of this socket socket which will serve a configured type of
    /// service using the redis broadcast feature.
    /// </summary>
    public class RedisExistenceBroadcaster : ExistenceBroadcaster
    {
        /// <summary>
        /// This timer will be used to broadcast on redis on its every elapsed ticks. 
        /// </summary>
        private readonly NetMQTimer _timer;
        
        /// <summary>
        /// The redis connection which this functionality will use to publish messages.
        /// </summary>
        private readonly string _redisConnectionString;
        
        /// <summary>
        /// The subscriber object which will be configured to subscribe on the given role as the redis broadcast topic.
        /// </summary>
        private readonly ISubscriber _subscriber;

        /// <param name="connectionString">The redis connection which this functionality will use to publish messages.</param>
        /// <param name="role">The name of the service which should be broadcast.</param>
        /// <param name="selfIdentity">The identity of the current socket.</param>
        /// <param name="networkInterfaceName">The name of the network interface which the beacons should be broadcast
        /// on.</param>
        /// <param name="broadcastInterval">The time interval between redis publishes.</param>
        public RedisExistenceBroadcaster(string connectionString, string role, Identity selfIdentity = null,
            string networkInterfaceName = null, TimeSpan? broadcastInterval = null)
            : base(role, selfIdentity, networkInterfaceName)
        {
            _redisConnectionString = connectionString;
            var redis = ConnectionMultiplexer.Connect(connectionString);
            _subscriber = redis.GetSubscriber();
            var bInterval = broadcastInterval ?? TimeSpan.FromSeconds(1);
            _timer = new NetMQTimer(bInterval);
            _timer.Elapsed += TimerOnElapsed;
            _timer.Enable = false;
        }

        private void TimerOnElapsed(object sender, NetMQTimerEventArgs e)
        {
            if (SelfIdentity != null)
                _subscriber.Publish($"{Role}", SelfIdentity.ConvertToString());
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.Poller.Add(_timer);
        }

        protected override void Bound(int port)
        {
            base.Bound(port);
            _timer.Enable = true;
        }

        public override object GetHealthInformation()
        {
            var baseInfo = base.GetHealthInformation();
            var myType = baseInfo.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());

            return new
            {
                self_identity = props.FirstOrDefault(x => x.Name == "self_identity")?.GetValue(baseInfo, null),
                role = props.FirstOrDefault(x => x.Name == "role")?.GetValue(baseInfo, null),
                interface_name = props.FirstOrDefault(x => x.Name == "interface_name")?.GetValue(baseInfo, null),
                redis_connection = _redisConnectionString
            };
        }

        public override Task StopGracefully()
        {
            _timer.Enable = false;
            return base.StopGracefully();
        }
    }
}