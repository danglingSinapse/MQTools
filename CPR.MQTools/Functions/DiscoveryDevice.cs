﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This class is the base class of all the functionalities which perform service discovery. They should gather the
    /// broadcasts about a service's existence then the offline nodes will be deduced by this class.
    /// </summary>
    public abstract class DiscoveryDevice : MqFunctionBase, IMqConnectionInformer, IMqLoadBalancer
    {
        /// <summary>
        /// This is the time which each discovered endpoint has to prove their existence before it is deduced to be
        /// offline.
        /// </summary>
        private readonly TimeSpan _deadNodeTimeout;
        
        /// <summary>
        /// The timer which will be set to find the dead nodes.
        /// </summary>
        private readonly NetMQTimer _timer;

        /// <summary>
        /// This dictionary maps each discovered node to the latest time that their existence has been approved.  
        /// </summary>
        protected readonly Dictionary<Identity, DateTime> Nodes;
        
        /// <summary>
        /// The desired service that should be discovered.
        /// </summary>
        protected readonly string DesiredService;
        
        /// <summary>
        /// The index of the latest node which has been chosen to pass a message to. This will help to choose the nodes
        /// for sending messages in a round robin fashion.
        /// </summary>
        private int _currentNodeIndex;

        public event ConnectionCallback EntityOnline;
        public event DisconnectionCallback EntityOffline;
        public event BindingCallback EntityBound;
        public event IdentitySetCallback IdentitySet;

        public AddEntityMonitoringCallback AddEntityMonitoring => identity => { };
        public RemoveEntityMonitoringCallback RemoveEntityMonitoring => identity => { };

        /// <param name="desiredService">The desired service that should be discovered.</param>
        /// <param name="deadNodeTimeout">This is the time which each discovered endpoint has to prove their existence
        /// before it is deduced to be offline.</param>
        protected DiscoveryDevice(string desiredService, TimeSpan? deadNodeTimeout = null)
        {
            Nodes = new Dictionary<Identity, DateTime>();
            _deadNodeTimeout = deadNodeTimeout ?? TimeSpan.FromSeconds(10);
            DesiredService = desiredService;

            _timer = new NetMQTimer(TimeSpan.FromSeconds(1));
            _timer.Elapsed += ClearDeadNodes;
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.Poller.Add(_timer);
        }

        /// <summary>
        /// This method will be called every second to check if a node has become offline or not.
        /// </summary>
        private void ClearDeadNodes(object sender, NetMQTimerEventArgs e)
        {
            var deadNodes = Nodes.Where(n => DateTime.Now > n.Value + _deadNodeTimeout)
                .Select(n => n.Key).ToArray();

            foreach (var node in deadNodes)
            {
                Nodes.Remove(node);
                Entity.Logger.Info($"Offline: {node.ConvertToString()}");
                Entity.Disconnect(node.ConvertToString());
            }
        }

        /// <summary>
        /// This method will select the next node for sending messages in a round robin fashion.
        /// </summary>
        /// <returns>The next node to send a message.</returns>
        public Identity GetNextNode()
        {
            _currentNodeIndex = Nodes.Any() ? _currentNodeIndex %= Nodes.Count : 0;
            return Nodes.ElementAtOrDefault(_currentNodeIndex++).Key;
        }
        
        public override object GetHealthInformation()
        {
            return new
            {
                desired_service = DesiredService,
                nodes = Nodes.Select(x => new {address = x.Key.ConvertToBase64(), last_seen_at = x.Value})
            };
        }

        public override Task StopGracefully()
        {
            base.StopGracefully();
            _timer.Enable = false;
            Nodes.Clear();
            return Task.CompletedTask;
        }
    }
}