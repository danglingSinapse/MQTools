using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will broadcast the existence of this socket which will serve a configured type of service
    /// using udp beacons on a specific port.
    /// </summary>
    public class UdpExistenceBroadcaster : ExistenceBroadcaster
    {
        /// <summary>
        /// The beacon object which will be used to publish udp messages.
        /// </summary>
        private readonly UdpBeacon _beacon;

        /// <summary>
        /// The time between each broadcast on the wire.
        /// </summary>
        private readonly TimeSpan _broadcastInterval;

        /// <summary>
        /// The port which should be used for upd broadcasts.
        /// </summary>
        private readonly int _broadcastPort;

        private readonly string _networkInterfaceName;


        /// <param name="broadcastPort">The port which should be used for upd broadcasts.</param>
        /// <param name="role">The name of the service which should be broadcast.</param>
        /// <param name="selfIdentity">The identity of the current socket.</param>
        /// <param name="networkInterfaceName">The name of the network interface which the beacons should be broadcast
        /// on.</param>
        /// <param name="broadcastInterval">The time between each broadcast on the wire.</param>
        public UdpExistenceBroadcaster(int broadcastPort, string role, Identity selfIdentity = null,
            string networkInterfaceName = null, TimeSpan? broadcastInterval = null) 
            : base(role, selfIdentity, networkInterfaceName)
        {
            _broadcastInterval = broadcastInterval ?? TimeSpan.FromSeconds(1);
            _broadcastPort = broadcastPort;
            _networkInterfaceName = networkInterfaceName;
            _beacon = new UdpBeacon(_broadcastPort);
        }

        /// <summary>
        /// This method will be called when the current socket has been bound to a port for serving a service. This will
        /// set the identity of this socket to be the address of this service, then it will notify the mqEntity object
        /// about changing the identity.
        /// </summary>
        /// <param name="port">The port which this socket has been bound to.</param>
        protected override void Bound(int port)
        {
            base.Bound(port);
            _beacon.Publish($"{Role}@{SelfIdentity.ConvertToString()}", Entity.Poller, _broadcastInterval, _networkInterfaceName);
        }

        public override object GetHealthInformation()
        {
            var baseInfo = base.GetHealthInformation();
            var myType = baseInfo.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());

            return new
            {
                self_identity = props.FirstOrDefault(x => x.Name == "self_identity")?.GetValue(baseInfo, null),
                role = props.FirstOrDefault(x => x.Name == "role")?.GetValue(baseInfo, null),
                interface_name = props.FirstOrDefault(x => x.Name == "interface_name")?.GetValue(baseInfo, null),
                broadcast_port = _broadcastPort
            };
        }

        public override Task StopGracefully()
        {
            _beacon.StopPublish();
            _beacon.Dispose();
            return base.StopGracefully();
        }
    }
}