﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality can be used to publish data for some consumers in a cautiously manner! It will assign an
    /// incremental index to each published data. And if a subscriber is out of sync with the published data, it will
    /// cooperate with it to fetch the previously published data and publish them again.
    /// </summary>
    public class CautiousDataPublisher : MqFunctionBase, IMqReceiveMiddleware, IMqConnectionInformee
    {
        /// <summary>
        /// The data source object which will represent an arbitrary repository for storing and restoring published
        /// data. This repository may keep the published data for an arbitrary amount of time and remove them afterward.
        /// If fetching of data was not successful, it may return null which will result in a MISS packet for that data
        /// index.
        /// </summary>
        private readonly IDataSource _dataSource;
        
        /// <summary>
        /// The version controller object which will be used to save and retrieve the latest version information from
        /// an arbitrary source.
        /// </summary>
        private readonly IDataVersionController _versionController;
        
        /// <summary>
        /// List of subscribers which this socket has been connected to. 
        /// </summary>
        private readonly List<Identity> _consumers;
        
        /// <summary>
        /// The cached last index of published data.
        /// </summary>
        private long _lastDataIndex;
        
        /// <summary>
        /// The id of this publisher which will be used to support multi publisher scenarios.
        /// </summary>
        private readonly string _publisherId;
        
        /// <summary>
        /// The queue which will be used to publish the data on the wire.
        /// </summary>
        private readonly ConcurrentQueue<MissingData> _publishQueue;
        
        /// <summary>
        /// This completion source determines whether it is ok to stop gracefully or not.
        /// </summary>
        private readonly TaskCompletionSource<object> _publishCompletion = new TaskCompletionSource<object>();
        
        /// <summary>
        /// This will propagate the publish event so that the process loop can wake up and continue.
        /// </summary>
        private readonly ManualResetEventSlim _publishEvent = new ManualResetEventSlim();

        /// <param name="dataSource">The data source object which will represent an arbitrary repository for storing and
        /// restoring published data. This repository may keep the published data for an arbitrary amount of time and
        /// remove them afterward. If fetching of data was not successful, it may return null which will result in a
        /// MISS packet for that data index.</param>
        /// <param name="dataVersionController">The version controller object which will be used to save and retrieve
        /// the latest version information from an arbitrary source.</param>
        /// <param name="publisherId">The id of this publisher which will be used to support multi publisher scenarios.
        /// The default value will use current machine's ip address as publisherId.</param>
        /// <param name="networkInterfaceName">In case of using ip address as the publisherId, this can be set to
        /// specify the interface name for retrieving ip address. The default value will use the first up and running
        /// ethernet interface.</param>
        public CautiousDataPublisher(IDataSource dataSource, IDataVersionController dataVersionController,
            string publisherId = "", string networkInterfaceName = "")
        {
            _dataSource = dataSource;
            _versionController = dataVersionController;
            _consumers = new List<Identity>();
            _publisherId = publisherId;
            _publishQueue = new ConcurrentQueue<MissingData>();
            if (string.IsNullOrEmpty(_publisherId))
            {
                _publisherId = NetworkHelper.GetCurrentIp(networkInterfaceName);
            }

            var info = _versionController.GetLastDataInfo(_publisherId);
            _lastDataIndex = info?.DataIndex ?? -1;
        }

        /// <summary>
        /// This method creates a process loop for processing the publishing of data in publish queue. It will await on
        /// publishing events, then will try to empty the publishing queue. On each data, it will call the store it and
        /// its data info using the data source and version control objects. Then it will send them on wire. Note that
        /// the published data will only be taken out of the publish queue if no exception has been occured during the
        /// processes. 
        /// </summary>
        private async Task ProcessPublish()
        {
            try
            {
                await _publishEvent.WaitHandle.AsTask();
                _publishEvent.Reset();

                while (_publishQueue.Any())
                {
                    var data = new MissingData(_publishQueue.First())
                    {
                        CreationTimestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
                        DataIndex = ++_lastDataIndex
                    };
                    
                    await Task.WhenAll(
                        Task.Factory.StartNew(() => _dataSource.Store(_publisherId, data.DataIndex, data)),
                        Task.Factory.StartNew(() => _versionController.StoreDataInfo(_publisherId,
                            new DataInfo
                                {DataIndex = data.DataIndex, LastCreationTimestamp = data.CreationTimestamp})));
                    
                    WrapMessage(data.Message, data.DataIndex, data.CreationTimestamp, data.MessageType);
                    foreach (var consumer in _consumers)
                    {
                        Entity.SendMessage(consumer, new MessageType("DATA"), new NetMQMessage(data.Message))
                            .ConfigureAwait(false);
                    }

                    _publishQueue.TryDequeue(out _);
                }

                if (IsShuttingDown)
                {
                    _publishCompletion.SetResult(null);
                }
            }
            catch (Exception e)
            {
                Entity.Logger.Error(e, "exception thrown while processing the publish of a data");
            }
            finally
            {
                Entity.PollerTaskFactory.StartNew(ProcessPublish).ConfigureAwait(false);
            }
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.PollerTaskFactory.StartNew(ProcessPublish);
        }
        
        public ConnectionCallback EntityOnline => identity => { _consumers.Add(identity); };

        public DisconnectionCallback EntityOffline => identity => { _consumers.Remove(identity); };

        public BindingCallback EntityBound => port => { };

        public IdentitySetCallback IdentitySet => identity => { };

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo {Callback = FetchData, MessageType = new MessageType("FETCH")}
            };
        }

        /// <summary>
        /// This method is the entry point which will result in publishing a data. This will enqueue the giving data
        /// in the publish queue.
        /// </summary>
        /// <param name="messageType">The message type of the data that is going to be published.</param>
        /// <param name="message">The actual message which represents the body of the publishing data.</param>
        public void Publish(MessageType messageType, NetMQMessage message)
        {
            _publishQueue.Enqueue(new MissingData
            {
                Message = message,
                MessageType = messageType
            });
            _publishEvent.Set();
        }

        /// <summary>
        /// This method will handle fetch requests which will be sent to resolve an out-of-sync subscriber of these data.
        /// This expects one or two indices to fetch an specific index or a range of indices from the first to the second
        /// passed index.
        /// </summary>
        private async Task FetchData(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            var fromIndex = message.Pop().ConvertToInt64();
            if (message.Any())
            {
                var toIndex = message.Pop().ConvertToInt64();
                var fetchedData =
                    (await Task.Factory.StartNew(() => _dataSource.RestoreRange(_publisherId, fromIndex, toIndex))
                        .Unwrap()).ToList();

                for (var i = fromIndex; i <= toIndex; ++i)
                {
                    var data = fetchedData.FirstOrDefault(x => x != null && x.DataIndex == i);
                    SendMessage(identity, i, data);
                }
            }
            else
            {
                SendMessage(identity, fromIndex, await _dataSource.Restore(_publisherId, fromIndex));
            }
        }

        /// <summary>
        /// This method will prepare a fetched missing data to be sent on wire. It will put MISS frame for data body
        /// if it was not presented.
        /// </summary>
        /// <param name="identity">The identity of the target subscriber for this data to be sent.</param>
        /// <param name="expectingIndex">The expecting index for this data.</param>
        /// <param name="data">The fetched object for the missing data.</param>
        private void SendMessage(Identity identity, long expectingIndex, MissingData data)
        {
            var presented = data?.Message != null && data.DataIndex == expectingIndex;
            var sendingMessage = presented ? new NetMQMessage(data.Message) : new NetMQMessage();
            WrapMessage(sendingMessage, expectingIndex, presented ? data.CreationTimestamp : 0,
                presented ? data.MessageType : new MessageType("MISS"));
            Entity.SendMessage(identity, new MessageType("DATA"), sendingMessage);
        }

        /// <summary>
        /// This method wraps a data instance to become ready to be sent on the wire.
        /// </summary>
        /// <param name="message">The message body of this data.</param>
        /// <param name="dataIndex">The index of this data.</param>
        /// <param name="timestamp">The timestamp of publishing this data.</param>
        /// <param name="messageType">The message type of this data.</param>
        private void WrapMessage(NetMQMessage message, long dataIndex, long timestamp, MessageType messageType)
        {
            message.Push(messageType);
            message.Push(timestamp);
            message.Push(dataIndex);
            message.Push(_publisherId);
        }

        /// <summary>
        /// The health information gathering method of this function, Which contains consumers list, publisher id of
        /// this publisher and the last index of data which was published.
        /// </summary>
        /// <returns></returns>
        public override object GetHealthInformation()
        {
            return new
            {
                consumers = _consumers.Select(x => x.ConvertToBase64()),
                publisher_id = _publisherId,
                last_index = _lastDataIndex
            };
        }

        /// <summary>
        /// Gracefully stopping method, which its task will be completed when the publish queue is empty.
        /// </summary>
        public override Task StopGracefully()
        {
            base.StopGracefully();
            _publishEvent.Set();
            return _publishCompletion.Task;
        }
    }
}