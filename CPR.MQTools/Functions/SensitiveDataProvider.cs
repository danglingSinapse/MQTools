﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will simply answer NDATA packets of a given type and provides a sensitive data which the
    /// asking socket depends on. This can be used to share configurations between entities. 
    /// </summary>
    public class SensitiveDataProvider : MqFunctionBase, IMqReceiveMiddleware
    {
        /// <summary>
        /// The actual data which is the answer of NDATA packets.
        /// </summary>
        private readonly string _sensitiveData;
        
        /// <summary>
        /// The name of this data which will be asked by an unknown entity.
        /// </summary>
        private readonly string _dataName;

        /// <param name="dataName">The actual data which is the answer of NDATA packets.</param>
        /// <param name="sensitiveData">The name of this data which will be asked by an unknown entity.</param>
        public SensitiveDataProvider(string dataName, string sensitiveData)
        {
            _dataName = dataName;
            _sensitiveData = sensitiveData;
        }

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo
                {
                    Callback = SensitiveDataRequestReceived,
                    MessageType = new MessageType($"NDATA({_dataName})")
                }
            };
        }

        /// <summary>
        /// This is the method which will be called if an NDATA packet is received. It will immediately send the
        /// sensitive data to the requester.
        /// </summary>
        private Task SensitiveDataRequestReceived(IMqEntity sender, Identity identity, MessageType messagetype,
            NetMQMessage message)
        {
            var response = new NetMQMessage(new[] {new NetMQFrame(_sensitiveData)});
            sender.SendMessage(identity, new MessageType($"SDATA({_dataName})"), response);
            return Task.CompletedTask;
        }

        public override object GetHealthInformation()
        {
            return new
            {
                data_name = _dataName
            };
        }
    }
}