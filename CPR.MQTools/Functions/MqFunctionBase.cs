﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;

[assembly: InternalsVisibleTo("CPR.MQTools.Tests")]

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This class is the base class for any functionality that wants to be implemented.
    /// </summary>
    public abstract class MqFunctionBase
    {
        /// <summary>
        /// The entity which this functionality is added to. The default is going to be the type name of this class.
        /// </summary>
        protected internal IMqEntity Entity;
        
        /// <summary>
        /// The name of this functionality which will be used in the health information object.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// A boolean that represents whether the entity is going to be shutdown or not. 
        /// </summary>
        protected bool IsShuttingDown = false;

        protected MqFunctionBase()
        {
            Name = GetType().Name;
        }

        /// <summary>
        /// This method will be called by the MqEntity class to set the entity instance which this functionality has
        /// been added to.
        /// </summary>
        /// <param name="entity">The entity which this functionality has been added to.</param>
        public virtual void SetEntity(IMqEntity entity)
        {
            Entity = entity;
        }

        /// <summary>
        /// This method will be called when there is a request to see the real-time health metric of this functionality.
        /// This should be overriden to attach a customized object for this functionality in the health information.
        /// </summary>
        /// <returns>The object representing the health information of this functionality.</returns>
        public virtual object GetHealthInformation()
        {
            return null;
        }

        /// <summary>
        /// This method will be called when the system wants to be shutdown but in a graceful matter. This should be
        /// overriden to implement customized strategies for graceful shutdowns.
        /// </summary>
        /// <returns>A task which only be completed when this functionality is ready for shutdown.</returns>
        public virtual Task StopGracefully()
        {
            IsShuttingDown = true;
            return Task.CompletedTask;
        }
    }
}