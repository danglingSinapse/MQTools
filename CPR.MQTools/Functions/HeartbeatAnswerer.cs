﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will just answer PING packets by PONG. It can be used for the entities that their health may
    /// be monitored by pinging.
    /// </summary>
    public class HeartbeatAnswerer : MqFunctionBase, IMqReceiveMiddleware
    {
        private Task SocketReceivedMessage(object sender, Identity identity, MessageType messageType,
            NetMQMessage message)
        {
            Entity.SendMessage(identity, new MessageType("PONG"));
            return Task.CompletedTask;
        }

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
                {new ReceiveHandleInfo {Callback = SocketReceivedMessage, MessageType = new MessageType("PING")}};
        }
    }
}