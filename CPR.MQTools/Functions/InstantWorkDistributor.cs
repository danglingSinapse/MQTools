﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will add the capability to distribute works for some other remote sockets that have the
    /// worker functionality. It will accept new works from the application through its API then instantly it will send
    /// the passed work to a worker. The socket that have this functionality will usually connect to a socket that has
    /// RelaxedWorker functionality. This way the works will be accumulated in RelaxedWorker to be done.
    /// </summary>
    public class InstantWorkDistributor : MqFunctionBase, IMqReceiveMiddleware, IMqConnectionInformee
    {
        /// <summary>
        /// This class encapsulates the information about a work that needs to be passed to a worker to be done. It
        /// implements (de)serialization to byte array to have a simple API for repositories.
        /// </summary>
        [Serializable]
        public class Work : SerializableObject<Work>, ISerializable, IComparable<Work>
        {
            /// <summary>
            /// The message that should be sent as a work to a worker.
            /// </summary>
            public NetMQMessage Message;
            
            /// <summary>
            /// The priority of this work. Works with lower value for their priority will be sent sooner than the ones
            /// with higher value as their priority.
            /// </summary>
            public int Priority;
            
            /// <summary>
            /// The number of sending to workers that has been done for this work.
            /// </summary>
            public int TryCount = 1;
            
            /// <summary>
            /// The time of the instantiation of this object.
            /// </summary>
            public DateTime CreationTime = DateTime.Now;
            
            /// <summary>
            /// The latest time which this work has been passed to a worker.
            /// </summary>
            public DateTime DeliverTime = DateTime.MinValue;
            
            /// <summary>
            /// A task that will be completed when this work has been passed to a worker and gets an index.
            /// </summary>
            public readonly TaskCompletionSource<long> WorkIndexTask = new TaskCompletionSource<long>();

            public int CompareTo(Work other)
            {
                return Priority == other.Priority
                    ? CreationTime.CompareTo(other.CreationTime)
                    : Priority.CompareTo(other.Priority);
            }

            public Work()
            {
            }

            private Work(SerializationInfo info, StreamingContext context)
            {
                Message = new NetMQMessage((byte[][]) info.GetValue("m", typeof(byte[][])));
                Priority = (int) info.GetValue("p", typeof(int));
                TryCount = (int) info.GetValue("t", typeof(int));
                CreationTime = (DateTime) info.GetValue("c", typeof(DateTime));
                DeliverTime = (DateTime) info.GetValue("d", typeof(DateTime));
            }

            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("m", Message.Select(x => x.ToByteArray()).ToArray());
                info.AddValue("p", Priority);
                info.AddValue("t", TryCount);
                info.AddValue("c", CreationTime);
                info.AddValue("d", DeliverTime);
            }
        }

        //TODO: pending works should be persist in the injected repository 
        /// <summary>
        /// This dictionary contains the works that has been sent to the the workers to be done and this functionality
        /// expects their results. 
        /// </summary>
        private readonly MultiValueDictionary<Identity, Work> _pendingWorks;
        
        /// <summary>
        /// The index of the last work that has been sent to a worker.
        /// </summary>
        private long _lastWorkIndex;
        
        /// <summary>
        /// The time that will given for each work to be done by a worker before it becomes timed out.
        /// </summary>
        protected readonly TimeSpan WorkTimeout;
        
        /// <summary>
        /// The timer which will be used to detect timed out works.
        /// </summary>
        private readonly NetMQTimer _timer;
        
        /// <summary>
        /// The number of tries for each work before it becomes failed.
        /// </summary>
        protected readonly int MaxWorkTries;
        
        /// <summary>
        /// This queue will be used to publish works to workers.
        /// </summary>
        protected readonly NetMQQueue<Work> WorkPassingQueue;
        
        /// <summary>
        /// This task will be used for gracefully stopping and it will be completed when all the pending works has been
        /// done by workers.
        /// </summary>
        private readonly TaskCompletionSource<object> _pendingWorksCompletion = new TaskCompletionSource<object>();

        public delegate void WorkTimeoutHandler(long index, NetMQMessage work);

        /// <summary>
        /// This event will be invoked when a for has reached the maximum tries and hence it is failed. The application
        /// layer can be informed about it through the invocation of this event.
        /// </summary>
        public event WorkTimeoutHandler OnWorkFailure;

        public delegate void WorkIndexChangeHandler(long before, long after);

        /// <summary>
        /// This event will be invoked when a work has been retried. This will inform the application about the change
        /// in the work index.
        /// </summary>
        public event WorkIndexChangeHandler OnWorkIndexChange;
        
        public ConnectionCallback EntityOnline => identity => { };

        public DisconnectionCallback EntityOffline => identity =>
        {
            var pendings = _pendingWorks.GetValues(identity);
            _pendingWorks.Remove(identity);
            WorksFailed(pendings);
        };

        public BindingCallback EntityBound => port => { };
        public IdentitySetCallback IdentitySet => identity => { };

        /// <param name="workTimeout">The time that will given for each work to be done by a worker before it becomes
        /// timed out.</param>
        /// <param name="maxWorkTries">The number of tries for each work before it becomes failed.</param>
        public InstantWorkDistributor(TimeSpan? workTimeout = null, int maxWorkTries = 3)
        {
            MaxWorkTries = maxWorkTries;
            WorkTimeout = workTimeout ?? TimeSpan.FromMinutes(3);
            _timer = new NetMQTimer(TimeSpan.FromSeconds(1)) {Enable = true};
            _timer.Elapsed += CheckWorksTimeout;
            _pendingWorks = new MultiValueDictionary<Identity, Work>();
            _lastWorkIndex = 0;
            WorkPassingQueue = new NetMQQueue<Work>();
            WorkPassingQueue.ReceiveReady += WorkReady;
        }

        /// <summary>
        /// This method will be called every second to detect the timed out works.
        /// </summary>
        private void CheckWorksTimeout(object sender, NetMQTimerEventArgs e)
        {
            var now = DateTime.Now;
            var removeList = new List<KeyValuePair<Identity, Work>>();
            foreach (var idWorks in _pendingWorks)
            foreach (var work in idWorks.Value)
            {
                if (now > work.DeliverTime.Add(WorkTimeout))
                {
                    removeList.Add(new KeyValuePair<Identity, Work>(idWorks.Key, work));
                }
            }

            foreach (var item in removeList)
            {
                _pendingWorks.Remove(item.Key, item.Value);
                var index = item.Value.Message.Pop().ConvertToInt64();
                if (item.Value.TryCount < MaxWorkTries)
                {
                    ++item.Value.TryCount;
                    RetryWork(index, item.Value);
                }
                else
                    OnWorkFailure?.Invoke(index, item.Value.Message);
            }

            var removeMonitoring = removeList.GroupBy(x => x.Key)
                .Where(x => x.Key != new Identity(-1) && !_pendingWorks.GetValues(x.Key).Any());
            foreach (var item in removeMonitoring)
                Entity.RemoveEntityMonitoring(item.Key);

            if (IsShuttingDown && _pendingWorks.Sum(x => x.Value.Count) == 0)
                _pendingWorksCompletion.SetResult(null);
        }

        public override void SetEntity(IMqEntity entity)
        {
            entity.Poller.Add(_timer);
            entity.Poller.Add(WorkPassingQueue);
            base.SetEntity(entity);
        }

        public virtual IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo {Callback = WorkerResult, MessageType = new MessageType("WORKRESULT")}
            };
        }

        /// <summary>
        /// This method will be called when a work's result has been received. It will resolve its pending work and pass
        /// it to the MqEntity to be processed by the application.
        /// </summary>
        private Task WorkerResult(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            var index = message.Pop().ConvertToInt64();
            var type = message.Pop();
            message.Push(index);
            message.Push(type);
            var pendings = _pendingWorks.GetValues(identity);
            if (pendings.RemoveWhere(x => x.Message[0].ConvertToInt64() == index) == 0)
                Entity.Logger.Warn("work result received but it was not pending");
            else
            {
                if (!pendings.Any())
                    sender.RemoveEntityMonitoring(identity);
                sender.MessageReceived(identity, message);
            }
            
            return Task.CompletedTask;
        }

        /// <summary>
        /// This method will do the actual process for sending a work. It will assign a new index to the passed work,
        /// arrange the frames needed and send it to a target worker through MqEntity's API. It will also add the target
        /// worker to the monitoring list so that it can be informed if it becomes offline.
        /// </summary>
        /// <param name="work">The work object that needs to be sent to a worker.</param>
        /// <param name="identity">The identity of the target worker. The default value will use load balancer
        /// functionalities to determine which connected worker this work should sent to.</param>
        /// <returns>The index of this work.</returns>
        protected async Task<long> SendWork(Work work, Identity identity = null)
        {
            var index = ++_lastWorkIndex;
            work.Message.Push(index);
            if (identity == null)
                identity = await Entity.SendMessage(new MessageType("WORK"), new NetMQMessage(work.Message));
            else
                await Entity.SendMessage(identity, new MessageType("WORK"), new NetMQMessage(work.Message));
            
            if (identity != null && !_pendingWorks.GetValues(identity).Any())
                Entity.AddEntityMonitoring(identity);
            work.DeliverTime = DateTime.Now;
            _pendingWorks.Add(identity ?? new Identity(-1), work);
            return index;
        }
        
        /// <summary>
        /// This method be called when there is a new work in WorkPassingQueue which is waiting to be processed. It will
        /// try to empty the pending queue by sending them using the SendWork method.
        /// </summary>
        protected virtual async void WorkReady(object sender, NetMQQueueEventArgs<Work> e)
        {
            while (WorkPassingQueue.Any())
            {
                var work = WorkPassingQueue.Dequeue();
                work.WorkIndexTask.SetResult(await SendWork(work));                
            }
        }

        /// <summary>
        /// This method can be used to pass a work from the application layer to be sent to to a worker. In this
        /// overload, the type of the work can be passed separately.
        /// </summary>
        /// <param name="type">The message type of this work.</param>
        /// <param name="msg">The body of the work.</param>
        /// <param name="priority">The priority of this work. Works with lower value for their priority will be sent
        /// sooner than the ones with higher value as their priority.</param>
        /// <returns>The index of this work.</returns>
        public Task<long> SendWork(MessageType type, NetMQMessage msg, int priority = 0)
        {
            msg.Push(type);
            return SendWork(msg, priority);
        }
        
        /// <summary>
        /// This method can be used to pass a work from the application layer to be sent to to a worker. In this
        /// overload, the passed message should contain the work's type as its first frame.
        /// </summary>
        /// <param name="msg">The message that </param>
        /// <param name="priority">The priority of this work. Works with lower value for their priority will be sent
        /// sooner than the ones with higher value as their priority.</param>
        /// <returns>The index of this work.</returns>
        /// <exception cref="Exception">This exception will be thrown if the application is shutting down gracefully and
        /// it cannot accept new works to sent to workers.</exception>
        public Task<long> SendWork(NetMQMessage msg, int priority = 0)
        {
            if (IsShuttingDown)
                throw new Exception("cannot accept new work when shutting down");
            
            var work = new Work {Message = msg, Priority = priority};
            WorkPassingQueue.Enqueue(work);
            return work.WorkIndexTask.Task;
        }

        /// <summary>
        /// This method will be called when a work is needed to be sent on the wire again, for retrying the process.
        /// </summary>
        /// <param name="previousIndex">The latest index that was assigned to this work.</param>
        /// <param name="work">The actual work that has been failed.</param>
        protected virtual async void RetryWork(long previousIndex, Work work)
        {
            var newIndex = await SendWork(work);
            OnWorkIndexChange?.Invoke(previousIndex, newIndex);
        }

        /// <summary>
        /// This method will be called when a worker has been disconnected and its pending works is now filed. This
        /// will retry the work if the try count was not exceeded MaxWorkTries. Otherwise, this will fail the work
        /// completely.
        /// </summary>
        /// <param name="works"></param>
        protected void WorksFailed(IEnumerable<Work> works)
        {
            foreach (var work in works)
            {
                var index = work.Message.Pop().ConvertToInt64();
                if (DateTime.Now < work.CreationTime.Add(WorkTimeout) && work.TryCount < MaxWorkTries)
                {
                    ++work.TryCount;
                    RetryWork(index, work);
                }
                else
                {
                    OnWorkFailure?.Invoke(index, work.Message);
                }
            }
        }

        public override object GetHealthInformation()
        {
            return new
            {
                pending_works = _pendingWorks.Sum(x => x.Value.Count),
                works_in_passing_queue = WorkPassingQueue.Count(),
                last_work_index = _lastWorkIndex
            };
        }

        /// <summary>
        /// Stopping gracefully will be done when all the pending works has been done.
        /// </summary>
        /// <returns></returns>
        public override Task StopGracefully()
        {
            base.StopGracefully();
            return _pendingWorksCompletion.Task;
        }
    }
}