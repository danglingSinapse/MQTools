﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will check the health of requested entities by pinging them. If they dont answer the PING
    /// packet for a configured time, it will report them to be offline.
    /// </summary>
    public class HeartbeatAsker : MqFunctionBase, IMqReceiveMiddleware, IMqConnectionInformer
    {
        /// <summary>
        /// This dictionary maps each entity that should be monitored to its latest received PONG packet as the answer
        /// of PING. 
        /// </summary>
        private readonly Dictionary<Identity, DateTime> _monitoringIdentities;
        
        /// <summary>
        /// This timer will be used to determine the dead nodes.
        /// </summary>
        private readonly NetMQTimer _timer;
        
        /// <summary>
        /// The time which any entity has to answer PING packets before it becomes offline.
        /// </summary>
        private readonly TimeSpan _deadIdentityTimeout;

        public event ConnectionCallback EntityOnline;
        public event DisconnectionCallback EntityOffline;
        public event BindingCallback EntityBound;
        public event IdentitySetCallback IdentitySet;

        public AddEntityMonitoringCallback AddEntityMonitoring =>
            identity => _monitoringIdentities[identity] = DateTime.Now;

        public RemoveEntityMonitoringCallback RemoveEntityMonitoring =>
            identity => _monitoringIdentities.Remove(identity);

        /// <param name="monitoringIdentities">The initial list of entities which should be monitored.</param>
        /// <param name="heartbeatInterval">The interval between sending the PING packets.</param>
        /// <param name="deadIdentityTimeout">The time which any entity has to answer PING packets before it becomes
        /// offline.</param>
        public HeartbeatAsker(IEnumerable<Identity> monitoringIdentities = null,
            TimeSpan? heartbeatInterval = null, TimeSpan? deadIdentityTimeout = null)
        {
            var identitiesList = monitoringIdentities ?? new List<Identity>();
            _monitoringIdentities = identitiesList.ToDictionary(x => x, x => DateTime.Now);
            _timer = new NetMQTimer(heartbeatInterval ?? TimeSpan.FromSeconds(1));
            _deadIdentityTimeout = deadIdentityTimeout ?? TimeSpan.FromSeconds(10);
            _timer.Elapsed += TimerTick;
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.Poller.Add(_timer);
        }

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
                {new ReceiveHandleInfo {Callback = SocketReceivedMessage, MessageType = new MessageType("PONG")}};
        }

        private void TimerTick(object sender, NetMQTimerEventArgs args)
        {
            ClearDeadNodes();
            foreach (var id in _monitoringIdentities)
                Entity.SendMessage(id.Key, new MessageType("PING"));
        }

        /// <summary>
        /// This method will be called every second to check if a node has become offline or not.
        /// </summary>
        private void ClearDeadNodes()
        {
            var deadNodes = _monitoringIdentities.Where(n => DateTime.Now > n.Value + _deadIdentityTimeout)
                .Select(n => n.Key).ToArray();

            foreach (var node in deadNodes)
            {
                _monitoringIdentities.Remove(node);
                EntityOffline?.Invoke(node);
            }
        }

        /// <summary>
        /// On receiving PONG packets, the time of the latest received PONG for that identity will be updated.
        /// </summary>
        private Task SocketReceivedMessage(object sender, Identity identity, MessageType messageType,
            NetMQMessage message)
        {
            _monitoringIdentities[identity] = DateTime.Now;
            return Task.CompletedTask;
        }

        public override object GetHealthInformation()
        {
            return new
            {
                monitoring_nodes =
                    _monitoringIdentities.Select(x => new {id = x.Key.ConvertToBase64(), last_check = x.Value})
            };
        }
    }
}