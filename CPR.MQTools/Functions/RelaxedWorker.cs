﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will add the capability of receiving and tracking works that the target socket is set to do.
    /// It will wait for the work messages to be received, then it will engage in unwrapping works and passing them to
    /// the application layer to be processed. Also it will manipulate the results of the works so that the connected
    /// work distributor can track the receiving results. The socket that has this functionality will usually connect
    /// to a socket having InstantWorkDistributor functionality. InstantWorkDistributor will instantly send the works
    /// for RelaxedWorker to be processed hence the works will be queued in RelaxedWorker and that is why it needs a
    /// repository to persist the works in a queue.
    /// </summary>
    public class RelaxedWorker : MqFunctionBase, IMqSendMiddleware, IMqReceiveMiddleware
    {
        /// <summary>
        /// This class is the default implementation of the repository of pending works which stores them in the memory.
        /// </summary>
        private class MemoryPendingWorkRepository : IPendingWorkRepository
        {
            private readonly Dictionary<long, NetMQMessage> _pendingWorks = new Dictionary<long, NetMQMessage>();
            
            public void AddPendingWork(NetMQMessage message, long workIndex)
            {
                _pendingWorks.Add(workIndex, message);
            }

            public bool RemovePendingWork(long workIndex)
            {
                return _pendingWorks.Remove(workIndex);
            }

            public int GetCount()
            {
                return _pendingWorks.Count;
            }
        }

        /// <summary>
        /// This is the repository which will be used to store and retrieve pending works.
        /// </summary>
        private readonly IPendingWorkRepository _pendingWorkRepository;
        
        /// <summary>
        /// This is the type of the result of the works which their sending will be manipulated by this functionality
        /// so that they can be captured by the connected work distributor.
        /// </summary>
        private readonly IEnumerable<MessageType> _resultTypes;
        
        /// <summary>
        /// This task will be used to stop gracefully. It will be completed when there is no pending works in the
        /// repository. 
        /// </summary>
        private readonly TaskCompletionSource<object> _pendingWorksCompletion = new TaskCompletionSource<object>();

        //TODO: revive after crash using repository
        /// <param name="pendingWorkRepository">The repository connected to an arbitrary data source which will be used
        /// to store and retrieve pending works. The default value will use in-memory repository.</param>
        /// <param name="resultTypes">The type of the result of the works which their sending will be manipulated by
        /// this functionality so that they can be captured by the connected work distributor.</param>
        public RelaxedWorker(IPendingWorkRepository pendingWorkRepository = null, params MessageType[] resultTypes)
        {
            if (pendingWorkRepository == null)
                _pendingWorkRepository = new MemoryPendingWorkRepository();
            _resultTypes = resultTypes;
        }

        public void AddResultType(MessageType type)
        {
            Entity.RegisterSendHandler(type, SendingResult);
        }
        
        public virtual IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo {Callback = WorkReceived, MessageType = new MessageType("WORK")}
            };
        }

        public IEnumerable<SendHandleInfo> GetSendMiddlewares()
        {
            return _resultTypes.Select(x => new SendHandleInfo {Callback = SendingResult, MessageType = x});
        }

        //TODO: how about queuing the process of works by the application and sending a certain amount of works at a time to application layer??
        /// <summary>
        /// This method is the entry point for the received works. It will unwrap the message and adds it to the pending
        /// work repository that has been set previously. Then it will pass the work to the MQEntity so that its process
        /// can be done by the application.
        /// </summary>
        protected virtual Task WorkReceived(IMqEntity sender, Identity identity, MessageType messageType,
            NetMQMessage message)
        {
            var index = message.Pop().ConvertToInt64();
            var workType = new MessageType(message.Pop());
            message.Push(index);
            _pendingWorkRepository.AddPendingWork(message, index);
            sender.MessageReceived(identity, workType, message);
            return Task.CompletedTask;
        }

        /// <summary>
        /// This method will be called if any result message wants to be sent on the wire. It will manipulate the
        /// message so that the work distributor at the other end of the wire can capture it.
        /// </summary>
        protected virtual void SendingResult(IMqEntity sender, ref Identity identity, ref MessageType messageType,
            ref NetMQMessage message)
        {
            var index = message.Pop().ConvertToInt64();
            message.Push(messageType);
            message.Push(index);
            messageType = new MessageType("WORKRESULT");
            if (!_pendingWorkRepository.RemovePendingWork(index))
            {
                Entity.Logger.Warn($"Work result with index {index} was sent but it was not pending");
            }

            if (IsShuttingDown && _pendingWorkRepository.GetCount() == 0)
                _pendingWorksCompletion.SetResult(null);
        }

        public override object GetHealthInformation()
        {
            return new
            {
                pending_works = _pendingWorkRepository.GetCount()
            };
        }

        /// <summary>
        /// In case of stopping gracefully, this function will release its task only when all the pending works are done.
        /// </summary>
        public override Task StopGracefully()
        {
            base.StopGracefully();
            return _pendingWorkRepository.GetCount() == 0 ? Task.CompletedTask : _pendingWorksCompletion.Task;
        }
    }
}