﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using C5;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality extends the InstantWorkDistributor to answer READY packets from workers. It will only send
    /// works when a worker has been stated to be ready to do a job. A socket with this functionality will usually
    /// connect to a remote socket having the EagerWorker functionality. In this setup works will be accumulated in this
    /// class that is why it accepts a work repository to store the works in an arbitrary source.
    /// </summary>
    public class LazyWorkDistributor : InstantWorkDistributor
    {
        /// <summary>
        /// This is the default implementation of work repositories. Which will store the works in the memory.
        /// </summary>
        private class MemoryWorkRepository : IWorkRepository
        {
            private readonly IntervalHeap<Work> _works = new IntervalHeap<Work>();

            public void AddWork(Work work, int priority)
            {
                _works.Add(work);
            }

            public Work PopMin()
            {
                return _works.Any() ? _works.DeleteMin() : null;
            }

            public int GetCount()
            {
                return _works.Count;
            }
        }

        /// <summary>
        /// The repository which should save the works in a priority queue in an arbitrary data source. 
        /// </summary>
        private readonly IWorkRepository _workRepository;

        /// <summary>
        /// This task will be used for stopping gracefully and will be done when all the works that has been sent to
        /// a worker, has been done.
        /// </summary>
        private readonly TaskCompletionSource<object> _queueCompletion = new TaskCompletionSource<object>();

        /// <param name="workTimeout">The process timeout for each work to be processed by a worker.</param>
        /// <param name="maxWorkTries">The maximum number of tries for each work before it becomes failed.</param>
        /// <param name="workRepository">The repository which should save the works in a priority queue in an arbitrary
        /// data source.</param>
        public LazyWorkDistributor(TimeSpan? workTimeout = null, int maxWorkTries = 3,
            IWorkRepository workRepository = null) : base(workTimeout, maxWorkTries)
        {
            if (workRepository == null) workRepository = new MemoryWorkRepository();
            _workRepository = workRepository;
        }

        //TODO: enqueue should reject additional work if it is too busy and return if the work is queued
        /// <summary>
        /// This method should be called from the application layer to add a work to a priority queue that will be sent
        /// to the workers. In this overload, the message type of the message should be put in the first frame of the
        /// passed message.
        /// </summary>
        /// <param name="msg">The message object containing message type and the body of the work.</param>
        /// <param name="priority">The priority of this work. Works with lower value for their priority will be sent
        /// sooner than the ones with higher value as their priority.</param>
        /// <param name="queueAfter">This value can be set to have an arbitrary delay for before sending this works.</param>
        /// <exception cref="Exception">If the socket is in the gracefully shutdown state, this exception will be
        /// thrown.</exception>
        public void EnqueueWork(NetMQMessage msg, int priority = 0, TimeSpan? queueAfter = null)
        {
            if (IsShuttingDown)
                throw new Exception("cannot enqueue new work when shutting down");

            if (!queueAfter.HasValue)
                queueAfter = TimeSpan.Zero;

            WorkPassingQueue.Enqueue(new Work
                {Message = msg, Priority = priority, CreationTime = DateTime.Now + queueAfter.Value});
        }

        /// <summary>
        /// This method should be called from the application layer to add a work to a priority queue that will be sent
        /// to the workers. In this overload, the message type of the message can be set in a different parameter.
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="msg">The message object containing message type and the body of the work.</param>
        /// <param name="priority">The priority of this work. Works with lower value for their priority will be sent
        /// sooner than the ones with higher value as their priority.</param>
        /// <param name="queueAfter">This value can be set to have an arbitrary delay for before sending this works.</param>
        public void EnqueueWork(MessageType messageType, NetMQMessage msg, int priority = 0,
            TimeSpan? queueAfter = null)
        {
            msg.Push(messageType);
            EnqueueWork(msg, priority, queueAfter);
        }
        
        /// <summary>
        /// This method be called when there is a new work in WorkPassingQueue which is waiting to be processed. It will
        /// enqueue this work in the work repository so that it can be sent to the workers when they are ready to do
        /// the job.
        /// </summary>
        protected override void WorkReady(object sender, NetMQQueueEventArgs<Work> e)
        {
            while (WorkPassingQueue.Any())
            {
                var work = WorkPassingQueue.Dequeue();
                EnqueueWork(work);
            }
        }

        protected void EnqueueWork(Work work)
        {
            _workRepository.AddWork(work, work.Priority);
        }

        public override IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return base.GetReceiveMiddlewares()
                .Append(new ReceiveHandleInfo {Callback = WorkerReady, MessageType = new MessageType("READY")});
        }

        /// <summary>
        /// This method will be called when a READY packet has been sent from a worker. This will simply pop the next
        /// worker that should be done (judged by the work repository), and will send it on the wire.
        /// </summary>
        private Task WorkerReady(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            var requeueList = new List<Work>();
            Task re = null;

            do
            {
                var work = _workRepository.PopMin();
                if (work != null)
                {
                    if (work.CreationTime <= DateTime.Now)
                        re = SendWork(work, identity);
                    else
                        requeueList.Add(work);
                }
                else
                {
                    sender.SendMessage(identity, new MessageType("NOWORK"));
                    re = Task.CompletedTask;
                }
            } while (re == null);

            requeueList.ForEach(EnqueueWork);
            if (IsShuttingDown && _workRepository.GetCount() == 0)
                _queueCompletion.SetResult(null);

            return re;
        }

        protected override void RetryWork(long previousIndex, Work work)
        {
            EnqueueWork(work);
        }

        /// <summary>
        /// The SendWork methods has been deleted in the API of this class because this functionality should not send
        /// a work instantly to a worker.
        /// </summary>
        public new long SendWork(NetMQMessage msg, int priority = 0)
        {
            throw new NotImplementedException("This method is not supported in LazyWorkDistributor class");
        }

        /// <summary>
        /// The SendWork methods has been deleted in the API of this class because this functionality should not send
        /// a work instantly to a worker.
        /// </summary>
        [Obsolete("This method is not supported in LazyWorkDistributor class")]
        public new long SendWork(MessageType type, NetMQMessage msg, int priority = 0)
        {
            throw new NotImplementedException("This method is not supported in LazyWorkDistributor class");
        }

        public override object GetHealthInformation()
        {
            var baseInfo = base.GetHealthInformation();
            var myType = baseInfo.GetType();
            var props = new List<PropertyInfo>(myType.GetProperties());

            return new
            {
                pending_works = props.FirstOrDefault(x => x.Name == "pending_works")?.GetValue(baseInfo, null),
                works_in_passing_queue = props.FirstOrDefault(x => x.Name == "works_in_passing_queue")
                    ?.GetValue(baseInfo, null),
                last_work_index = props.FirstOrDefault(x => x.Name == "last_work_index")?.GetValue(baseInfo, null),
                works_in_queue = _workRepository.GetCount()
            };
        }

        /// <summary>
        /// Stopping gracefully will be done when all the works that has been sent to a worker, has been done.
        /// </summary>
        public override Task StopGracefully()
        {
            return Task.WhenAll(base.StopGracefully(), _queueCompletion.Task);
        }
    }
}