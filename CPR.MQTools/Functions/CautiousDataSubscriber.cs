﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;
using Newtonsoft.Json;
using Unosquare.Swan;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This class implements a functionality in which a socket will be the subscriber of a publisher but in a cautious
    /// way! It will receive the published data but at the same time, it keeps a track of their indices, if the indices
    /// are out-of-sync, it will try to fetch the missing data from the publisher.
    /// </summary>
    public class CautiousDataSubscriber : MqFunctionBase, IMqReceiveMiddleware
    {
        /// <summary>
        /// This class encapsulates information of a received data.
        /// </summary>
        private class PendingData
        {
            public long DataIndex;
            public long CreationTimestamp;
            public NetMQMessage Message;
        }

        /// <summary>
        /// This class encapsulates the data that currently is being processed by the application.
        /// </summary>
        private class ProcessingData
        {
            public Identity Sender;
            public PendingData Data;
        }

        /// <summary>
        /// The version controller object which will be used to save and retrieve  the latest version information from
        /// an arbitrary source.
        /// </summary>
        private readonly IDataVersionController _dataVersionController;

        /// <summary>
        /// This is a dictionary which caches some information that maps a data publisher to its latest data received
        /// by this subscriber.
        /// </summary>
        private readonly Dictionary<string, Dictionary<long, PendingData>> _pendingData;

        /// <summary>
        /// This dictionary maps each publisher Id to the expected next data information along with its latest data
        /// receive timestamp.
        /// </summary>
        private readonly Dictionary<string, DataInfo> _expectingDataInformation;

        /// <summary>
        /// This dictionary maps each publisher Id to its latest processed data by the application.
        /// </summary>
        private readonly ConcurrentDictionary<string, DataInfo> _lastProcessedDataInformation;

        /// <summary>
        /// This dictionary determines whether this subscriber is fetching missing data from a publisher or not. 
        /// </summary>
        private readonly Dictionary<string, bool> _isFetchingMissedData;

        private readonly Dictionary<string, DateTime> _lastFetchTriggerTime;

        /// <summary>
        /// This event will be invoked when this functionality is needed to store the latest data information for each
        /// publisher.
        /// </summary>
        private readonly ManualResetEventSlim _dataInfoStoreEvent;

        /// <summary>
        /// This event will be invoked when there may be some received data to be processed by the application.
        /// </summary>
        private readonly ManualResetEventSlim _startProcessingEvent;

        /// <summary>
        /// This dictionary maps each publisher Id to its processing queue which contains the received data that needs
        /// to be processed by the application.
        /// </summary>
        private readonly Dictionary<string, Queue<ProcessingData>> _processQueue;

        /// <summary>
        /// This parameter will be used to specify the time that is needed to be passed before retrying the process of
        /// a received data which previously has encountered an exception during its processing.
        /// </summary>
        private readonly TimeSpan _exceptionRetryTimeSpan;

        /// <summary>
        /// The maximum number of retries for processing a single data if it encountered an exception.
        /// </summary>
        private readonly int _exceptionMaxRetry;

        /// <summary>
        /// The timeout for not receiving fetch responses from publishers. If restoring
        /// data from publishers was timed-out, fetching missing data will be retried. The default value is one second.
        /// </summary>
        private readonly TimeSpan _fetchDataTimeout;

        /// <summary>
        /// This dictionary maps each publisher Id to the number of exceptions occured during processing the latest data. 
        /// </summary>
        private readonly Dictionary<string, int> _exceptionRetryCount;

        /// <summary>
        /// This is a condition variable which be done if we are during gracefully shutting down state and the process
        /// queue of all publishers are empty.
        /// </summary>
        private readonly TaskCompletionSource<object> _processQueueCompletion = new TaskCompletionSource<object>();

        /// <summary>
        /// This parameters determines if the gracefully shutdown depends on storing the latest data information or not.
        /// </summary>
        private bool _shuttingDownDependsOnStore;

        public delegate void MissedDataCallback(Identity publisher, long dataIndex);

        /// <summary>
        /// This event will be invoked when a data index is missed by this subscriber. That is when a MISS packet is
        /// received by a publisher.
        /// </summary>
        public event MissedDataCallback OnMissedData;

        public delegate Task ProcessFailureCallback(Identity publisher, long dataIndex, long creationTimestamp,
            NetMQMessage message);

        /// <summary>
        /// This event will be invoked when processing of a received data has passed the exceptionMaxRetry value. That
        /// is retrying exceptionMaxRetry times and each of them has thrown an exception. In this case, this event will
        /// be called and the processing of that data will be ignored.
        /// </summary>
        public event ProcessFailureCallback OnProcessFailure;

        /// <param name="dataVersionController">The version controller object which will be used to save and retrieve
        /// the latest version information from an arbitrary source.</param>
        /// <param name="exceptionRetryTimeSpan">This parameter will be used to specify the time that is needed to be
        /// passed before retrying the process of a received data which previously has encountered an exception during
        /// its processing.</param>
        /// <param name="exceptionMaxRetry">The maximum number of retries for processing a single data if it encountered
        /// an exception.</param>
        /// <param name="fetchDataTimeout">The timeout for not receiving fetch responses from publishers. If restoring
        /// data from publishers was timed-out, fetching missing data will be retried. The default value is one second.</param>
        public CautiousDataSubscriber(IDataVersionController dataVersionController,
            TimeSpan? exceptionRetryTimeSpan = null, int exceptionMaxRetry = -1, TimeSpan? fetchDataTimeout = null)
        {
            _exceptionRetryTimeSpan = exceptionRetryTimeSpan ?? TimeSpan.FromSeconds(1);
            _exceptionMaxRetry = exceptionMaxRetry;
            _fetchDataTimeout = fetchDataTimeout ?? TimeSpan.FromSeconds(1);
            _exceptionRetryCount = new Dictionary<string, int>();
            _dataVersionController = dataVersionController;
            _pendingData = new Dictionary<string, Dictionary<long, PendingData>>();
            _expectingDataInformation = new Dictionary<string, DataInfo>();
            _lastProcessedDataInformation = new ConcurrentDictionary<string, DataInfo>();
            _isFetchingMissedData = new Dictionary<string, bool>();
            _dataInfoStoreEvent = new ManualResetEventSlim(false);
            _startProcessingEvent = new ManualResetEventSlim(false);
            _processQueue = new Dictionary<string, Queue<ProcessingData>>();
            _lastFetchTriggerTime = new Dictionary<string, DateTime>();
            
            Task.Factory.StartNew(StoreDataInfoLoop).ConfigureAwait(false);
        }

        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.PollerTaskFactory.StartNew(ProcessQueue);
        }

        /// <summary>
        /// This is the main block of code which will process the elements of _processQueue and passes them to mqEntity
        /// to be processed by the application. In each iteration it will await on the invocation of process event,
        /// then will try to empty each publisher's process queue. If it is a missed data, it will invoke the
        /// OnMissedData event and if not, it will pass the data to the mqEntity to be processed by a registered handler.
        /// It everything has been done with no thrown exception, it will invoke _dataInfoStoreEvent so that the latest
        /// information would be stored.
        /// </summary>
        private async Task ProcessQueue()
        {
            await _startProcessingEvent.WaitHandle.AsTask();
            _startProcessingEvent.Reset();

            try
            {
                await Task.WhenAll(_processQueue.Select(async queueItem =>
                {
                    var currentData = queueItem.Value.FirstOrDefault();
                    while (currentData != null)
                    {
                        try
                        {
                            if (currentData.Data.Message.First().ConvertToString() == "MISS")
                                OnMissedData?.Invoke(currentData.Sender, currentData.Data.DataIndex);
                            else
                                await Entity.MessageReceived(currentData.Sender,
                                    new NetMQMessage(currentData.Data.Message));

                            _exceptionRetryCount[queueItem.Key] = 0;
                            _dataInfoStoreEvent.Set();

                            if (IsShuttingDown)
                                _shuttingDownDependsOnStore = true;

                            currentData = queueItem.Value.Dequeue();
                            _lastProcessedDataInformation[queueItem.Key] = new DataInfo
                            {
                                DataIndex = currentData.Data.DataIndex,
                                LastCreationTimestamp = currentData.Data.CreationTimestamp
                            };
                        }
                        catch (Exception e)
                        {
                            Entity.Logger.Error(e, "Processing data encountered error");
                            if (!_exceptionRetryCount.ContainsKey(queueItem.Key))
                                _exceptionRetryCount[queueItem.Key] = 0;

                            ++_exceptionRetryCount[queueItem.Key];
                            if (_exceptionMaxRetry == -1 || _exceptionRetryCount[queueItem.Key] < _exceptionMaxRetry)
                            {
                                await Task.Delay(_exceptionRetryTimeSpan);
                                _startProcessingEvent.Set();
                            }
                            else
                            {
                                var data = queueItem.Value.Dequeue();
                                OnProcessFailure?.Invoke(data.Sender, data.Data.DataIndex, data.Data.CreationTimestamp,
                                    data.Data.Message);
                            }
                        }

                        currentData = queueItem.Value.FirstOrDefault();
                    }
                }));

                if (IsShuttingDown && !_shuttingDownDependsOnStore)
                    _processQueueCompletion.SetResult(null);
            }
            finally
            {
                Entity.PollerTaskFactory.StartNew(ProcessQueue).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// This method is the main loop for storing the latest data information. It will await on the store event and
        /// then stores every publisher's data information using _dataVersionController.
        /// </summary>
        private async Task StoreDataInfoLoop()
        {
            await _dataInfoStoreEvent.WaitHandle.AsTask();
            _dataInfoStoreEvent.Reset();

            try
            {
                await Task.WhenAll(_lastProcessedDataInformation.Select(entry => _dataVersionController.StoreDataInfo(
                    entry.Key, new DataInfo
                    {
                        DataIndex = entry.Value.DataIndex, LastCreationTimestamp = entry.Value.LastCreationTimestamp
                    })));
            }
            catch (Exception e)
            {
                Entity.Logger.Error(e, "Error in Storing data info");
            }
            finally
            {
                if (IsShuttingDown && _shuttingDownDependsOnStore)
                    _processQueueCompletion.SetResult(null);
                Task.Factory.StartNew(StoreDataInfoLoop).ConfigureAwait(false);
            }
        }

        public IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return new[]
            {
                new ReceiveHandleInfo {Callback = DataReceived, MessageType = new MessageType("DATA")}
            };
        }

        /// <summary>
        /// This method is the entry point for a received data. It will check the meta data frames of the received
        /// message and check it to be synced with the previously received data. If it is out of sync, it will try to
        /// fetch the missing data from the target publisher. If the data is synced, it will be put on the process queue
        /// to be processed by the application.
        /// </summary>
        private Task DataReceived(IMqEntity sender, Identity identity, MessageType messagetype, NetMQMessage message)
        {
            if (IsShuttingDown)
                return Task.CompletedTask;

            var publisherId = message.Pop().ConvertToString();
            var dataIndex = message.Pop().ConvertToInt64();
            var creationTimestamp = message.Pop().ConvertToInt64();

            if (!_expectingDataInformation.TryGetValue(publisherId, out var expectingInfo))
            {
                expectingInfo = _dataVersionController.GetLastDataInfo(publisherId) ??
                                new DataInfo {DataIndex = -1, LastCreationTimestamp = 0};
                ++expectingInfo.DataIndex;
                _expectingDataInformation[publisherId] = expectingInfo;
            }

            if (!_pendingData.TryGetValue(publisherId, out var pendings))
            {
                pendings = new Dictionary<long, PendingData>();
                _pendingData[publisherId] = pendings;
            }

            if (CheckResetIndex(dataIndex, creationTimestamp, ref expectingInfo))
                _expectingDataInformation[publisherId] = expectingInfo;

            if (dataIndex == expectingInfo.DataIndex)
            {
                do
                {
                    if (!_processQueue.ContainsKey(publisherId))
                        _processQueue[publisherId] = new Queue<ProcessingData>();

                    _processQueue[publisherId].Enqueue(new ProcessingData
                    {
                        Sender = identity,
                        Data = new PendingData
                        {
                            Message = message,
                            DataIndex = dataIndex,
                            CreationTimestamp = creationTimestamp
                        }
                    });

                    ++dataIndex;
                    if (!pendings.TryGetValue(dataIndex, out var nextData))
                    {
                        var followingData = pendings.Values.OrderBy(p => p.DataIndex)
                            .FirstOrDefault(p => p.DataIndex > dataIndex);

                        if (followingData != null)
                            FetchData(identity, dataIndex, followingData.DataIndex - 1);

                        break;
                    }

                    message = nextData.Message;
                    creationTimestamp = nextData.CreationTimestamp;
                    pendings.Remove(dataIndex);
                } while (message != null);

                _startProcessingEvent.Set();
                if (!pendings.Any())
                {
                    _isFetchingMissedData[publisherId] = false;
                }

                _expectingDataInformation[publisherId] =
                    new DataInfo {DataIndex = dataIndex, LastCreationTimestamp = creationTimestamp};
            }
            else if (dataIndex > expectingInfo.DataIndex)
            {
                if (!(_isFetchingMissedData.TryGetValue(publisherId, out var value) && value))
                {
                    _isFetchingMissedData[publisherId] = true;
                    _lastFetchTriggerTime[publisherId] = DateTime.Now;

                    Entity.PollerTaskFactory.StartNew(async () =>
                    {
                        await Task.Delay(_fetchDataTimeout.Add(TimeSpan.FromMilliseconds(100)));
                        if (_lastFetchTriggerTime.TryGetValue(publisherId, out var lastTriggerTime) &&
                            lastTriggerTime.Add(_fetchDataTimeout) < DateTime.Now)
                            _isFetchingMissedData[publisherId] = false;
                    });

                    var followingData = pendings.Values.OrderBy(p => p.DataIndex)
                        .FirstOrDefault(p => p.DataIndex < dataIndex);
                    var toIndex = Math.Min(followingData?.DataIndex - 1 ?? long.MaxValue, dataIndex - 1);
                    Entity.Logger.Warn(
                        $"Fetching missing data from {expectingInfo.DataIndex} to {toIndex}. expectingInfo={JsonConvert.SerializeObject(expectingInfo)}");
                    FetchData(identity, expectingInfo.DataIndex, toIndex);
                }

                pendings[dataIndex] = new PendingData
                {
                    DataIndex = dataIndex,
                    Message = message,
                    CreationTimestamp = creationTimestamp
                };
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// This method will check if the data index from a publisher has been reset. If the receiving data index is
        /// prior to the expecting data index but its timestamp is further, it will be detected to be reset. 
        /// </summary>
        /// <param name="dataIndex">The received data index.</param>
        /// <param name="creationTimestamp">The received timestamp of the data.</param>
        /// <param name="expectingInfo">The expecting metadata information for this data.</param>
        /// <returns>Whether or not a data index reset was detected.</returns>
        private bool CheckResetIndex(long dataIndex, long creationTimestamp, ref DataInfo expectingInfo)
        {
            if (!(creationTimestamp > 0 && expectingInfo.LastCreationTimestamp > 0 &&
                  creationTimestamp > expectingInfo.LastCreationTimestamp && dataIndex < expectingInfo.DataIndex))
                return false;

            Entity.Logger.Warn(
                $"Data index reset detected. dataIndex={dataIndex}, creationTimestamp={creationTimestamp}, expectingInfo={JsonConvert.SerializeObject(expectingInfo)}");
            expectingInfo = new DataInfo
            {
                DataIndex = 0,
                LastCreationTimestamp = creationTimestamp
            };
            return true;
        }

        /// <summary>
        /// This method will send a fetch request to a publisher to restore a range of missing data. 
        /// </summary>
        /// <param name="identity">The identity of publisher.</param>
        /// <param name="fromIndex">Starting index to be fetched.</param>
        /// <param name="toIndex">The finishing index to be fetched.</param>
        private void FetchData(Identity identity, long fromIndex, long toIndex)
        {
            var message = new NetMQMessage();
            message.Append(fromIndex);
            message.Append(toIndex);
            Entity.SendMessage(identity, new MessageType("FETCH"), message);
        }

        public override object GetHealthInformation()
        {
            dynamic expando = new ExpandoObject();

            var index = 0;
            _lastProcessedDataInformation.ForEach((key, value) =>
            {
                var identity = new MqFrame(key);
                AddProperty(expando, $"processed_data_{index}_id", identity.ConvertToBase64());
                AddProperty(expando, $"processed_data_{index}_last_index", value.DataIndex);
                ++index;
            });

            index = 0;
            _expectingDataInformation.ForEach((key, value) =>
            {
                var identity = new MqFrame(key);
                AddProperty(expando, $"expecting_data_{index}_id", identity.ConvertToBase64());
                AddProperty(expando, $"expecting_data_{index}_last_index", value.DataIndex);
                AddProperty(expando, $"expecting_data_{index}_fetching_missed_data",
                    _isFetchingMissedData.TryGetValue(key, out var isFetching) && isFetching);
                ++index;
            });

            index = 0;
            _pendingData.ForEach((key, value) =>
            {
                var identity = new MqFrame(key);
                AddProperty(expando, $"pending_data_{index}_publisher", identity.ConvertToBase64());
                AddProperty(expando, $"pending_data_{index}_count", value.Count);
                ++index;
            });

            index = 0;
            _exceptionRetryCount.ForEach((key, value) =>
            {
                var identity = new MqFrame(key);
                AddProperty(expando, $"exceptions_{index}_publisher", identity.ConvertToBase64());
                AddProperty(expando, $"exceptions_{index}_count", value);
                ++index;
            });

            return expando;
        }

        private static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            var expandoDict = expando as IDictionary<string, object>;

            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        /// <summary>
        /// This method will pass a task which will be complete if the processing queue is empty and the store
        /// information is done.
        /// </summary>
        /// <returns></returns>
        public override Task StopGracefully()
        {
            base.StopGracefully();
            _startProcessingEvent.Set();
            return _processQueueCompletion.Task;
        }
    }
}