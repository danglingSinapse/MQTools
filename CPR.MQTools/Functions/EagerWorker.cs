﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPR.MQTools.Interfaces;
using NetMQ;

namespace CPR.MQTools.Functions
{
    /// <summary>
    /// This functionality will add the capability of receiving and tracking works that the target socket is set to do.
    /// Instead of waiting for works to be received to this socket, it will extend the RelaxedWorker functionality and
    /// volunteer for doing the works. The socket that has this functionality will usually connect to a socket having
    /// LazyWorkDistributor functionality. This functionality will send READY packets to the work distributor and
    /// expects to receive new works after it. This way, the works will be queued in the LazyWorkDistributor
    /// functionality.
    /// </summary>
    public class EagerWorker : RelaxedWorker
    {
        /// <summary>
        /// This timer will be used to send READY packets if necessary on some intervals.
        /// </summary>
        private readonly NetMQTimer _timer;
        
        /// <summary>
        /// This is the time interval between each checking for sending READY packets.
        /// </summary>
        private readonly TimeSpan _readyInterval;
        
        /// <summary>
        /// This is the max number of works that this socket should be doing at the same time. This will be used to
        /// determine how many READY packets should be sent to the work distributor.
        /// </summary>
        private readonly int _maxConcurrentWorks;
        
        /// <summary>
        /// This is the current works that is being processed by the application. 
        /// </summary>
        private int _currentWorks;
        
        /// <summary>
        /// This task source will be used for gracefully stopping. This will be done if the current processing of
        /// receiving works has been completed.
        /// </summary>
        private readonly TaskCompletionSource<object> _currentWorksCompletion = new TaskCompletionSource<object>();

        public EagerWorker(params MessageType[] resultTypes) : this(null, 10, resultTypes)
        {
        }
        
        /// <param name="readyInterval">The time interval between each checking for sending READY packets.</param>
        /// <param name="maxConcurrentWorks">The max number of works that this socket should be doing at the same time.
        /// This will be used to determine how many READY packets should be sent to the work distributor.</param>
        /// <param name="resultTypes">This is the type of the result of the works which their sending will be
        /// manipulated by this functionality so that they can be captured by the connected work distributor.</param>
        public EagerWorker(TimeSpan? readyInterval = null, int maxConcurrentWorks = 10,
            params MessageType[] resultTypes) : base(resultTypes: resultTypes)
        {
            _readyInterval = readyInterval ?? TimeSpan.FromSeconds(1);
            _timer = new NetMQTimer(_readyInterval) {Enable = true};
            _timer.Elapsed += (sender, args) => Ready();
            _maxConcurrentWorks = maxConcurrentWorks;
        }
        
        public override void SetEntity(IMqEntity entity)
        {
            base.SetEntity(entity);
            entity.Poller.Add(_timer);
        }

        public override IEnumerable<ReceiveHandleInfo> GetReceiveMiddlewares()
        {
            return base.GetReceiveMiddlewares()
                .Append(new ReceiveHandleInfo {Callback = NoWorkReceived, MessageType = new MessageType("NOWORK")});
        }

        /// <summary>
        /// This method will be called once every ready interval. It may send some some READY packets depending on the
        /// amount of works that is currently being processed by the application and the max number of concurrent works.
        /// </summary>
        private void Ready()
        {
            for (var i = _currentWorks + 1; i <= _maxConcurrentWorks; ++i)
                Entity.SendMessage(new MessageType("READY")).ConfigureAwait(false);
        }

        private Task NoWorkReceived(IMqEntity sender, Identity identity, MessageType messageType, NetMQMessage message)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// This method is the entry point for the received works. It will just increment the number of works that is
        /// being processed and pass it to be unwrapped by the base class. 
        /// </summary>
        protected override Task WorkReceived(IMqEntity sender, Identity identity, MessageType messageType,
            NetMQMessage message)
        {
            if (++_currentWorks >= _maxConcurrentWorks)
                _timer.Enable = false;
            return base.WorkReceived(sender, identity, messageType, message);
        }

        /// <summary>
        /// This method will be called if any result message wants to be sent on the wire. It will pass the control to
        /// the base class to manipulate the message so that the work distributor at the other end of the wire can
        /// capture it. Then it will decrement the number of works that is being processed and will immediately call
        /// Ready method to send ready packets if necessary. 
        /// </summary>
        protected override void SendingResult(IMqEntity sender, ref Identity identity, ref MessageType messageType,
            ref NetMQMessage message)
        {
            base.SendingResult(sender, ref identity, ref messageType, ref message);
            --_currentWorks;
            if (IsShuttingDown && _currentWorks == 0)
                _currentWorksCompletion.SetResult(null);
            if (_currentWorks >= _maxConcurrentWorks) return;
            Ready();
            _timer.Enable = true;
        }

        public override object GetHealthInformation()
        {
            return new
            {
                current_works = _currentWorks
            };
        }

        /// <summary>
        /// Stopping gracefully will be done if the current processing of receiving works has been completed.
        /// </summary>
        public override Task StopGracefully()
        {
            _timer.Enable = false;
            return Task.WhenAll(base.StopGracefully(), _currentWorksCompletion.Task);
        }
    }
}