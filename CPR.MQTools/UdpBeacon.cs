﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NetMQ;

namespace CPR.MQTools
{
    public class UdpBeacon : IDisposable
    {
        [DllImport("libc", SetLastError = true)]
        private static extern unsafe int setsockopt(int socket, int level, int option_name, void* option_value,
            uint option_len);

        private byte[] _data;
        private IPEndPoint _broadcastEndPoint;
        private UdpClient _udpClient;
        private NetMQTimer _timer;
        private bool _isDisposed;
        private TaskFactory _taskFactory;
        private string _receiveStartsWithFilter;
        public event EventHandler<string> OnReceive;

        public UdpBeacon(int port)
        {
            _broadcastEndPoint = new IPEndPoint(IPAddress.Broadcast, port);
            _udpClient = new UdpClient
            {
                EnableBroadcast = true
            };

            _udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _udpClient.Client.ExclusiveAddressUse = false;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                var value = 1;
                unsafe
                {
                    setsockopt(_udpClient.Client.Handle.ToInt32(), 1, 2, &value, sizeof(int));
                }
            }
        }

        public void Publish(string data, NetMQPoller poller, TimeSpan broadcastInterval, string networkInterfaceName)
        {
            if (_isDisposed)
                throw new Exception("Object disposed.");

            _data = Encoding.ASCII.GetBytes(data);

            if (!string.IsNullOrWhiteSpace(networkInterfaceName))
                SetSocketInterface(_udpClient, networkInterfaceName);

            _timer = new NetMQTimer(broadcastInterval);
            _timer.Elapsed += TimerOnElapsed;
            poller.Add(_timer);
        }

        private static void SetSocketInterface(UdpClient udpClient, string networkInterfaceName)
        {
            var networkInterface = NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(q => string.Equals(q.Name, networkInterfaceName, StringComparison.OrdinalIgnoreCase));

            if (networkInterface == null)
                throw new ArgumentException($"Specified network interface ({networkInterfaceName}) not found.");

            if (networkInterface.OperationalStatus != OperationalStatus.Up)
                throw new ArgumentException($"Specified network interface must be in UP operation state");

            if (networkInterface.SupportsMulticast)
                throw new ArgumentException($"Specified network interface doesn't support multicast");

            var iPv4Properties = networkInterface.GetIPProperties().GetIPv4Properties();
            if (iPv4Properties == null)
                throw new ArgumentException("IPV4 is not configured on the specified network interface",
                    nameof(networkInterfaceName));

            udpClient.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastInterface,
                IPAddress.HostToNetworkOrder(iPv4Properties.Index));
        }

        public void StopPublish()
        {
            _timer.Enable = false;
        }

        public void Subscribe(string filter, TaskFactory taskFactory)
        {
            if (_isDisposed)
                throw new Exception("Object disposed.");

            _receiveStartsWithFilter = filter;
            _taskFactory = taskFactory;


            _udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, _broadcastEndPoint.Port));
            _udpClient.BeginReceive(UdpReceiveCallback, null);
        }

        private void UdpReceiveCallback(IAsyncResult ar)
        {
            try
            {
                var receivedBytes = _udpClient.EndReceive(ar, ref _broadcastEndPoint);
                var receivedStr = Encoding.ASCII.GetString(receivedBytes);
                if (receivedStr.StartsWith(_receiveStartsWithFilter))
                    _taskFactory.StartNew(() => OnReceive?.Invoke(this, receivedStr));

                _udpClient.BeginReceive(UdpReceiveCallback, null);
            }
            catch (ObjectDisposedException)
            {
                // This happens when we want to close the UDP Client
            }
        }

        private void TimerOnElapsed(object sender, NetMQTimerEventArgs e)
        {
            _udpClient.Send(_data, _data.Length, _broadcastEndPoint);
        }

        public void Dispose()
        {
            _timer.Enable = false;
            _udpClient.Close();
            _udpClient?.Dispose();
            _udpClient = null;
            _isDisposed = true;
        }
    }
}