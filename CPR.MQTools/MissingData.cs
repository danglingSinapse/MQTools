using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class represents the information about a published data by the class CautiousDataPublisher. It also provides
    /// automatic serialization and deserialization from a byte array.
    /// </summary>
    [Serializable]
    public class MissingData : SerializableObject<MissingData>, ISerializable
    {
        /// <summary>
        /// The actual message that should be published on the wire.
        /// </summary>
        public NetMQMessage Message;
        
        /// <summary>
        /// The data index of this data.
        /// </summary>
        public long DataIndex;
        
        /// <summary>
        /// The timestamp of when this data was published.
        /// </summary>
        public long CreationTimestamp;
        
        /// <summary>
        /// The MessageType of this message 
        /// </summary>
        public MessageType MessageType;

        public MissingData()
        {
        }

        /// <summary>
        /// This constructor will deep copy an object of this class.  
        /// </summary>
        /// <param name="other">The object that should be copied</param>
        public MissingData(MissingData other)
        {
            Message = new NetMQMessage(other.Message);
            DataIndex = other.DataIndex;
            CreationTimestamp = other.CreationTimestamp;
            MessageType = new MessageType(other.MessageType);
        }

        /// <summary>
        /// This constructor will deserialize an object of this class from a previously serialized one.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"></see>
        /// to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"></see>)
        /// for this serialization.</param>
        private MissingData(SerializationInfo info, StreamingContext context)
        {
            foreach (var entry in info)
            {
                switch (entry.Name)
                {
                    case "m":
                        Message = ((byte[][]) entry.Value).ToNetMqMessage();
                        break;
                    case "i":
                        DataIndex = (long)entry.Value;
                        break;
                    case "c":
                        CreationTimestamp = (long) entry.Value;
                        break;
                    case "t":
                        MessageType = new MessageType((byte[])entry.Value);
                        break;
                }
            }
        }

        /// <summary>
        /// This method will serialize the current object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"></see>
        /// to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"></see>)
        /// for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("m", Message.ToByteArray());
            info.AddValue("i", DataIndex);
            info.AddValue("c", CreationTimestamp);
            info.AddValue("t", MessageType.ToByteArray());
        }
    }
}