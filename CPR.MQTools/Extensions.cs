using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class provides some helper extensions for classes used in this library.
    /// </summary>
    public static class Extensions
    {
        public static byte[][] ToByteArray(this NetMQMessage message)
        {
            return message.Select(x => x.ToByteArray()).ToArray();
        }

        public static NetMQMessage ToNetMqMessage(this byte[][] bytes)
        {
            return new NetMQMessage(bytes);
        }
        
        public static Task AsTask(this WaitHandle handle)
        {
            return AsTask(handle, Timeout.InfiniteTimeSpan);
        }

        public static Task AsTask(this WaitHandle handle, TimeSpan timeout)
        {
            var tcs = new TaskCompletionSource<object>();
            var registration = ThreadPool.RegisterWaitForSingleObject(handle, (state, timedOut) =>
            {
                var localTcs = (TaskCompletionSource<object>)state;
                if (timedOut)
                    localTcs.TrySetCanceled();
                else
                    localTcs.TrySetResult(null);
            }, tcs, timeout, executeOnlyOnce: true);
            tcs.Task.ContinueWith((_, state) => ((RegisteredWaitHandle)state).Unregister(null), registration, TaskScheduler.Default);
            return tcs.Task;
        }
    }
}