using System;
using System.Runtime.Serialization;

namespace CPR.MQTools
{
    /// <summary>
    /// This class represents the data information for CautiousDataPublisher and CautiousDataSubscriber class.
    /// It also provides automatic serialization and deserialization from a byte array.
    /// </summary>
    [Serializable]
    public class DataInfo : SerializableObject<DataInfo>, ISerializable
    {
        public long DataIndex;
        public long LastCreationTimestamp;

        public DataInfo()
        {
        }

        private DataInfo(SerializationInfo info, StreamingContext context)
        {
            DataIndex = (long) info.GetValue("i", typeof(long));
            LastCreationTimestamp = (long) info.GetValue("c", typeof(long));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("i", DataIndex);
            info.AddValue("c", LastCreationTimestamp);
        }
    }
}