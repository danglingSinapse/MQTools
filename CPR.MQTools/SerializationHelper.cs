﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CPR.MQTools
{
    //TODO: write tests for serialize/deserialize backward compatibility with different fields and namespaces
    /// <summary>
    /// This class is implemented to hide the background commands for serialization and deserialization of an object.
    /// </summary>
    public static class SerializationHelper
    {
        public static byte[] ObjectToByteArray<T>(T obj)
        {
            var bf = new BinaryFormatter
            {
                Binder = new CustomBinder<T>()
            };

            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static T ByteArrayToObject<T>(byte[] arrBytes) where T : class
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter
                {
                    Binder = new CustomBinder<T>()
                };

                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return (T) obj;
            }
        }

        class CustomBinder<T> : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                return typeof(T);
            }
        }
    }
}