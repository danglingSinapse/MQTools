﻿using System;
using System.Text;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class is created to separate specific kinds of netmq frames which will be used to identify endpoints.
    /// </summary>
    public class Identity : MqFrame
    {
        public Identity(byte[] buffer) : base(buffer)
        {
        }

        public Identity(string message) : base(message)
        {
        }

        public Identity(string message, Encoding encoding) : base(message, encoding)
        {
        }

        public Identity(int value) : base(value)
        {
        }

        public Identity(long value) : base(value)
        {
        }

        public Identity(byte value) : base(value)
        {
        }
        
        public Identity(NetMQFrame frame) : base(frame)
        {
        }

        public Identity(Enum enumValue) : base(enumValue)
        {
        }
    }
}