using System.Diagnostics;

namespace CPR.MQTools
{
    /// <summary>
    /// This class uses a SimpleAverage instance to wrap its usage for performance sampling by a simpler API. 
    /// </summary>
    public class PerformanceSampler
    {
        private readonly SimpleMovingAverage _movingAverage;
        private readonly object _performanceLock = new object();

        public PerformanceSampler(int maxSamplesCount)
        {
            _movingAverage = new SimpleMovingAverage(maxSamplesCount);
        }

        public Stopwatch StartSample()
        {
            var watch = new Stopwatch();
            watch.Start();
            return watch;
        }

        public void StopSample(Stopwatch watch)
        {
            lock (_performanceLock)
            {
                _movingAverage.AddSample(watch.ElapsedMilliseconds);
            }
        }

        public void PauseSample(Stopwatch watch)
        {
            watch.Stop();
        }

        public void ResumeSample(Stopwatch watch)
        {
            watch.Start();
        }

        public float GetAverage()
        {
            lock (_performanceLock)
            {
                return _movingAverage.Average;
            }
        }
    }
}