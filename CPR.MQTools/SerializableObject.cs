﻿namespace CPR.MQTools
{
    /// <summary>
    /// This class makes it easier for an ISerializable class to do serialization and deserialization.
    /// </summary>
    public class SerializableObject<TClass> where TClass : class
    {
        public byte[] ToByteArray()
        {
            return SerializationHelper.ObjectToByteArray(this);
        }

        public static TClass FromByteArray(byte[] bytes)
        {
            return SerializationHelper.ByteArrayToObject<TClass>(bytes);
        }
    }
}