using System;
using System.Collections.Generic;
using System.Linq;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class counts the occurence of some samples and reports their count in the specified time window. It can be
    /// used to measure rating metrics to be monitored.
    /// </summary>
    public class RateSampler
    {
        private readonly NetMQTimer _cleanupTimer = new NetMQTimer(1000);

        public void SetPoller(NetMQPoller poller = null)
        {
        }

        private readonly object _sampleLock = new object();
        private SortedList<DateTime, DateTime> _samples = new SortedList<DateTime, DateTime>();

        public RateSampler(TimeSpan? validTime, NetMQPoller poller = null)
        {
            if (poller == null)
            {
                poller = new NetMQPoller();
                poller.RunAsync();
            }
            
            poller.Add(_cleanupTimer);

            validTime = validTime ?? TimeSpan.FromSeconds(5);
            
            _cleanupTimer.Enable = true;
            _cleanupTimer.Elapsed += (sender, args) =>
            {
                var now = DateTime.Now;
                lock (_sampleLock)
                {
                    _samples = new SortedList<DateTime, DateTime>(_samples.Where(x => now.Add(-validTime.Value) < x.Key)
                        .ToDictionary(x => x.Key, x => x.Value));
                }
            };
        }

        public void AddSample()
        {
            var now = DateTime.Now;
            lock (_sampleLock)
            {
                _samples.Add(now, now);
            }
        }

        public int GetCount()
        {
            var now = DateTime.Now;
            lock (_sampleLock)
            {
                return _samples.Count(x => now.AddSeconds(-5) < x.Key);
            }
        }
    }
}