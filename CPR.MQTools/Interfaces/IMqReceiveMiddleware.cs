﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NetMQ;

namespace CPR.MQTools.Interfaces
{
    public delegate Task ReceiveMiddleware(IMqEntity sender, Identity identity, MessageType messageType,
        NetMQMessage message);

    /// <summary>
    /// This class wraps the information of a receive middleware.
    /// </summary>
    public class ReceiveHandleInfo
    {
        /// <summary>
        /// The MessageType of the messages it wants to handle.
        /// </summary>
        public MessageType MessageType;
        
        /// <summary>
        /// The actual callback which should be called when a message of the set type is received.
        /// </summary>
        public ReceiveMiddleware Callback;
        
        /// <summary>
        /// This parameter controls the running of the callback. If it is set to true,
        /// it will run on this object's poller. If it is false, it will be run on .net's default task scheduler.
        /// </summary>
        public bool IsPlugin = true;
    }

    /// <summary>
    /// This an abstraction of a type of functionality in which it wants to manipulate certain types of receiving
    /// messages. 
    /// </summary>
    public interface IMqReceiveMiddleware
    {
        /// <summary>
        /// This method should be called by the MqEntity when it needs to retrieve the receive middlewares.
        /// </summary>
        /// <returns>A collection containing the information of receive middlewares of this functionality.</returns>
        IEnumerable<ReceiveHandleInfo>  GetReceiveMiddlewares();
    }
}