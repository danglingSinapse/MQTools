﻿using System.Threading.Tasks;

namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This is an abstraction of storing and retrieving data version which is information about the latest published
    /// data. This will be used in both CautiousDataPublisher and CautiousDataSubscriber so that these functionalities
    /// wont lose track of data versions even when they are restarted.
    /// </summary>
    public interface IDataVersionController
    {
        /// <summary>
        /// This method should be called when the managing functionality requires the latest information of the data
        /// version. It will be called on startup of the application.
        /// </summary>
        /// <param name="publisherId">A unique ID corresponding to the requiring publisher entity.</param>
        /// <returns>The latest data information of this publisher.</returns>
        DataInfo GetLastDataInfo(string publisherId);
        
        /// <summary>
        /// This method should be called when the managing functionality requires to replace the latest information of
        /// data which has been published.
        /// </summary>
        /// <param name="publisherId">A unique ID corresponding to the requiring publisher entity.</param>
        /// <param name="info">The latest data information of this publisher</param>
        Task StoreDataInfo(string publisherId, DataInfo info);
    }
}