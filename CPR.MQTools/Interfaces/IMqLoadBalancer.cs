﻿namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This an abstraction of a type of functionality in which it wants to balance the load for connected endpoints.
    /// MqEntity will use this interface in situations where it want to send a message on the wire but the target
    /// endpoint is not specified. It can be used to implement arbitrary strategies for balancing the load e.g. round robin.
    /// </summary>
    public interface IMqLoadBalancer
    {
        /// <summary>
        /// This method will be called by MqEntity when it needs the next node to send a message to.
        /// </summary>
        /// <returns>The identity of the next node which the current message should be sent to.</returns>
        Identity GetNextNode();
    }
}