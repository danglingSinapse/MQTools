﻿using System.Collections.Generic;
using NetMQ;

namespace CPR.MQTools.Interfaces
{
    public delegate void SendMiddleware(IMqEntity sender, ref Identity identity, ref MessageType messageType,
        ref NetMQMessage message);

    /// <summary>
    /// This class wraps the information of a send middleware.
    /// </summary>
    public struct SendHandleInfo
    {
        /// <summary>
        /// The MessageType of the messages it wants to manipulate.
        /// </summary>
        public NetMQFrame MessageType;
        
        /// <summary>
        /// The actual callback which should be called when a message of the set type is going to be sent.
        /// </summary>
        public SendMiddleware Callback;
    }

    /// <summary>
    /// This an abstraction of a type of functionality in which it wants to manipulate certain types of sending
    /// messages.
    /// </summary>
    public interface IMqSendMiddleware
    {
        /// <summary>
        /// This method should be called by the MqEntity when it needs to retrieve the send middlewares.
        /// </summary>
        /// <returns>A collection containing the information of send middlewares of this functionality.</returns>
        IEnumerable<SendHandleInfo> GetSendMiddlewares();
    }
}