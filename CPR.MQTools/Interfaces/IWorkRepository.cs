using Work = CPR.MQTools.Functions.InstantWorkDistributor.Work;

namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This is an abstraction of work repository for LazyWorkDistributor class. The implementations are expected to
    /// behave as a priority queue.
    /// </summary>
    public interface IWorkRepository
    {
        /// <summary>
        /// This method should add the passed work with the passed priority to the repository. 
        /// </summary>
        /// <param name="work">The work object to be saved.</param>
        /// <param name="priority">The priority of this work.</param>
        void AddWork(Work work, int priority);
        
        /// <summary>
        /// This method should scan the priority queue and pop the next work to be processed.
        /// </summary>
        /// <returns>The next work in the internal priority queue.</returns>
        Work PopMin();
        
        /// <summary>
        /// This method should return the current number of works in the repository.
        /// </summary>
        /// <returns>The number of works in the repository.</returns>
        int GetCount();
    }
}