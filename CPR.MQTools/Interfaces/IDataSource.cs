﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This is an abstraction of storing and restoring missing data object. It will be used is CautiousDataPublisher
    /// class to represent an arbitrary repository of data objects.
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// This should be called when a given data is needed to be stored in the repository.
        /// </summary>
        /// <param name="publisherId">A unique ID corresponding to the publisher entity.</param>
        /// <param name="index">The index of the which is going to be stored.</param>
        /// <param name="data">The actual data object which should be stored.</param>
        /// <returns></returns>
        Task Store(string publisherId, long index, MissingData data);

        /// <summary>
        /// This method should be called when there is a need to restore a previously stored data. This should be called
        /// when the manager functionality is out of sync with a remote functionality.
        /// </summary>
        /// <param name="publisherId">A unique ID corresponding to the publisher entity.</param>
        /// <param name="index">The required data index which should be restored.</param>
        /// <returns>The restored data object. It should return null if this data cannot be restored.</returns>
        Task<MissingData> Restore(string publisherId, long index);

        /// <summary>
        /// This method is the bulk version of restore method. It will be called when there is a need to restore a group
        /// of previously stored data. This should be called when the manager functionality is out of sync with a remote
        /// functionality. It should return a container of missing data including the indices from fromIndex to
        /// index toIndex (including fromIndex and toIndex).
        /// </summary>
        /// <param name="publisherId">A unique ID corresponding to the publisher entity.</param>
        /// <param name="fromIndex">The start of restoring indices.</param>
        /// <param name="toIndex">The end of restoring indices.</param>
        /// <returns>A collection including the requested missing data.</returns>
        Task<IEnumerable<MissingData>> RestoreRange(string publisherId, long fromIndex, long toIndex);
    }
}