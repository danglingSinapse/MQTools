using NetMQ;

namespace CPR.MQTools.Interfaces
{
    //TODO: can it be merged with IWorkRepository?? why not have a priority queue here as well??
    
    /// <summary>
    /// This class is an abstraction of storing pending works in relaxed workers. Relaxed worker will except adding
    /// and removing a work index from the backend of this interface.
    /// </summary>
    public interface IPendingWorkRepository
    {
        /// <summary>
        /// This method should add a given message to the backend repository.
        /// </summary>
        /// <param name="message">The actual work message to be stored.</param>
        /// <param name="workIndex">The index of the work.</param>
        void AddPendingWork(NetMQMessage message, long workIndex);
        
        /// <summary>
        /// This method should remove a work with the given index from the backend repository.
        /// </summary>
        /// <param name="workIndex">The index of the work to be removed.</param>
        /// <returns>Whether the remove operation was successful or not.</returns>
        bool RemovePendingWork(long workIndex);
        
        
        /// <summary>
        /// This method should return the current number of works in the repository.
        /// </summary>
        /// <returns>The number of works in the repository.</returns>
        int GetCount();
    }
}