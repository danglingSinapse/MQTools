﻿namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This an abstraction of a type of functionality in which it requires to be informed about the network connection
    /// events.
    /// </summary>
    public interface IMqConnectionInformee
    {
        /// <summary>
        /// This callback should be called when there is a new entity became online.
        /// </summary>
        ConnectionCallback EntityOnline { get; }
        
        /// <summary>
        /// This callback should be called when a previously online entity became offline.
        /// </summary>
        DisconnectionCallback EntityOffline { get; }
        
        /// <summary>
        /// This callback should be called when the current entity has been bound to a port.
        /// </summary>
        BindingCallback EntityBound { get; }
        
        /// <summary>
        /// This callback should be called if there is a change in the current entity's identity.
        /// </summary>
        IdentitySetCallback IdentitySet { get; }
    }
}