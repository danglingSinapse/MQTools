﻿namespace CPR.MQTools.Interfaces
{
    //TODO: if an entity does not include a connection informer functionality it still can deduce the state of a remote entity when received a message from it and by health checking it can deduce its disconnection
    
    public delegate void ConnectionCallback(Identity identity);

    public delegate void DisconnectionCallback(Identity identity);

    public delegate void BindingCallback(int port);

    public delegate void AddEntityMonitoringCallback(Identity identity);

    public delegate void RemoveEntityMonitoringCallback(Identity identity);
    
    public delegate void IdentitySetCallback(Identity identity);

    /// <summary>
    /// This an abstraction of a type of functionality in which it wants to inform other functionalities about
    /// connection events.
    /// </summary>
    public interface IMqConnectionInformer
    {
        /// <summary>
        /// This event should be invoked when this functionality deduced an entity to be online.
        /// </summary>
        event ConnectionCallback EntityOnline;
        
        /// <summary>
        /// This event should be invoked when this functionality deduced an entity to be offline.
        /// </summary>
        event DisconnectionCallback EntityOffline;
        
        /// <summary>
        /// This event should be invoked when this functionality deduced that it is bound to a port.
        /// </summary>
        event BindingCallback EntityBound;
        
        /// <summary>
        /// This event should be invoked when this functionality deduced that it has changed to current entity's identity.
        /// </summary>
        event IdentitySetCallback IdentitySet;

        /// <summary>
        /// This callback should be invoked by MqEntity to order a health monitoring of a remote entity. After receiving
        /// a call it should inform if a remote entity is disconnected.
        /// </summary>
        AddEntityMonitoringCallback AddEntityMonitoring { get; }
        
        /// <summary>
        /// This callback should be invoked by MqEntity to order a stop to health monitoring of a remote entity. After
        /// receiving a call it should stop monitoring the health of the given remote entity.
        /// </summary>
        RemoveEntityMonitoringCallback RemoveEntityMonitoring { get; }
    }
}