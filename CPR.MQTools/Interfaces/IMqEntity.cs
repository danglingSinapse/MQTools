﻿using System.Threading.Tasks;
using CPR.MQTools.Functions;
using NetMQ;
using NLog;

namespace CPR.MQTools.Interfaces
{
    /// <summary>
    /// This is an abstraction of the core class which wraps a zmq socket. This will manage functionalities and will
    /// engage in sending and receiving the messages. 
    /// </summary>
    public interface IMqEntity
    {
        /// <summary>
        /// A one-threaded task scheduler which all the process of this class and its functionalities should use to
        /// prevent locks and data racing. 
        /// </summary>
        NetMQPoller Poller { get; }
        
        /// <summary>
        /// The factory object of the poller which accepts new tasks to be run on its thread.
        /// </summary>
        TaskFactory PollerTaskFactory { get; }
        
        /// <summary>
        /// The wrapped socket of zmq.
        /// </summary>
        NetMQSocket Socket { get; }
        
        /// <summary>
        /// The logger object which will be used in this class and its functionalities.
        /// </summary>
        ILogger Logger { get; }
        
        /// <summary>
        /// This method will actually send a message on wire.
        /// </summary>
        /// <param name="identity">The identity of the target remote socket.</param>
        /// <param name="messageType">The message type of this message which should be presented on a  frame after
        /// identity.</param>
        /// <param name="message">The actual message to be sent.</param>
        /// <returns>A boolean which tells weather the sending was successful or not.</returns>
        Task<bool> SendMessage(Identity identity, MessageType messageType, NetMQMessage message = null);
        
        /// <summary>
        /// This method will actually send a message on wire. But it should interact with load balancers to figure out
        /// the identity which the message should be sent to.
        /// </summary>
        /// <param name="messageType">The message type of this message which should be presented on a  frame after
        /// identity.</param>
        /// <param name="message">The actual message to be sent.</param>
        /// <returns>A boolean which tells weather the sending was successful or not.</returns>
        Task<Identity> SendMessage(MessageType messageType, NetMQMessage message = null);
        
        /// <summary>
        /// This method should be used to register a callback which will handle the processing of a type of messages.
        /// </summary>
        /// <param name="messageType">The target MessageType which this handler should be registered for.</param>
        /// <param name="callback">The actual callback which should be called when a message of the given type is
        /// received.</param>
        /// <param name="isPlugin">This parameter controls the running of the callback. If it is set to true,
        /// it will run on this object's poller. If it is false, it will be run on .net's default task scheduler.</param>
        void RegisterReceiveHandler(MessageType messageType, ReceiveMiddleware callback, bool isPlugin = false);
        
        /// <summary>
        /// This method should be used to register a callback which will act as a middleware for sending the given
        /// message types. This callback is allowed to manipulate the sending message on the wire. 
        /// </summary>
        /// <param name="messageType">The target MessageType which this handler should be registered for.</param>
        /// <param name="callback">The actual callback which should be called when a message of the given type is
        /// going to be sent.</param>
        void RegisterSendHandler(MessageType messageType, SendMiddleware callback);
        
        /// <summary>
        /// This method will connect the inner socket to a given address.
        /// </summary>
        /// <param name="address">The address which the socket should be connected to.</param>
        void Connect(string address);
        
        /// <summary>
        /// This method will disconnect the inner socket from a given address.
        /// </summary>
        /// <param name="address">The address which the socket should be disconnected from.</param>
        void Disconnect(string address);
        
        /// <summary>
        /// This method should be called to give a message to this class to be processed as if it is received from the
        /// wire. This will usually be called from a functionality which has processed a previously received message
        /// from the wire, manipulated it, and wants an other middleware to continue the processing.
        /// </summary>
        /// <param name="identity">The identity of an endpoint which this message is received from.</param>
        /// <param name="message">The actual message to be processed.</param>
        Task MessageReceived(Identity identity, NetMQMessage message);
        
        /// <summary>
        /// An overload of MessageReceived which also accepts the MessageType of the message. Note that this method
        /// will not assume that the first frame is going to be the MessageType of the message. This method should be
        /// called to give a message to this class to be processed as if it is received from the  wire. This will
        /// usually be called from a functionality which has processed a previously received message from the wire,
        /// manipulated it, and wants an other middleware to continue the processing.
        /// </summary>
        /// <param name="identity">The identity of an endpoint which this message is received from.</param>
        /// <param name="messageType">The message type of the received message.</param>
        /// <param name="message">The actual message to be processed.</param>
        Task MessageReceived(Identity identity, MessageType messageType, NetMQMessage message);
        
        /// <summary>
        /// This method accepts requests for monitoring a certain endpoint for its health. MqEntity should pass it to
        /// ConnectionInformers to be monitored.
        /// </summary>
        /// <param name="identity">The identity of an endpoint which should be monitored.</param>
        void AddEntityMonitoring(Identity identity);
        
        /// <summary>
        /// This method accepts requests for stop monitoring a certain endpoint for its health. MqEntity should pass it
        /// to ConnectionInformers to stop the monitoring process for this endpoint.
        /// </summary>
        /// <param name="identity">The identity of an endpoint which should be monitored.</param>
        void RemoveEntityMonitoring(Identity identity);
        
        /// <summary>
        /// This method should be called to inform the MqEntity that the identity of the managing socket has been
        /// changed.
        /// </summary>
        /// <param name="selfIdentity">The new identity which has been set.</param>
        void IdentitySet(Identity selfIdentity);
        
        /// <summary>
        /// This method should be used to introduce a new functionality to the MqEntity class.
        /// </summary>
        /// <param name="function">The actual function should be added to this entity's functionalities.</param>
        /// <param name="name">The name of this functionality. This will be put in monitoring object which will be
        /// exposed by MonitoringDevice.</param>
        void AddFunctionality(MqFunctionBase function, string name = "");
        
        /// <summary>
        /// This method should be called to command a binding for the wrapped socket.
        /// </summary>
        /// <param name="bindPort">The port number which the socket should be bound to. The default value will bind the
        /// socket to a random open port.</param>
        void Bind(int bindPort = 0);
        
        /// <summary>
        /// This method should run the poller object on the current thread. It will block the execution.
        /// </summary>
        void Run();
        
        /// <summary>
        /// This method should run the poller object on a background thread asynchronously.
        /// </summary>
        Task RunAsync();
    }
}