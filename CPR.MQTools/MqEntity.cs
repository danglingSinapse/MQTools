﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CPR.MQTools.Functions;
using CPR.MQTools.Interfaces;
using NetMQ;
using NetMQ.Sockets;
using NLog;

namespace CPR.MQTools
{
    public delegate Task ExceptionHandler(Exception e, Identity identity, MessageType messageType,
        NetMQMessage message);

    /// <summary>
    /// This class is the core of wrapping a zmq socket. This will manage functionalities and will engage in sending
    /// and receiving the messages. 
    /// </summary>
    public class MqEntity : IMqEntity
    {
        /// <summary>
        /// This class encapsulates the information needed to for sending a message.
        /// </summary>
        private class SendItem
        {
            /// <summary>
            /// The target identity which this message should be sent to.
            /// </summary>
            public Identity Target;
            
            /// <summary>
            /// The actual message to be sent.
            /// </summary>
            public NetMQMessage Message;
            
            /// <summary>
            /// The message type of this message.
            /// </summary>
            public MessageType Type;
            
            /// <summary>
            /// The completion source of sending this message. One can await on the completion of this task.
            /// </summary>
            public readonly TaskCompletionSource<bool> SendingTask = new TaskCompletionSource<bool>();
        }

        /// <summary>
        /// The instances of MqEntity class which has been created so far.
        /// </summary>
        public static readonly List<MqEntity> Instances = new List<MqEntity>();
        
        /// <summary>
        /// A collection which contains functionalities of this socket.
        /// </summary>
        private readonly List<MqFunctionBase> _functions;
        
        /// <summary>
        /// A collection which contains the registered receive middlewares so far.
        /// </summary>
        private readonly List<ReceiveHandleInfo> _receiveMiddlewares;
        
        /// <summary>
        /// A collection which contains the registered send middlewares so far. 
        /// </summary>
        private readonly List<SendHandleInfo> _sendMiddlewares;
        
        /// <summary>
        /// The task factory of the internal poller. This can be used to inject additional tasks for the poller.
        /// </summary>
        public TaskFactory PollerTaskFactory { get; }
        
        /// <summary>
        /// This semaphore will be used to control the concurrent tasks which has been queued for the .net's default
        /// task scheduler. 
        /// </summary>
        private readonly SemaphoreSlim _receiveSemaphore; //TODO: Think of some other way to control concurrent jobs
        
        /// <summary>
        /// This event will be invoked if a new message is received but no handler has been registered to process its
        /// type of message.
        /// </summary>
        public event ReceiveMiddleware OnReceiveWithNoHandler;
        
        /// <summary>
        /// This event will be invoked if a receive middleware has thrown an exception. In this case, the application
        /// can be informed about the problem. Also the task of processing that message will be faulty by this exception.
        /// </summary>
        public event ExceptionHandler OnReceiveException;
        
        /// <summary>
        /// This event will be invoked when a new remote entity (endpoint) has became online.
        /// </summary>
        public event ConnectionCallback OnEntityOnline;
        
        /// <summary>
        /// This event will be invoked when a new remote entity (endpoint) has became offline.
        /// </summary>
        public event DisconnectionCallback OnEntityOffline;
        
        /// <summary>
        /// This event will be invoked when a this socket has been bound to a port.
        /// </summary>
        public event BindingCallback OnBind;
        
        /// <summary>
        /// This event will be invoked when the identity of this socket has been changed.
        /// </summary>
        public event IdentitySetCallback OnIdentitySet;
        
        /// <summary>
        /// This event will be invoked when there is a need to monitor an endpoint for its health. Hence the
        /// functionalities of type ConnectionInformer will register a callback for this event to get the monitoring
        /// requests.
        /// </summary>
        protected event AddEntityMonitoringCallback OnAddEntityMonitoring;
        
        /// <summary>
        /// This event will be invoked when monitoring of an endpoint's health needs to be stopped. Hence the
        /// functionalities of type ConnectionInformer will register a callback.
        /// </summary>
        protected event RemoveEntityMonitoringCallback OnRemoveEntityMonitoring;
        
        /// <summary>
        /// This is a container of functionalities which implement load balancer interface.
        /// </summary>
        private readonly List<IMqLoadBalancer> _loadBalancers;
        
        /// <summary>
        /// This is a collection containing the addresses which this entity is connected to.
        /// </summary>
        private readonly List<string> _connectedAddresses;
        
        public delegate object ApplicationHealthInformationCallback();
        
        /// <summary>
        /// This is a callback which can be set by the application to gather its health information as well as health
        /// information of functionalities and mqEntity.
        /// </summary>
        private ApplicationHealthInformationCallback _applicationHealthInformation;
        
        /// <summary>
        /// The logger which will be used to log events occured in the process of messages.
        /// </summary>
        public ILogger Logger { get; }
        
        /// <summary>
        /// The actual socket which this class is wrapping.
        /// </summary>
        public NetMQSocket Socket { get; }
        
        /// <summary>
        /// The poller object which will be used to schedule this object and its functionalities processes in one single
        /// thread and avoid locks. 
        /// </summary>
        public NetMQPoller Poller { get; }
        
        /// <summary>
        /// This queue will be used for sending messages on the wire.
        /// </summary>
        private NetMQQueue<SendItem> SendQueue { get; }
        
        /// <summary>
        /// The name of this object which will be put in the health information's output object.
        /// </summary>
        public string EntityName { get; set; }

        private readonly PerformanceSampler _sendingPerformance = new PerformanceSampler(100);
        
        private readonly RateSampler _sendRateSampler;
        private readonly RateSampler _receiveRateSampler;
        
        /// <param name="socket">The socket which this object should wrap.</param>
        /// <param name="poller">The poller object used to schedule this object's 'processes. If null is passed, a new
        /// one will be created.</param>
        /// <param name="logger">The logger object, which should be used to log processing events. If null is passed,
        /// a new logger with name MqTools will be obtained from the NLog library.</param>
        /// <param name="entityName">The name of this object which will be put on health information's output object.
        /// The default value will use the type name of this object.</param>
        /// <param name="concurrentJobs">The max number of concurrent tasks to be scheduled for .net's default
        /// task scheduler by this object.</param>
        public MqEntity(NetMQSocket socket, NetMQPoller poller = null, ILogger logger = null, string entityName = "", int concurrentJobs = 300)
        {
            Logger = logger ?? LogManager.GetLogger("MqTools");
            _connectedAddresses = new List<string>();
            _functions = new List<MqFunctionBase>();
            _receiveMiddlewares = new List<ReceiveHandleInfo>();
            _sendMiddlewares = new List<SendHandleInfo>();
            _loadBalancers = new List<IMqLoadBalancer>();
            EntityName = string.IsNullOrEmpty(entityName) ? GetType().Name : entityName;
            socket.Options.RouterMandatory = true;
            Socket = socket;
            Socket.ReceiveReady += MessageReceived;
            Poller = poller ?? new NetMQPoller();
            SendQueue = new NetMQQueue<SendItem>();
            SendQueue.ReceiveReady += SendReady;
            Poller.Add(Socket);
            Poller.Add(SendQueue);
            
            _sendRateSampler = new RateSampler(TimeSpan.FromSeconds(5), Poller);
            _receiveRateSampler = new RateSampler(TimeSpan.FromSeconds(5), Poller);
            
            PollerTaskFactory = new TaskFactory(Poller);
            _receiveSemaphore = new SemaphoreSlim(concurrentJobs, concurrentJobs);
            Instances.Add(this);
        }

        /// <summary>
        /// This method will be called if there is an item in the sending queue. It will try and process every item in
        /// the queue, if it has no target it will assign an identity from load balancers. Then it will pass the message
        /// to any registered send middlewares of this message's type'. At the end, it will put the message on the wire
        /// and complete the sending task.   
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendReady(object sender, NetMQQueueEventArgs<SendItem> e)
        {
            var sendWatch = _sendingPerformance.StartSample();

            while (SendQueue.Any())
            {
                var sendItem = SendQueue.Dequeue();
                if (sendItem != null)
                {
                    try
                    {
                        if (sendItem.Message == null) sendItem.Message = new NetMQMessage();

                        if (sendItem.Target == null)
                        {
                            foreach (var loadBalancer in _loadBalancers)
                            {
                                sendItem.Target = loadBalancer.GetNextNode();
                                if (sendItem.Target != null) break;
                            }

                            sendItem.Target = _loadBalancers.Any()
                                ? sendItem.Target
                                : new Identity(Socket.Options.LastPeerRoutingId);
                        }

                        if (sendItem.Target != null)
                        {
                            //TODO: call send middlewares in certain order with respect to their functionality
                            foreach (var handler in _sendMiddlewares.Where(x => x.MessageType == sendItem.Type))
                            {
                                handler.Callback(this, ref sendItem.Target, ref sendItem.Type, ref sendItem.Message);
                            }

                            sendItem.Message.Push(sendItem.Type);
                            if (sendItem.Target != null)
                            {
                                sendItem.Message.PushEmptyFrame();
                                sendItem.Message.Push(sendItem.Target);
                            }

                            sendItem.SendingTask.SetResult(Socket.TrySendMultipartMessage(sendItem.Message));
                            var now = DateTime.Now;
                            _sendRateSampler.AddSample();
                        }
                        else
                        {
                            Logger.Error($"There is no peers connected to send message {sendItem}");
                            sendItem.SendingTask.SetResult(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"Sending message {sendItem} encountered exception: {ex}");
                        sendItem.SendingTask.SetResult(false);
                    }
                }
                else
                {
                    Logger.Error("SendQueue ready method dequeued null");
                }
            }

            _sendingPerformance.StopSample(sendWatch);
        }

        /// <summary>
        /// This method will add a given functionality to this object and based on the type of this functionality, it
        /// will setup event connection needed for it to operate. 
        /// </summary>
        /// <param name="function">The function it that is needed to be add to this object.</param>
        /// <param name="name">The name of the function that is going to be put on the health information.</param>
        public void AddFunctionality(MqFunctionBase function, string name = "")
        {
            if (!string.IsNullOrEmpty(name))
                function.Name = name;
            
            _functions.Add(function);
            function.SetEntity(this);

            if (function is IMqConnectionInformer connectionInformer)
            {
                connectionInformer.EntityOnline += EntityOnline;
                connectionInformer.EntityOffline += EntityOffline;
                connectionInformer.EntityBound += EntityBound;
                OnAddEntityMonitoring += connectionInformer.AddEntityMonitoring;
                OnRemoveEntityMonitoring += connectionInformer.RemoveEntityMonitoring;
            }

            if (function is IMqConnectionInformee connectionInformee)
            {
                OnEntityOnline += connectionInformee.EntityOnline;
                OnEntityOffline += connectionInformee.EntityOffline;
                OnBind += connectionInformee.EntityBound;
                OnIdentitySet += connectionInformee.IdentitySet;
            }

            if (function is IMqReceiveMiddleware receiveMiddleware)
            {
                _receiveMiddlewares.AddRange(receiveMiddleware.GetReceiveMiddlewares());
            }

            if (function is IMqSendMiddleware sendMiddleware)
            {
                _sendMiddlewares.AddRange(sendMiddleware.GetSendMiddlewares());
            }

            if (function is IMqLoadBalancer loadBalancer)
            {
                _loadBalancers.Add(loadBalancer);
            }
        }

        /// <summary>
        /// This method will be registered for EntityOnline events in connection informer functions. It will invoke
        /// OnEntityOnline which will inform connection informee functions as well as any additional listener registered
        /// for it.
        /// </summary>
        /// <param name="identity">The identity which has became online.</param>
        private void EntityOnline(Identity identity)
        {
            OnEntityOnline?.Invoke(identity);
        }

        /// <summary>
        /// This method will be registered for EntityOffline events in connection informer functions. It will invoke
        /// OnEntityOffline which will inform connection informee functions as well as any additional listener registered
        /// for it. 
        /// </summary>
        /// <param name="identity">The identity which has became offline.</param>
        private void EntityOffline(Identity identity)
        {
            OnEntityOffline?.Invoke(identity);
        }

        /// <summary>
        /// This method will be registered for EntityBound events in connection informer functions. It will invoke
        /// OnBind which will inform connection informee functions as well as any additional listener registered
        /// for it. 
        /// </summary>
        /// <param name="port">The port which the socket has bound to.</param>
        private void EntityBound(int port)
        {
            Logger.Info($"Bound to port {port}");
            OnBind?.Invoke(port);
        }

        /// <summary>
        /// This method will put a message on send queue to be sent on wire. It does not get the target identity which
        /// will result in sending to endpoints using a load balancer function.
        /// </summary>
        /// <param name="messageType">The type of the message.</param>
        /// <param name="message">The actual message to be sent on wire.</param>
        /// <returns>A task which will provide the identity as its result. This identity is the endpoint which this
        /// message has been sent to.</returns>
        public async Task<Identity> SendMessage(MessageType messageType, NetMQMessage message = null)
        {
            var sendItem = new SendItem
            {
                Message = message,
                Type = messageType
            };
            SendQueue.Enqueue(sendItem);
            await sendItem.SendingTask.Task;
            return sendItem.Target;
        }

        /// <summary>
        /// This method will put a message on send queue to be sent on wire. It accepts identity which will directly
        /// send this message to that endpoint.
        /// </summary>
        /// <param name="identity">The identity which this message should be sent to.</param>
        /// <param name="messageType">The type of the message.</param>
        /// <param name="message">The actual message to be sent on wire.</param>
        /// <returns>A task which will provide a boolean as its result. This boolean determines whether sending was
        /// successful or not.</returns>
        public Task<bool> SendMessage(Identity identity, MessageType messageType, NetMQMessage message = null)
        {
            var sendItem = new SendItem
            {
                Target = identity,
                Message = message,
                Type = messageType
            };
            SendQueue.Enqueue(sendItem);
            return sendItem.SendingTask.Task;
        }

        /// <summary>
        /// This methods commands the socket to bind to a given port. 
        /// </summary>
        /// <param name="bindPort">The port which this socket should be bound to. The default value will bind to a
        /// random port.</param>
        public void Bind(int bindPort = 0)
        {
            if (bindPort == 0)
                bindPort = Socket.BindRandomPort("tcp://*");
            else
                Socket.Bind($"tcp://*:{bindPort}");

            EntityBound(bindPort);
        }

        /// <summary>
        /// This method commands the socket to connect to a given address.
        /// </summary>
        /// <param name="address">The address which this socket should connect to.</param>
        public void Connect(string address)
        {
            _connectedAddresses.Add(address);
            Socket.Connect(address);
            EntityOnline(new Identity(address));
        }

        /// <summary>
        /// This method commands the socket to disconnect from a given address.
        /// </summary>
        /// <param name="address">The address which this socket should be disconnected from.</param>
        public void Disconnect(string address)
        {
            if (!_connectedAddresses.Remove(address)) return;
            Socket.Disconnect(address);
            EntityOffline(new Identity(address));
        }

        /// <summary>
        /// This method runs the internal poller synchronously.
        /// </summary>
        public void Run()
        {
            Poller.Run();
        }

        private Thread _pollerThread;
        /// <summary>
        /// This method runs the internal poller asynchronously.
        /// </summary>
        /// <returns></returns>
        public Task RunAsync()
        {
            var threadTask = new TaskCompletionSource<object>();
            _pollerThread = new Thread(() =>
            {
                Poller.Run();
                threadTask.SetResult(null);
            }) { Name = "NetMQPollerThread" };
            _pollerThread.Start();
            return threadTask.Task;
        }

        /// <summary>
        /// This method will force the poller to be stopped immediately.
        /// </summary>
        public void ForceStop()
        {
            if (Poller.IsRunning)
                Poller.Stop();
        }

        //TODO: write gracefully shutdown tests
        /// <summary>
        /// This method will stop the poller gracefully. Which will also stop all the functions gracefully.
        /// </summary>
        /// <returns>A task which will be completed when all the functions has finished working in a graceful manner.</returns>
        public Task StopGracefully()
        {
            return Task.WhenAll(_functions.Select(f => PollerTaskFactory.StartNew(f.StopGracefully))).ContinueWith(x => ForceStop());
        }

        //TODO: think of a way to remove messageType from this API and send API
        /// <summary>
        /// This method will register a receive handler for a given message type. This handler will be called to process
        /// if any message of the specified type has been received.
        /// </summary>
        /// <param name="messageType">The desired message type which the passed callback is going to be registered for.</param>
        /// <param name="callback">The callback to be called.</param>
        /// <param name="isPlugin">This parameter determines if this handler should be run on the internal poller
        /// (true value) or on the .net's default task scheduler (false value).</param>
        public void RegisterReceiveHandler(MessageType messageType, ReceiveMiddleware callback, bool isPlugin = false)
        {
            _receiveMiddlewares.Add(new ReceiveHandleInfo
                {Callback = callback, MessageType = messageType, IsPlugin = isPlugin});
        }
        
        /// <summary>
        /// This method will register a send middleware to be called if the given message type wants to be sent on the
        /// wire. This middleware may manipulate the message that is going to be sent.
        /// </summary>
        /// <param name="messageType">The message type which this send middleware wants to be put in the way of sending.</param>
        /// <param name="callback">The actual callback to be called.</param>
        public void RegisterSendHandler(MessageType messageType, SendMiddleware callback)
        {
            _sendMiddlewares.Add(new SendHandleInfo
                {Callback = callback, MessageType = messageType});
        }

        /// <summary>
        /// This method is the entry point of incoming messages. It will be called if there is any messages on the wire
        /// waiting to be read. It will try to read all the messages until the queue is empty, also it will separate
        /// the incoming message to its identity and the rest of the message.
        /// </summary>
        public void MessageReceived(object sender, NetMQSocketEventArgs args)
        {
            var msg = new NetMQMessage();
            while (args.Socket.TryReceiveMultipartMessage(ref msg))
            {
                var now = DateTime.Now;
                _receiveRateSampler.AddSample();
                Identity identity = null;
                if (Socket is RouterSocket)
                {
                    identity = new Identity(msg.Pop());
                    if (msg.First.IsEmpty)
                        msg.Pop();
                }

                MessageReceived(identity, msg);
                msg = new NetMQMessage();
            }
        }

        /// <summary>
        /// This method will unwrap an incoming message and separate its message type from its body. Then passes the
        /// information to another overloaded version of this method to continue its process. 
        /// </summary>
        /// <param name="identity">The identity of the endpoint which has been sent this message.</param>
        /// <param name="message">The actual received message.</param>
        /// <returns>A task which will be completed when the process of this message has been completed.</returns>
        public Task MessageReceived(Identity identity, NetMQMessage message)
        {
            var messageType = message.Any() ? message.Pop() : NetMQFrame.Empty;
            return MessageReceived(identity, new MessageType(messageType), message);
        }

        /// <summary>
        /// This method will queue the received message to be processed and immediately returns the control to the
        /// caller. In the body of the processing, this will look for any receive middleware which can handle this
        /// message's type and will schedule it on poller or .net's default task scheduler based on its IsPlugin
        /// configuration. Also this will call OnReceiveWithNoHandler if no middleware was registered for this type.
        /// Besides, in case any of these handlers has thrown an exception, it will catch them and call the
        /// OnReceiveException event.
        /// </summary>
        /// <param name="identity">The identity of the endpoint which has been sent this message.</param>
        /// <param name="messageType">The message type of this message.</param>
        /// <param name="message">The actual received message.</param>
        /// <returns>A task which will be completed when the process of this message has been completed.</returns>
        public Task MessageReceived(Identity identity, MessageType messageType, NetMQMessage message)
        {
            return PollerTaskFactory.StartNew(() =>
            {
                var handlers = _receiveMiddlewares.Where(x => x.MessageType == messageType).ToArray();
                if (!handlers.Any())
                {
                    if (OnReceiveWithNoHandler != null)
                    {
                        return Task.Factory.StartNew(async () =>
                        {
                            await _receiveSemaphore.WaitAsync();
                            try
                            {
                                await OnReceiveWithNoHandler(this, identity, messageType, new NetMQMessage(message));
                            }
                            catch (Exception e)
                            {
                                OnReceiveException?.Invoke(e, identity, messageType, message);
                                throw;
                            }
                            finally
                            {
                                _receiveSemaphore.Release();
                            }
                        }).Unwrap();
                    }

                    Logger.Warn($"No handler registered for type {messageType.ConvertToString()}");
                    return Task.CompletedTask;
                }

                return Task.WhenAll(handlers.Select(handler =>
                {
                    if (handler.IsPlugin)
                    {
                        try
                        {
                            handler.Callback(this, identity, messageType, new NetMQMessage(message));
                            return Task.CompletedTask;
                        }
                        catch (Exception e)
                        {
                            OnReceiveException?.Invoke(e, identity, messageType, message);
                            return Task.FromException(e);
                        }
                    }

                    return Task.Factory.StartNew(async () =>
                    {
                        await _receiveSemaphore.WaitAsync();
                        try
                        {
                            await handler.Callback(this, identity, messageType, new NetMQMessage(message));
                        }
                        catch (Exception e)
                        {
                            OnReceiveException?.Invoke(e, identity, messageType, message);
                            throw;
                        }
                        finally
                        {
                            _receiveSemaphore.Release();
                        }
                    }).Unwrap();
                }));
            }).Unwrap();
        }

        /// <summary>
        /// This method will accept requests for monitoring an identity an will pass it to connection informers.
        /// </summary>
        /// <param name="identity">The identity of the endpoint which wants to be monitored.</param>
        public void AddEntityMonitoring(Identity identity)
        {
            OnAddEntityMonitoring?.Invoke(identity);
        }

        /// <summary>
        /// This method will accept requests for stop monitoring an identity an will pass it to connection informers.
        /// </summary>
        /// <param name="identity">The identity of the endpoint which its monitoring wants to be stoped.</param>
        public void RemoveEntityMonitoring(Identity identity)
        {
            OnRemoveEntityMonitoring?.Invoke(identity);
        }

        /// <summary>
        /// This method will be called if the identity of this socket has been changed. It will inform connection
        /// informee functionalities. by calling OnIdentitySet event.
        /// </summary>
        /// <param name="selfIdentity"></param>
        public void IdentitySet(Identity selfIdentity)
        {
            OnIdentitySet?.Invoke(selfIdentity);
        }

        /// <summary>
        /// This method can be used to set a callback which will be used to gather application's health information.
        /// the output of this method will be included in health information of this object.
        /// </summary>
        /// <param name="callback">The actual callback which will be called for application's health information.</param>
        public void SetApplicationHealthInformation(ApplicationHealthInformationCallback callback)
        {
            _applicationHealthInformation = callback;
        }

        /// <summary>
        /// This method gathers health information of this object's functionalities along with this entity's metrics.
        /// And creates the output object to be returned.
        /// </summary>
        /// <returns>The output object containing health metrics.</returns>
        public virtual Task<Dictionary<string, object>> GetHealthInformation()
        {
            if (!Poller.IsRunning)
                return Task.FromResult(new Dictionary<string, object> {{"is_running", false}});
            
            return PollerTaskFactory.StartNew(() =>
            {
                try
                {
                    var groups = _functions.GroupBy(func => func.Name);
                    var dict = groups.SelectMany(group =>
                        group.Count() > 1
                            ? group.Select((func, index) => new KeyValuePair<MqFunctionBase, int>(func, index))
                            : new[] {new KeyValuePair<MqFunctionBase, int>(group.First(), -1)}).ToDictionary(funcInfo =>
                            funcInfo.Value == -1
                                ? funcInfo.Key.Name
                                : funcInfo.Key.Name + "_" + funcInfo.Value.ToString(),
                        funcInfo => funcInfo.Key.GetHealthInformation());
                    var appObj = _applicationHealthInformation?.Invoke();
                    if (appObj != null)
                        dict["application"] = appObj;
                    var now = DateTime.Now;
                    dict["entity"] = new
                    {
                        current_free_jobs = _receiveSemaphore.CurrentCount,
                        sending_queue_count = SendQueue.Count(),
                        sending_performance = _sendingPerformance.GetAverage(),
                        send_rate = _sendRateSampler.GetCount(),
                        receive_rate = _receiveRateSampler.GetCount()
                    };
                    return dict;
                }
                catch (Exception e)
                {
                    Logger.Error($"Error accured while getting health informations: {e}");
                    return new Dictionary<string, object>();
                }
            });
        }
    }
}