﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace CPR.MQTools
{
    public static class NetworkHelper
    {
        /// <summary>
        /// This method will automatically export the ip address of this machine on a given interface or an ethernet one.
        /// </summary>
        /// <param name="networkInterfaceName">The name of the interface to export this machine's ip. The default value
        /// will use the first up and running ethernet interface.</param>
        /// <returns>The ip of this machine.</returns>
        /// <exception cref="Exception">If an ip could not be retrieved.</exception>
        public static string GetCurrentIp(string networkInterfaceName = "")
        {
            var ip = NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(x =>
                    x.OperationalStatus == OperationalStatus.Up &&
                    x.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    (string.IsNullOrEmpty(networkInterfaceName) || x.Name == networkInterfaceName))
                ?.GetIPProperties().UnicastAddresses
                .FirstOrDefault(x => x.Address.AddressFamily == AddressFamily.InterNetwork)
                ?.Address.ToString();
            
            if (ip == null)
                throw new Exception(
                    $"No IP address could be retrieved on this machine. InterfaceName = {networkInterfaceName}.");

            return ip;
        }
    }
}