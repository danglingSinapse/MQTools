using System.Collections.Generic;

namespace CPR.MQTools
{
    /// <summary>
    /// This class simply accepts the latest passed number of samples of float types and provides the user with its
    /// average.
    /// </summary>
    public class SimpleMovingAverage
    {
        private readonly List<float> _samples;
        private float _total;
        private readonly int _maxSampleCount;
        private int _firstSample;

        public float Average
        {
            get
            {
                if (_samples.Count == 0)
                {
                    return 0;
                }

                return _total / _samples.Count;
            }
        }

        public SimpleMovingAverage(int maxSamplesCount)
        {
            _maxSampleCount = maxSamplesCount;
            _samples = new List<float>();
            _total = 0;
        }

        public void AddSample(int val)
        {
            AddSample((float) val);
        }

        public void AddSample(long val)
        {
            AddSample((float) val);
        }

        public void AddSample(float val)
        {
            if (_samples.Count == _maxSampleCount)
            {
                _total -= _samples[_firstSample];
                _samples[_firstSample] = val;
                _firstSample = ++_firstSample % _maxSampleCount;
            }
            else
            {
                _samples.Add(val);
            }
            
            _total += val;
        }

        public void ClearSamples()
        {
            _total = 0;
            _firstSample = 0;
            _samples.Clear();
        }
    }
}