using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Unosquare.Labs.EmbedIO;
using Unosquare.Labs.EmbedIO.Constants;
using Unosquare.Labs.EmbedIO.Modules;
using Unosquare.Swan;

namespace CPR.MQTools
{
    /// <summary>
    /// These classes will construct a tiny web server which will answer http calls for the application's health.
    /// This can be used to extract metrics of the internal components of the application.
    /// </summary>
    public class MonitoringDevice
    {
        private readonly WebServer _server;
        private Task _serverTask;

        /// <param name="entities">The entity which their health should be monitored. If it is null, all the created
        /// instances of MqEntity will be monitored automatically.</param>
        /// <param name="monitoringPort">The port which this web service should be run on.</param>
        public MonitoringDevice(IEnumerable<MqEntity> entities = null, int monitoringPort = 80)
        {
            Terminal.Settings.DisplayLoggingMessageType = LogMessageType.None;

            if (entities == null)
                entities = MqEntity.Instances;
            
            HealthController.Entities = entities.ToList();
            _server = new WebServer(monitoringPort);
            _server.RegisterModule(new WebApiModule());
            _server.Module<WebApiModule>().RegisterController<HealthController>();
            _serverTask = _server.RunAsync();
        }
    }
    
    internal class HealthController : WebApiController
    {
        public static List<MqEntity> Entities;
        
        public HealthController(IHttpContext context) : base(context)
        {
        }
        
        /// <summary>
        /// This is the actual handler of http request to health url. This will call the GetHealthInformation methods
        /// of monitoring MqEntity objects and convert the returned object to a json response.
        /// </summary>
        [WebApiHandler(HttpVerbs.Get, "/health")]
        public Task<bool> GetHealth()
        {
            var groups = Entities.GroupBy(ent => ent.EntityName);
            var re = groups.SelectMany(group =>
                group.Count() > 1
                    ? group.Select((ent, index) => new KeyValuePair<MqEntity, int>(ent, index))
                    : new[] {new KeyValuePair<MqEntity, int>(group.First(), -1)}).ToDictionary(entInfo =>
                    entInfo.Value == -1
                        ? entInfo.Key.EntityName
                        : entInfo.Key.EntityName + "_" + entInfo.Value.ToString(),
                funcInfo => funcInfo.Key.GetHealthInformation().Result);
            
            return this.JsonResponseAsync(JsonConvert.SerializeObject(re));
        }

    }
}