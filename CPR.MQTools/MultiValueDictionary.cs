﻿using System.Collections.Generic;

namespace CPR.MQTools
{
    /// <summary>
    /// This class extends Dictionary type to accept multiple values for a unique given key.
    /// </summary>
    public class MultiValueDictionary<TKey, TValue> : Dictionary<TKey, HashSet<TValue>>
    {
        public void Add(TKey key, TValue value)
        {
            if (!TryGetValue(key, out var container))
            {
                container = new HashSet<TValue>();
                base.Add(key, container);
            }

            container.Add(value);
        }

        public bool ContainsValue(TKey key, TValue value)
        {
            var toReturn = false;
            if (TryGetValue(key, out var values))
            {
                toReturn = values.Contains(value);
            }

            return toReturn;
        }

        public void Remove(TKey key, TValue value)
        {
            if (!TryGetValue(key, out var container)) return;
            container.Remove(value);
            if (container.Count <= 0)
            {
                Remove(key);
            }
        }

        public void Merge(MultiValueDictionary<TKey, TValue> toMergeWith)
        {
            if (toMergeWith == null)
            {
                return;
            }

            foreach (var pair in toMergeWith)
            {
                foreach (var value in pair.Value)
                {
                    Add(pair.Key, value);
                }
            }
        }

        public HashSet<TValue> GetValues(TKey key, bool returnEmptySet = true)
        {
            if (!TryGetValue(key, out var toReturn) && returnEmptySet)
            {
                toReturn = new HashSet<TValue>();
            }

            return toReturn;
        }
    }
}