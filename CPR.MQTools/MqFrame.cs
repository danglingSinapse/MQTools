﻿using System;
using System.Text;
using NetMQ;

namespace CPR.MQTools
{
    /// <summary>
    /// This class wraps the NetMQFrame and extends its ability to convert between different types.
    /// </summary>
    public class MqFrame : NetMQFrame
    {
        public MqFrame(byte[] buffer) : base(buffer)
        {
        }

        public MqFrame(string message) : base(message ?? "")
        {
        }

        public MqFrame(string message, Encoding encoding) : base(message ?? "", encoding)
        {
        }

        public MqFrame(int value) : base(NetworkOrderBitsConverter.GetBytes(value))
        {
        }

        public MqFrame(long value) : base(NetworkOrderBitsConverter.GetBytes(value))
        {
        }

        public MqFrame(byte value) : base(new[] {value})
        {
        }

        public MqFrame(NetMQFrame frame) : base(frame.ToByteArray())
        {
        }
        
        public MqFrame(Enum enumValue) : this(Convert.ToInt32(enumValue))
        {
        }

        public TEnum ConvertToEnum<TEnum>() where TEnum : struct
        {
            var type = typeof(TEnum);
            if (!type.IsEnum)
                throw new ArgumentException("Generic type must be an enum.");

            return (TEnum) (object) ConvertToInt32();
        }

        public string ConvertToBase64()
        {
            var bytes = ToByteArray();
            return bytes == null || bytes.Length <= 0 ? "" : Convert.ToBase64String(bytes);
        }
    }
}